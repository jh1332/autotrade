
import requests
import os, sys, ctypes
import win32com.client
import pandas as pd
from datetime import datetime
import time, calendar
from PyQt5.QtWidgets import *


#####################################################################################
def SendMsgToSlack(token, channel, msg):
    """인자로 받은 문자열을 파이썬 셸과 슬랙으로 동시에 출력한다."""
    print(datetime.now().strftime('[%m/%d %H:%M:%S]'), msg)
    strbuf = datetime.now().strftime('[%m/%d %H:%M:%S] ') + msg

    response = requests.post("https://slack.com/api/chat.postMessage",
                            headers={"Authorization": "Bearer "+token},
                            data={"channel": channel,"text": msg})
    
    print(response)
#####################################################################################

#####################################################################################
# 좋은 종목코드들 얻기
def GetGreatStockCodeList():
    codeList = [1, 2, 3]
    return codeList
    
def GetGoodStockCodeList():
    codeList = [4, 5, 6]
    return codeList
#####################################################################################

#####################################################################################
# 매수할 타이밍인지 체크
def IsGreatBuyTiming():
    return True

def IsGoodBuyTiming():
    return True
#####################################################################################

#####################################################################################
# 매도할 타이밍인지 체크
def IsSellTiming():
    return True
#####################################################################################

#####################################################################################
# 매도
def SellStock():
    pass
#####################################################################################

#####################################################################################
# 매수
def BuyStock():
    pass
#####################################################################################

#####################################################################################
class CpEvent:
    instance = None
 
    def OnReceived(self):
        code = CpEvent.instance.GetHeaderValue(0) # 종목코드
        # time = CpEvent.instance.GetHeaderValue(3)  # 시간
        timess = CpEvent.instance.GetHeaderValue(18)  # 초
        exFlag = CpEvent.instance.GetHeaderValue(19)  # 예상체결 플래그
        cprice = CpEvent.instance.GetHeaderValue(13)  # 현재가
        diff = CpEvent.instance.GetHeaderValue(2)  # 대비
        cVol = CpEvent.instance.GetHeaderValue(17)  # 순간체결수량
        vol = CpEvent.instance.GetHeaderValue(9)  # 거래량
 
        if (exFlag == ord('1')):  # 동시호가 시간 (예상체결)
            print("종목", code, " 실시간(예상체결) ", timess, "*", cprice, "대비", diff, "체결량", cVol, "거래량", vol)
        elif (exFlag == ord('2')):  # 장중(체결)
            print("종목", code, " 실시간(장중 체결) ", timess, cprice, "대비", diff, "체결량", cVol, "거래량", vol)


class CpStockCur:
    def Subscribe(self, code):
        self.objStockCur = win32com.client.Dispatch("DsCbo1.StockCur")
        win32com.client.WithEvents(self.objStockCur, CpEvent)
        self.objStockCur.SetInputValue(0, code)
        CpEvent.instance = self.objStockCur
        self.objStockCur.Subscribe()
 
    def Unsubscribe(self):
        self.objStockCur.Unsubscribe()      


class CpMarketEye:
    def Request(self, codes, rqField):
        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        # 관심종목 객체 구하기
        objRq = win32com.client.Dispatch("CpSysDib.MarketEye")
        # 요청 필드 세팅 - 종목코드, 종목명, 시간, 대비부호, 대비, 현재가, 거래량
        # rqField = [0,17, 1,2,3,4,10]
        objRq.SetInputValue(0, rqField) # 요청 필드
        objRq.SetInputValue(1, codes)  # 종목코드 or 종목코드 리스트
        objRq.BlockRequest()
 
 
        # 현재가 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        rqRet = objRq.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
 
        cnt  = objRq.GetHeaderValue(2)
 
        for i in range(cnt):
            rpCode = objRq.GetDataValue(0, i)  # 코드
            rpName = objRq.GetDataValue(1, i)  # 종목명
            rpTime= objRq.GetDataValue(2, i)  # 시간
            rpDiffFlag = objRq.GetDataValue(3, i)  # 대비부호
            rpDiff = objRq.GetDataValue(4, i)  # 대비
            rpCur = objRq.GetDataValue(5, i)  # 현재가
            rpVol = objRq.GetDataValue(6, i)  # 거래량
            print(rpCode, rpName, rpTime,  rpDiffFlag, rpDiff, rpCur, rpVol)
 
        return True
 


class CpStockMst:
    def Request(self, code):
        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        # 현재가 객체 구하기
        objStockMst = win32com.client.Dispatch("DsCbo1.StockMst")
        objStockMst.SetInputValue(0, code)  # 종목 코드 - 삼성전자
        objStockMst.BlockRequest()
 
        # 현재가 통신 및 통신 에러 처리
        rqStatus = objStockMst.GetDibStatus()
        rqRet = objStockMst.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
 
        # 현재가 정보 조회
        code = objStockMst.GetHeaderValue(0)  # 종목코드
        name = objStockMst.GetHeaderValue(1)  # 종목명
        time = objStockMst.GetHeaderValue(4)  # 시간
        cprice = objStockMst.GetHeaderValue(11)  # 종가
        diff = objStockMst.GetHeaderValue(12)  # 대비
        open = objStockMst.GetHeaderValue(13)  # 시가
        high = objStockMst.GetHeaderValue(14)  # 고가
        low = objStockMst.GetHeaderValue(15)  # 저가
        offer = objStockMst.GetHeaderValue(16)  # 매도호가
        bid = objStockMst.GetHeaderValue(17)  # 매수호가
        vol = objStockMst.GetHeaderValue(18)  # 거래량
        vol_value = objStockMst.GetHeaderValue(19)  # 거래대금
 
        print("코드 이름 시간 현재가 대비 시가 고가 저가 매도호가 매수호가 거래량 거래대금")
        print(code, name, time, cprice, diff, open, high, low, offer, bid, vol, vol_value)
        return True             
#####################################################################################

#####################################################################################
# 현재가 구하기


#####################################################################################

#####################################################################################
# 체결량 구독
def SubscribeConclusionInfo(objStockCur, code):
    objStockCur.Subscribe(code)

def UnSubscribeConclusionInfo(objStockCur):
    objStockCur.Unsubscribe()    

#####################################################################################
class StockInfo:
    baseLines = [] # (날짜 또는 시간, 가격)
    today3MinBaseLines = []
    code = 0
    isKosdaq = True
    def __init__(self):
        self.baseLines = []
        self.today3MinBaseLines = []
        self.code = 0
        self.isKosdaq = True

    def GetAllBaseLines(self):
        allBaseLines = []
        allBaseLines.extend(self.baseLines)
        allBaseLines.extend(self.today3MinBaseLines)
        return allBaseLines

    pass

# 일자별 주식 데이터
class DayStockInfo:
    date = 0    # 날짜
    time = 0    # 시간
    open = 0    # 시가
    high = 0    # 고가
    low = 0     # 저가
    close = 0   # 종가
    diff = 0    # 전일대비
    vol = 0     # 거래량

    pass

class StockInfosPerDay:
    objStockWeek = win32com.client.Dispatch("DsCbo1.StockWeek")

    def SetCode(self, code):
        self.objStockWeek.SetInputValue(0, code)
        pass

    def RequestDayStocks(self, infos, reqCount):
        tempInfos = []
        ret = self.RequestDayStock(tempInfos)
        if ret == False:
            print("날짜별 주식정보 요청 실패")
            exit()
        infos.extend(tempInfos)

        NextCount = 1
        while self.objStockWeek.Continue:  #연속 조회처리
            NextCount+=1
            if (NextCount > reqCount):
                break

            tempInfos = []
            ret = self.RequestDayStock(tempInfos)

            if ret == False:
                print("날짜별 주식정보 요청 실패")
                exit()

            infos.extend(tempInfos)
        pass

    def RequestDayStock(self, infos):
        time.sleep(2)
        # 데이터 요청
        self.objStockWeek.BlockRequest()
    
        # 통신 결과 확인
        rqStatus = self.objStockWeek.GetDibStatus()
        rqRet = self.objStockWeek.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
    
        # 일자별 정보 데이터 처리
        count = self.objStockWeek.GetHeaderValue(1)  # 데이터 개수
        for i in range(count):
            info = DayStockInfo()
            info.date = self.objStockWeek.GetDataValue(0, i)  # 일자
            info.open = self.objStockWeek.GetDataValue(1, i)  # 시가
            info.high = self.objStockWeek.GetDataValue(2, i)  # 고가
            info.low = self.objStockWeek.GetDataValue(3, i)  # 저가
            info.close = self.objStockWeek.GetDataValue(4, i)  # 종가
            info.diff = self.objStockWeek.GetDataValue(5, i)  # 전일대비
            info.vol = self.objStockWeek.GetDataValue(6, i)  # 거래량

            infos.append(info)
        
            # print(date, open, high, low, close, diff, vol)
    
        return True

    pass

#####################################################################################

#####################################################################################
# 기준선
def GetLow(a, b):
    if a < b:
        return a
    else:
        return b

def GetHigh(a, b):
    if a > b:
        return a
    else:
        return b       


def GetBaseLines(dayStockInfos):
    highPoints = GetHighPoints(dayStockInfos, 5)
    lowPoints = GetLowPoints(dayStockInfos, 5)

    points = highPoints
    points.update(lowPoints)

    # 정렬
    pointList = sorted(points.items(), key=lambda x: x[1])

# 기준선 정리
    resultPoints = []
    minRate = 0.005 # 0.5%

    # 가장 큰 값과 작은 값 저장
    resultPoints.append(pointList[0])
    del pointList[0]
    resultPoints.append(pointList[len(pointList) - 1])
    del pointList[len(pointList) - 1]

    average = (resultPoints[0][1] + resultPoints[1][1]) / 2

    if len(pointList) == 0: # 남은 값이 없으면
        # 평균 저장
        resultPoints.append(("평균", average))
    else:
        # 최고점과 최저점과의 거리가 일정 거리 이하인 점들 제거
        indexes = []
        min = resultPoints[0][1]
        max = resultPoints[1][1]
        for i in range(len(pointList)):
            cur = pointList[i][1]
            minDiff1 = cur - (cur * minRate)
            minDiff2 = cur + (cur * minRate)
            if minDiff1 <= min or max <= minDiff2:
                indexes.append(i)
        
        for i in indexes:
            del pointList[i]

        if len(pointList) == 0: # 남은 값이 없으면
            # 평균 저장
            resultPoints.append(("평균", average))     
        else:    
            if len(pointList) == 1: # 남은 값이 1개이면
                # 남은 값 저장
                resultPoints.append(pointList[0])
                del pointList[0]
            else: # 남은 값이 2개 이상이면
                minValue = pointList[0][1]
                maxValue = pointList[len(pointList) - 1][1]

                minDiff = minValue + (minValue * minRate)


                if maxValue <= minDiff: # 남은 값들 중 가장 큰 값과 작은 값의 차이가 일정 이하이면
                    # 남은 모든 값들의 평균 저장
                    sum = 0
                    for p in pointList:
                        sum = sum + p[1]

                    average = sum / len(pointList)
                    resultPoints.append((pointList[0][0], average))
                    pointList.clear()
                else:    
                    # 남은 값들 중 가장 큰 값과 작은 값을 저장
                    resultPoints.append(pointList[0])
                    resultPoints.append(pointList[len(pointList) - 1])
                    pointList.clear()
                    pass

    for p in resultPoints:
        print("날짜:", p[0], "기준선:", p[1])  






def GetLowPoints(isDay, dayStockInfos, wingCheckCount):
    lowPoints = {}

    for i in range(len(dayStockInfos)):
        first = GetLow(dayStockInfos[i].open, dayStockInfos[i].close)    

        if 1 < i and i < len(dayStockInfos) - 2: 
            isHigh = False
            for j in range(1, 3):
                lowLeft = GetLow(dayStockInfos[i + j].open, dayStockInfos[i + j].close)
                lowRight = GetLow(dayStockInfos[i - j].open, dayStockInfos[i - j].close)

                if lowLeft < first or lowRight < first:
                    isHigh = True
                    break

            if isHigh == True:
                continue    
        else:
            continue

        targetIndex = i + 1
        target = GetLow(dayStockInfos[targetIndex].open, dayStockInfos[targetIndex].close)

        IsLowPoint = True
        for j in range(wingCheckCount):
            if target < first:
                first = target
                IsLowPoint = False
                break
            else:
                targetIndex = targetIndex + 1
                if len(dayStockInfos) <= targetIndex:
                    break
                else:
                    target = GetLow(dayStockInfos[targetIndex].open, dayStockInfos[targetIndex].close)

        # print("Index:", i, "IsLowPoint:", str(IsLowPoint))

        if IsLowPoint == True:
            if isDay == True:
                date = dayStockInfos[i].date
                lowPoints[date] = first              
            else:
                time = dayStockInfos[i].time
                lowPoints[time] = first

    # 최저점 추가
    minIndex = 0
    min = GetLow(dayStockInfos[0].open, dayStockInfos[0].close) 
    for i in range(len(dayStockInfos)):
        tempLow = GetLow(dayStockInfos[i].open, dayStockInfos[i].close) 
        if tempLow < min:
            min = tempLow
            minIndex = i       

    if isDay == True:
        date = dayStockInfos[minIndex].date
        lowPoints[date] = min
    else:
        time = dayStockInfos[minIndex].time
        lowPoints[time] = min

    # 중복제거
    seen = []
    result = dict()
    for key, val in lowPoints.items():
        if val not in seen:
            seen.append(val)
            result[key] = val

    # for k in result.keys():
    #     print("시간:", k, "지지선:", str(result[k]))

    return result


def GetHighPoints(isDay, dayStockInfos, wingCheckCount):
    highPoints = {}

    for i in range(len(dayStockInfos)):
        first = GetHigh(dayStockInfos[i].open, dayStockInfos[i].close)    

        if 1 < i and i < len(dayStockInfos) - 2: 
            isLow = False
            for j in range(1, 3):
                highLeft = GetHigh(dayStockInfos[i + j].open, dayStockInfos[i + j].close)
                highRight = GetHigh(dayStockInfos[i - j].open, dayStockInfos[i - j].close)

                if first < highLeft or first < highRight:
                    isLow = True
                    break

            if isLow == True:
                continue    
        else:
            continue

        targetIndex = i + 1
        target = GetHigh(dayStockInfos[targetIndex].open, dayStockInfos[targetIndex].close)

        IsHighPoint = True
        for j in range(wingCheckCount):
            if first < target:
                first = target
                IsHighPoint = False
                break
            else:
                targetIndex = targetIndex + 1
                if len(dayStockInfos) <= targetIndex:
                    break
                else:
                    target = GetHigh(dayStockInfos[targetIndex].open, dayStockInfos[targetIndex].close)

        if IsHighPoint == True:
            if isDay == True:
                date = dayStockInfos[i].date
                highPoints[date] = first                
            else:
                time = dayStockInfos[i].time
                highPoints[time] = first

    # 최고점 추가
    maxIndex = 0
    max = GetHigh(dayStockInfos[0].open, dayStockInfos[0].close) 
    for i in range(len(dayStockInfos)):
        tempHigh = GetHigh(dayStockInfos[i].open, dayStockInfos[i].close) 
        if max < tempHigh:
            max = tempHigh
            maxIndex = i       

    if isDay == True:
        date = dayStockInfos[maxIndex].date
        highPoints[date] = max
    else:
        time = dayStockInfos[maxIndex].time
        highPoints[time] = max




    # 중복제거
    seen = []
    result = dict()
    for key, val in highPoints.items():
        if val not in seen:
            seen.append(val)
            result[key] = val

    
    # for k in result.keys():
    #     print("시간:", k, "저항선:", str(result[k]))

    return result

#####################################################################################



#####################################################################################
# 차트 요청
g_objCodeMgr = win32com.client.Dispatch('CpUtil.CpCodeMgr')
g_objCpStatus = win32com.client.Dispatch('CpUtil.CpCybos')
 
 
class CpStockChart:
    def __init__(self):
        self.objStockChart = win32com.client.Dispatch("CpSysDib.StockChart")
 
    # 차트 요청 - 기간 기준으로
    def RequestFromTo(self, code, fromDate, toDate, caller):
        print(code, fromDate, toDate)
        # 연결 여부 체크
        bConnect = g_objCpStatus.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        self.objStockChart.SetInputValue(0, code)  # 종목코드
        self.objStockChart.SetInputValue(1, ord('1'))  # 기간으로 받기
        self.objStockChart.SetInputValue(2, toDate)  # To 날짜
        self.objStockChart.SetInputValue(3, fromDate)  # From 날짜
        #self.objStockChart.SetInputValue(4, 500)  # 최근 500일치
        self.objStockChart.SetInputValue(5, [0, 2, 3, 4, 5, 8])  # 날짜,시가,고가,저가,종가,거래량
        self.objStockChart.SetInputValue(6, ord('D'))  # '차트 주기 - 일간 차트 요청
        self.objStockChart.SetInputValue(9, ord('1'))  # 수정주가 사용
        self.objStockChart.BlockRequest()
 
        rqStatus = self.objStockChart.GetDibStatus()
        rqRet = self.objStockChart.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            exit()
 
        len = self.objStockChart.GetHeaderValue(3)
         
        caller.dates = []
        caller.opens = []
        caller.highs = []
        caller.lows = []
        caller.closes = []
        caller.vols = []
        for i in range(len):
            caller.dates.append(self.objStockChart.GetDataValue(0,i))
            caller.opens.append(self.objStockChart.GetDataValue(1, i))
            caller.highs.append(self.objStockChart.GetDataValue(2, i))
            caller.lows.append(self.objStockChart.GetDataValue(3, i))
            caller.closes.append(self.objStockChart.GetDataValue(4, i))
            caller.vols.append(self.objStockChart.GetDataValue(5, i))
 
        print(len)
 
    # 차트 요청 - 최근일 부터 개수 기준
    def RequestDWM(self, code, dwm, count, caller):
        # 연결 여부 체크
        bConnect = g_objCpStatus.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        self.objStockChart.SetInputValue(0, code)  # 종목코드
        self.objStockChart.SetInputValue(1, ord('2'))  # 개수로 받기
        self.objStockChart.SetInputValue(4, count)  # 최근 500일치
        self.objStockChart.SetInputValue(5, [0, 2, 3, 4, 5, 8])  # 요청항목 - 날짜,시가,고가,저가,종가,거래량
        self.objStockChart.SetInputValue(6, dwm)  # '차트 주기 - 일/주/월
        self.objStockChart.SetInputValue(9, ord('1'))  # 수정주가 사용
        self.objStockChart.BlockRequest()
 
        rqStatus = self.objStockChart.GetDibStatus()
        rqRet = self.objStockChart.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            exit()
 
        len = self.objStockChart.GetHeaderValue(3)
 
        caller.dates = []
        caller.opens = []
        caller.highs = []
        caller.lows = []
        caller.closes = []
        caller.vols = []
        caller.times = []
        for i in range(len):
            caller.dates.append(self.objStockChart.GetDataValue(0, i))
            caller.opens.append(self.objStockChart.GetDataValue(1, i))
            caller.highs.append(self.objStockChart.GetDataValue(2, i))
            caller.lows.append(self.objStockChart.GetDataValue(3, i))
            caller.closes.append(self.objStockChart.GetDataValue(4, i))
            caller.vols.append(self.objStockChart.GetDataValue(5, i))
 
        print(len)
 
        return
 
    # 차트 요청 - 분간, 틱 차트
    # def RequestMT(self, code, dwm, count, caller):
    def RequestMT(self, code, dwm, cycle, count):
        # 연결 여부 체크
        bConnect = g_objCpStatus.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        try:
            self.objStockChart.SetInputValue(0, code)  # 종목코드
            self.objStockChart.SetInputValue(1, ord('2'))  # 개수로 받기
            self.objStockChart.SetInputValue(4, count)  # 조회 개수
            self.objStockChart.SetInputValue(5, [0, 1, 2, 3, 4, 5, 8])  # 요청항목 - 날짜, 시간,시가,고가,저가,종가,거래량
            self.objStockChart.SetInputValue(6, dwm)  # '차트 주기 - 분/틱
            self.objStockChart.SetInputValue(7, cycle)  # 분틱차트 주기
            self.objStockChart.SetInputValue(9, ord('1'))  # 수정주가 사용
            self.objStockChart.BlockRequest()
        except:
            return False

        rqStatus = self.objStockChart.GetDibStatus()
        rqRet = self.objStockChart.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            exit()
 
        len = self.objStockChart.GetHeaderValue(3)
 
        # caller.dates = []
        # caller.opens = []
        # caller.highs = []
        # caller.lows = []
        # caller.closes = []
        # caller.vols = []
        # caller.times = []
        # for i in range(len):
        #     caller.dates.append(self.objStockChart.GetDataValue(0, i))
        #     caller.times.append(self.objStockChart.GetDataValue(1, i))
        #     caller.opens.append(self.objStockChart.GetDataValue(2, i))
        #     caller.highs.append(self.objStockChart.GetDataValue(3, i))
        #     caller.lows.append(self.objStockChart.GetDataValue(4, i))
        #     caller.closes.append(self.objStockChart.GetDataValue(5, i))
        #     caller.vols.append(self.objStockChart.GetDataValue(6, i))

        stockInfos = []

        dates = []
        opens = []
        highs = []
        lows = []
        closes = []
        vols = []
        times = []
        for i in range(len):
            dates.append(self.objStockChart.GetDataValue(0, i))
            times.append(self.objStockChart.GetDataValue(1, i))
            opens.append(self.objStockChart.GetDataValue(2, i))
            highs.append(self.objStockChart.GetDataValue(3, i))
            lows.append(self.objStockChart.GetDataValue(4, i))
            closes.append(self.objStockChart.GetDataValue(5, i))
            vols.append(self.objStockChart.GetDataValue(6, i))

            stockInfo = DayStockInfo()
            stockInfo.date = dates[i]
            stockInfo.time = times[i]
            stockInfo.open = opens[i]
            stockInfo.high = highs[i]
            stockInfo.low = lows[i]
            stockInfo.close = closes[i]
            stockInfo.vol = vols[i]

            stockInfos.append(stockInfo)

            # print("날짜", dates[i], "시간:", times[i], "시가:", opens[i], "고가:", highs[i], "저가:", lows[i], "종가:", closes[i], "거래량", vols[i])
 
        print(len)
 
        return stockInfos
#####################################################################################


def GetBaseLines(isDay, dayStockInfos, wingCheckCount):
    highPoints = GetHighPoints(isDay, dayStockInfos, wingCheckCount)
    lowPoints = GetLowPoints(isDay, dayStockInfos, wingCheckCount)
    points = highPoints
    points.update(lowPoints)

    # 정렬
    pointList = sorted(points.items(), key=lambda x: x[1])

    baseLines = []
    GetBaseLinesSub(pointList, baseLines)
    return baseLines


def GetBaseLinesSub(pointList, baseLines):
    minRate = 0.005 # 0.5%

    remainCount = len(pointList)

    if remainCount == 0:
        return
    elif remainCount == 1:
        lastIndex = len(baseLines) - 1
        max = baseLines[lastIndex][1]
        min = baseLines[lastIndex - 1][1]
        
        cur = pointList[0][1]
        minDiff1 = cur - (cur * minRate)
        minDiff2 = cur + (cur * minRate)
        if minDiff1 <= min:
            average = (min + cur) * 0.5
            baseLines.append(("평균", average))
        elif max <= minDiff2:
            average = (max + cur) * 0.5
            baseLines.append(("평균", average))
        else: 
            baseLines.append(pointList[0])

        pointList.clear()  
        GetBaseLinesSub(pointList, baseLines)
    else: # 남은 선이 2개 이상이면
        minValue = pointList[0][1]
        maxValue = pointList[len(pointList) - 1][1]

        minDiff = minValue + (minValue * minRate)
        if maxValue <= minDiff: # 남은 값들 중 가장 큰 값과 작은 값의 차이가 일정 이하이면
            sum = 0
            for p in pointList:
                sum = sum + p[1]

            average = sum / len(pointList)
            baseLines.append(("평균", average)) 
            pointList.clear()
            GetBaseLinesSub(pointList, baseLines)
        else:    
            sum = 0
            noDeleteList = []
            count = 0
            minDiff = minValue + (minValue * minRate) 
            for i in range(len(pointList)):
                if pointList[i][1] <= minDiff: # 최저점과 일정거리 안에 있으면
                    sum = sum + pointList[i][1]
                    count = count + 1
                else:
                    noDeleteList.append(pointList[i])

            if count == 0: # 최저점과 일정거리 안에 있는 점들이 없으면
                baseLines.append(pointList[0])
                del pointList[0]
            else:
                # minAverage = sum / count
                # baseLines.append(("평균", minAverage)) 
                baseLines.append(pointList[0])

                # 평균 구한 점들 제거
                pointList.clear()
                pointList.extend(noDeleteList)


            sum = 0
            noDeleteList = []
            count = 0
            maxDiff = maxValue - (maxValue * minRate) 
            for i in range(len(pointList)):
                if maxDiff <= pointList[i][1]: # 최고점과 일정거리 안에 있으면
                    sum = sum + pointList[i][1]
                    count = count + 1
                else:
                    noDeleteList.append(pointList[i])

            if count == 0: # 최고점과 일정거리 안에 있는 점들이 없으면
                lastIndex = len(pointList) - 1
                baseLines.append(pointList[lastIndex])
                del pointList[lastIndex]               
            else:            
                # maxAverage = sum / count
                # baseLines.append(("평균", maxAverage)) 
                lastIndex = len(pointList) - 1
                baseLines.append(pointList[lastIndex])

                # 평균 구한 점들 제거
                pointList.clear()
                pointList.extend(noDeleteList)

            

            GetBaseLinesSub(pointList, baseLines)
        pass    