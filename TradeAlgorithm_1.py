import sys

from win32com.client import Constants
from TradeAlgorithmBase import TradeAlgorithmBase
from TradeFsmMgr import TradeFsmMgr
from TradeFsmMgr import TradeStateType
from TradeStateReadyFirst import TradeStateReadyFirst
from TradeStateZapBuy import TradeStateZapBuy
from TradeStatePlusSellReady import TradeStatePlusSellReady
from TradeStateEnd import TradeStateEnd
from TradeStateDefenseReady import TradeStateDefenseReady
from TradeStateDefenseZapBuy import TradeStateDefenseZapBuy
from TradeStateDefenseSell import TradeStateDefenseSell
from MyAutoTradeUtil import StockInfo
import datetime
import Constants

# 강한 거래대금으로 의미있는 가격대를 충분히 돌파한 이후 눌릴 때 매매하는 방식
class TradeAlgorithm_1(TradeAlgorithmBase):
    def __init__(self, tradeAI):
        super().__init__(tradeAI)

        self.zapping = False
        self.zapStartTime = 0
        self.zapHigh = 0
        self.zapLow = 0
        self.curZapCandleTime = 0

        # 
        self.onEnd_GetStockInfoForTrading = None


        # 임시변수들
        self.tempStockInfo = {}

        return

    def GetStockInfoForTrading(self, code, tradeDate, onEnd): # 이 종목을 해당 날짜에 매매하기 위한 정보 얻기
        self.onEnd_GetStockInfoForTrading = onEnd

        # 매매할 날짜 전날을 기준으로 일정기간의 과거 일봉 차트 얻기
        yearOfTradeDate = int(tradeDate[0:4]) # 매매 년
        monthOfTradeDate = int(tradeDate[4:6]) # 매매 월
        dayOfTradeDate = int(tradeDate[6:]) # 매매 일
        dateTimeOfTradeDate = datetime.datetime(yearOfTradeDate, monthOfTradeDate, dayOfTradeDate)

        dateTimeOfYesterDay = dateTimeOfTradeDate - datetime.timedelta(days = 1)
        toDate = str(dateTimeOfYesterDay.year) + str(dateTimeOfYesterDay.month).zfill(2) + str(dateTimeOfYesterDay.day).zfill(2)

        dateTimeOfFromDate = dateTimeOfYesterDay - datetime.timedelta(days = Constants.DAY_CHART_RANGE_FOR_SELECTING_TRADE_STOCK)
        fromDate = str(dateTimeOfFromDate.year) + str(dateTimeOfFromDate.month).zfill(2) + str(dateTimeOfFromDate.day).zfill(2)

        self.tradeAI.ReqChart_Day(code, fromDate, toDate, self.OnAckChart_Day)
        return

    def OnAckChart_Day(self, code, dayCandles):
        if False == dayCandles:
            self.onEnd_GetStockInfoForTrading(False)          
            return

        # 일봉차트를 분석해서 뚫어야 하는 가격을 알아냄
        # 특정 기간 안의 일봉차트를 분석해서 파동의 고점 고가들을 알아내기
        SIDE_CANDLE_COUNT_FOR_HIGH = 5 # 고점 판단을 위한 양옆의 캔들 갯수

        highPrices = [] 
        endIndex = len(dayCandles) - 1
        curIndex = endIndex
        while True: 
            if curIndex < 0:
                break

            # 현재 캔들의 고가가 오른쪽으로 최대 5개의 캔들들의 고가보다 큰지 확인
            isHighBiggerThanRight = True
            rightIndex = curIndex + 1
            while rightIndex <= endIndex:
                if dayCandles[curIndex].high < dayCandles[rightIndex].high:
                    isHighBiggerThanRight = False
                    break

                if SIDE_CANDLE_COUNT_FOR_HIGH == (rightIndex - curIndex): # 5개의 캔들과 비교를 끝냈으면
                    break

                rightIndex += 1

            if False == isHighBiggerThanRight: 
                curIndex -= 1
                continue

            # 현재 캔들의 고가가 왼쪽으로 최대 5개의 캔들들의 고가보다 큰지 확인
            isHighBiggerThanLeft = True
            leftIndex = curIndex - 1
            while 0 <= leftIndex:
                if dayCandles[curIndex].high < dayCandles[leftIndex].high:
                    isHighBiggerThanLeft = False
                    break

                if SIDE_CANDLE_COUNT_FOR_HIGH == (curIndex - leftIndex): # 5개의 캔들과 비교를 끝냈으면
                    break

                leftIndex -= 1            

            if False == isHighBiggerThanLeft: 
                curIndex -= 1
                continue

            # 찾은 고가를 리스트에 추가
            highPrices.append(dayCandles[curIndex].high)

            # 다음 비교할 캔들 인덱스 갱신(현재캔들의 왼쪽으로 5번째 캔들의 이전캔들)
            curIndex = curIndex - SIDE_CANDLE_COUNT_FOR_HIGH - 1

        if len(highPrices) < 2: # 고점이 충분히 발생하지 않았으면 
            return False

        baseHighPrice = highPrices[0]
        diffHighPriceIndex = 1
        priceToOver = 0 # 뚫어야 할 가격
        if baseHighPrice < highPrices[diffHighPriceIndex]: # 가장 최근 고점이 그 전 고점보다 낮음
            # 가장 최근 고점의 그 전 고점이 뚫어야 하는 가격임
            priceToOver = highPrices[diffHighPriceIndex]
        else:
            # 가장 최근 고점이 뚫어야 하는 가격임
            priceToOver = baseHighPrice

        self.tempStockInfo = StockInfo(code)
        self.tempStockInfo.AddImportantPrice(priceToOver)


        # 일봉차트에서 양봉이면서 거래량이 가장 큰 날의 3분봉 요청
        maxVol = 0
        maxVolDate = 0
        for i in range(len(dayCandles)):
            if dayCandles[i].IsPlus() and maxVol < dayCandles[i].vol:
                maxVol = dayCandles[i].vol
                maxVolDate = dayCandles[i].date

        self.tradeAI.ReqChart_3Min(code, maxVolDate, self.OnAckChart_3Min)        

        return
    
    def OnAckChart_3Min(self, code, candles):
        RATE_FOR_FEEL_BIG_VOLS = 0.1 # 거래량 터지기 전의 전조증상을 위한 거래량 비율

        # 가장 큰 거래량을 구함
        maxVol = 0
        for i in range(len(candles)):
            if maxVol < candles[i].vol:
                maxVol = candles[i].vol
        
        self.tempStockInfo.volForCheckBigVols = maxVol * RATE_FOR_FEEL_BIG_VOLS

        self.onEnd_GetStockInfoForTrading(self.tempStockInfo)
        return

    def CreateTradeFSM(self, code):
        super().CreateTradeFSM(code)

        self.tradeFsm[code].AddState(TradeStateReadyFirst(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStateZapBuy(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStatePlusSellReady(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStateDefenseReady(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStateDefenseZapBuy(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStateDefenseSell(code, self.tradeFsm[code], self))
        self.tradeFsm[code].AddState(TradeStateEnd(code, self.tradeFsm[code], self))
        
        self.tradeFsm[code].ChangeState(TradeStateType.ReadyFirst)

        return

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        super().OnUpdateStockCur(code, stockCur)

        # # 매수주문 테스트
        # if stockCur[2] == 20900 and stockCur[1] == 1116306:
        #     self.tradeAI.ReqBuy(code, 20850, 100)
        #     # self.tradeAI.ReqBuy(code, 20750, 100)

        # # 매도주문 테스트
        # if stockCur[2] == 20850 and stockCur[1] == 1204094:
        #     self.tradeAI.ReqSell(code, 20950, 100)


        # hh, mm = divmod(stockCur[5], 10000)
        # mm, tt = divmod(mm, 100)
        # if False == self.zapping and hh == 9 and 33 == mm:
        #     self.tradeAI.ReqBuy(code, 24800, 10)
        #     self.zapping = True



        # # 잽매수 테스트
        # startTime = 102100
        # endTime = 104200
        # hh, mm = divmod(stockCur[5], 10000)
        # mm, tt = divmod(mm, 100)

        # if False == self.zapping and startTime <= stockCur[5] and stockCur[5] < endTime: # 잽매수 시작
        #     self.zapHigh = 0
        #     self.zapLow = sys.maxsize
        #     self.zapping = True
        #     self.zapStartTime = stockCur[5]
        #     self.curZapCandleTime = self.stockInfos[code].chart_3Min[len(self.stockInfos[code].chart_3Min) - 1].time

        # if True == self.zapping and endTime <= stockCur[5]: # 잽매수 종료
        #     self.zapping = False

        #     #
        #     if None != self.tradeAI.account.accountInfos.get(code):
        #         print("잽매수 끝 -", "평단:", self.tradeAI.account.accountInfos[code].averagePrice)
        #     return

        # if False == self.zapping:
        #     return

        # delayTime = self.tradeUtil.GetTimeAddedSec(self.zapStartTime, 30)
        # if stockCur[5] < delayTime: # 딜레이 중이면
        #     return

        # curCandle = self.stockInfos[code].chart_3Min[len(self.stockInfos[code].chart_3Min) - 1]

        # # # 이전 캔들 잽매수가 끝났으면 이전 주문들을 모두 취소
        # # if self.curZapCandleTime < curCandle.time:
        # #     self.tradeAI.CancelAllReqBuy(code)
        # #     self.curZapCandleTime = curCandle.time

        # #
        # if self.zapHigh < curCandle.high:
        #     self.zapHigh = curCandle.high

        # if curCandle.low < self.zapLow:
        #     self.zapLow = curCandle.low

        # basisPrice = (self.zapHigh + self.zapLow) / 2
        # if curCandle.IsPlusCandle(): # 양봉이면
        #     # 현재가와 기준값 밑의 호가에 매수주문
        #     MaxBuyCount = 3
        #     curBuyCount = 0
        #     bidList = self.curStockBid[code]['bid']
        #     for i in range(len(bidList)):
        #         if MaxBuyCount <= curBuyCount:
        #             break

        #         if bidList[i] < stockCur[2] and bidList[i] < basisPrice:
        #             self.tradeAI.ReqBuy(code, bidList[i], 10)
        #             curBuyCount += 1
        #             self.zapStartTime = stockCur[5]

        # else: # 음봉이면
        #     if stockCur[2] <= basisPrice: # 현재가 <= 기준값
        #         # 현재가 밑의 호가에 매수주문
        #         MaxBuyCount = 3
        #         curBuyCount = 0
        #         bidList = self.curStockBid[code]['bid']
        #         for i in range(len(bidList)):
        #             if MaxBuyCount <= curBuyCount:
        #                 break

        #             if bidList[i] < stockCur[2]:
        #                 self.tradeAI.ReqBuy(code, bidList[i], 10)
        #                 curBuyCount += 1
        #                 self.zapStartTime = stockCur[5]                

        return