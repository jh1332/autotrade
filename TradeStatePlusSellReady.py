from TradeFsmMgr import TradeStateBase, TradeStateType
import Constants

class TradeStatePlusSellReady(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.PlusSellReady.value
        self.MaxProfitsRate = 0 # 현재 최고수익률
        self.waitingForEnd = False
        return

    def GetStateName(self): 
        return "수익 매도대기"  

    def Enter(self, args):
        super().Enter(args)

        self.waitingForEnd = False
        self.MaxProfitsRate = self.tradeAlgorithm.GetProfitsRate(self.code)

        # 현재 매도가능한 비중의 일정부분을 현재가에 매도신청
        sellableVol = self.tradeAlgorithm.GetSellableVol(self.code) # 현재 매도가능한 비중
        sellVol = int(sellableVol * Constants.FIRST_PLUS_SELL_VOL_RATE)
        curPrice = self.tradeAlgorithm.GetCurPrice(self.code) # 현재가격

        self.tradeAlgorithm.ReqSell(self.code, curPrice, sellVol)

        return

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        if self.waitingForEnd: # 매매 종료를 기다리고 있으면
            return

        # 최고수익률 갱신
        self.UpdateMaxProfitsRate(stockCur[2])

        # 최고수익률 대비 일정이상 하락하면 모두 시장가 매도하고 매매 끝내기
        profitsRateForExit = self.MaxProfitsRate * Constants.RATE_FOR_COMPLETED_PLUS_SELL 
        curProfitsRate = self.tradeAlgorithm.GetProfitsRate(self.code)
        if curProfitsRate <= profitsRateForExit:
            self.waitingForEnd = True
            sellableVol = self.tradeAlgorithm.GetSellableVol(self.code) # 현재 매도가능한 비중
            self.tradeAlgorithm.ReqSell_Market(self.code, sellableVol)
            
            return

        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        if self.waitingForEnd:
            self.fsm.ChangeState(TradeStateType.End)

        return

    def UpdateMaxProfitsRate(self, curPrice):
        curProfitsRate = self.tradeAlgorithm.GetProfitsRate(self.code)
        if self.MaxProfitsRate <= curProfitsRate:
            self.MaxProfitsRate = curProfitsRate

        return