from enum import Enum, auto
import Constants


class TradeFsmMgr:
    def __init__(self, code):
        self.code = code
        self.states = {}
        self.curState = None
        return

    def AddState(self, tradeState):
        self.states[tradeState.stateType] = tradeState
        return   

    def ChangeState(self, stateType, args = None):     
        if self.curState is not None:
            self.curState.Exit()

        self.curState = self.states[stateType.value]
        self.curState.Enter(args)   
        return

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        self.curState.OnUpdateStockCur(code, stockCur)
        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        self.curState.OnUpdateStockBid(code, stockBid)
        return

    def OnFailReqBuy(self, code, price, vol): # 매수주문 실패 알림
        self.curState.OnFailReqBuy(code, price, vol)
        return

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        self.curState.OnSuccessBuy(reqID, code, price, vol)
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        self.curState.OnCompleteBuy(reqID, code, price, vol)
        return

    def OnFailReqSell(self, code, price, vol): # 매도주문 실패 알림
        self.curState.OnFailReqSell(code, price, vol)
        return

    def OnSuccessSell(self, reqID, code, price, vol): # 매도체결 알림
        self.curState.OnSuccessSell(reqID, code, price, vol)
        return

    def OnCompleteSell(self, reqID, code, price, vol): # 매도체결 완료 알림
        self.curState.OnCompleteSell(reqID, code, price, vol)
        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        self.curState.OnCompleteSellMarket(code, price, vol)
        return                 


class TradeStateBase:
    def __init__(self, code, fsm, tradeAlgorithm):
        self.code = code
        self.fsm = fsm
        self.tradeAlgorithm = tradeAlgorithm
        self.stateType = TradeStateType.NoState.value
        return

    def Enter(self, args):
        print("종목:", self.code, self.GetStateName(), "상태.Enter()")
        return

    def Exit(self):
        print("종목:", self.code, self.GetStateName(), "상태.Exit()")
        return

    def GetStateName(self): 
        return    

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        return

    def OnFailReqBuy(self, code, price, vol): # 매수주문 실패 알림
        return

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        return

    def OnFailReqSell(self, code, price, vol): # 매도주문 실패 알림
        return

    def OnSuccessSell(self, reqID, code, price, vol): # 매도체결 알림
        return

    def OnCompleteSell(self, reqID, code, price, vol): # 매도체결 완료 알림
        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        return         

    def GetProfitsRate(self, averagePrice, vol, curPrice):
        totalMoney = averagePrice * vol # 총매입
        totalCurMoney = curPrice * vol # 총평가
        epal = ((curPrice - averagePrice) * vol) - self.GetTax(totalMoney, totalCurMoney) # 평가손익

        if 0 == totalMoney:
            return 0
        else:
            return (epal / totalMoney) * 100 # 수익률

    def GetRealProfits(self, averagePrice, vol, curPrice): # 실현손익 얻기
        totalMoney = averagePrice * vol
        totalCurMoney = curPrice * vol
        realProfits = ((curPrice - averagePrice) * vol) - self.GetTax(totalMoney, totalCurMoney)
        return realProfits

    def GetCurRealProfits(self): # 현재 실현손익 얻기
        accountInfo = self.tradeAlgorithm.GetAccountInfo(self.code)
        return accountInfo.realProfits

    def GetCurTotalMoney(self): # 현재 총매입
        accountInfo = self.tradeAlgorithm.GetAccountInfo(self.code)
        return accountInfo.totalMoney

    def GetAveragePrice(self): # 현재 평단
        accountInfo = self.tradeAlgorithm.GetAccountInfo(self.code)
        return accountInfo.averagePrice

    def GetSellableVol(self): # 현재 매도가능수량
        accountInfo = self.tradeAlgorithm.GetAccountInfo(self.code)
        return accountInfo.sellableVol                                

    def GetTax(self, totalMoney, totalCurMoney):
        return (totalMoney * Constants.BUY_TAX) + (totalCurMoney * Constants.SELL_TAX) + (totalCurMoney * Constants.TRANSACTION_TAX)

    def GetCurCandleIndex(self): # 현재 캔들 인덱스 얻기
        return self.tradeAlgorithm.GetCodeInfo(self.code).GetCurCandleIndex()

    def GetCandle(self, index): # 캔들 얻기
        return self.tradeAlgorithm.GetCodeInfo(self.code).GetCandle(index)

    def GetCurCandle(self): # 현재 캔들 얻기
        return self.GetCandle(self.GetCurCandleIndex())

class TradeStateType(Enum):
    NoState = auto()
    ReadyFirst = auto() # 초기대기
    ZapBuy = auto() # 잽매수
    DefenseReady = auto() # 방어대기
    PlusSellReady = auto() # 수익 매도대기
    DefenseZapBuy = auto() # 방어 잽매수
    DefenseSell = auto() # 방어매도
    End = auto() # 매매 종료
    

