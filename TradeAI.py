from MyAutoTradeUtil import MyAccount, MyAutoTradeUtil
from MyAutoTradeUtil import Candle, StockInfo
from HtsServer import HtsServerReal, HtsServerTest
import keyboard
import Constants
import os
import asyncio

from TradeAlgorithm_1 import TradeAlgorithm_1

class TradeAIBase:
    htsServer = {}

    def __init__(self):
        self.account = MyAccount(self)        
        self.tradeAlgorithm = TradeAlgorithm_1(self) # 매매 알고리즘 생성
        self.buyReqIDs = []
        self.sellReqIDs = []
        return

    def Update(self):
        return

    def IsSubscribed(self):    
        return self.htsServer.IsSubscribed()

    def IsSubscribeTime(self):
        return True

    def Subscribe(self): # 조건에 맞는 종목들 구독
        return       

    def GetCodesToTrade(self, tradeDate): # 매매할 종목들 얻기
        return

    def ReqChart_Day(self, code, fromDate, toDate, onEnd): # 일봉차트 얻기
        self.htsServer.ReqChart_Day(code, fromDate, toDate, onEnd)
        return

    def ReqChart_3Min(self, code, date, onEnd): # 3분봉차트 얻기
        self.htsServer.ReqChart_3Min(code, date, onEnd)
        return

    def ReqBuy(self, code, price, vol): # 매수주문
        self.htsServer.ReqBuy(code, price, vol)
        return

    def CancelAllReqBuy(self, code): # 모든 매수주문 취소
        reqIds = list(self.buyReqIDs)

        for i in range(len(reqIds)):
            self.htsServer.CancelReqBuy(reqIds[i], code)

        return    

    def ReqSell(self, code, price, vol): # 매도주문
        self.htsServer.ReqSell(code, price, vol)
        return

    def ReqSell_Market(self, code, vol): # 시장가 매도주문
        self.htsServer.ReqSell_Market(code, vol)
        return

    def CancelAllReq(self, code): # 모든 주문을 취소
        # 매수주문 취소
        _buyReqIDs = list(self.buyReqIDs)
        for i in range(len(_buyReqIDs)):
            self.htsServer.CancelReqBuy(_buyReqIDs[i], code)

        # 매도주문 취소
        _sellReqIDs = list(self.sellReqIDs)
        for i in range(len(_sellReqIDs)):
            self.htsServer.CancelReqSell(_sellReqIDs[i], code)        

        return   

    def IsTradeMarketEnded(self):
        return

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        self.tradeAlgorithm.OnUpdateStockCur(code, stockCur)
        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터
        self.tradeAlgorithm.OnUpdateStockBid(code, stockBid)
        return

    def OnSuccessReqBuy(self, reqID, code, price, vol): # 매수주문 성공 알림
        print("매수주문 성공", "주문ID:", reqID, "code:", code, "매수가격:", price, "수량:", vol)

        # 매수주문ID리스트에 주문ID 추가
        self.buyReqIDs.append(reqID)
        return

    def OnFailReqBuy(self, code, price, vol): # 매수주문 실패 알림
        print("매수주문 실패", "code:", code, "매수가격:", price, "실패수량:", vol)

        self.tradeAlgorithm.OnFailReqBuy(code, price, vol)        
        return

    def OnCancelReqBuy(self, reqID, code, price, vol): # 매수주문 취소 알림
        print("매수주문 취소", "주문ID:", reqID, "code:", code, "매수가격:", price, "수량:", vol)

        # 매수주문ID리스트에서 주문ID 제거
        self.buyReqIDs.remove(reqID)    
        return

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        print("****매수체결", "주문ID:", reqID, "code:", code, "매수가격:", price, "수량:", vol, "****")
        self.tradeAlgorithm.OnSuccessBuy(reqID, code, price, vol)
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        print("****매수체결 완료", "ID:", reqID, "code:", code, "가격:", price, "수량:", vol, "****")

        # 해당 매수주문 체결이 완료됐기 때문에 매수주문ID리스트에서 주문ID 제거
        self.buyReqIDs.remove(reqID)

        #
        self.tradeAlgorithm.OnCompleteBuy(reqID, code, price, vol)
        return

    def OnSuccessReqSell(self, reqID, code, price, vol): # 매도주문 성공 알림
        print("매도주문 성공", "주문ID:", reqID, "code:", code, "매도가격:", price, "수량:", vol)

        # 매도주문ID리스트에 주문ID 추가
        self.sellReqIDs.append(reqID)
        return

    def OnFailReqSell(self, code, price, vol): # 매도주문 실패 알림
        print("매도주문 실패", "code:", code, "매도가격:", price, "실패수량:", vol)
        self.tradeAlgorithm.OnFailReqSell(code, price, vol)
        return

    def OnCancelReqSell(self, reqID, code, price, vol): # 매도주문 취소 알림
        print("매도주문 취소", "주문ID:", reqID, "code:", code, "매도가격:", price, "수량:", vol)

        # 매수주문ID리스트에서 주문ID 제거
        self.buyReqIDs.remove(reqID)
        return

    def OnSuccessSell(self, reqID, code, price, vol): # 매도체결 알림
        print("****매도체결", "주문ID:", reqID, "code:", code, "매도가격:", price, "수량:", vol, "****")
        self.tradeAlgorithm.OnSuccessSell(reqID, code, price, vol)
        return

    def OnCompleteSell(self, reqID, code, price, vol): # 매도체결 완료 알림
        print("****매도체결 완료", "ID:", reqID, "code:", code, "가격:", price, "수량:", vol, "****")

        # 해당 매도주문 체결이 완료됐기 때문에 매도주문ID리스트에서 주문ID 제거
        self.sellReqIDs.remove(reqID)

        # 
        self.tradeAlgorithm.OnCompleteSell(reqID, code, price, vol)
        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        print("****시장가 매도체결 완료", "code:", code, "가격:", price, "수량:", vol, "****")
        self.tradeAlgorithm.OnCompleteSellMarket(code, price, vol)
        return
   

class TradeAIReal(TradeAIBase):
    def __init__(self):
        super().__init__()

        self.htsServer = HtsServerReal(self, self.account)
        return   

    def IsSubscribeTime(self):
        return False

    def Subscribe(self):
        print("거래대금 상위종목 검색")
        print("거래대금 상위종목 구독")
        self.subscribed = True
        return

    def GetCodesToTrade(self, tradeDate): # 매매할 종목들 얻기
        # 이미 저장된 파일이 있다면 읽어서 알아내기
        # 없다면 전체 종목들의 일봉차트를 분석해서 매매할 종목을 선정하기


        return

    def IsTradeMarketEnded(self):
        return False
     

class TradeAITest(TradeAIBase):
    def __init__(self):
        super().__init__()

        self.tradeUtil = MyAutoTradeUtil()
        self.htsServer = HtsServerTest(self, self.account)

        self.isTradingStarted = False

        #
        self.stockInfos = []
        self.reqCountOfStockInfoForTrading = 0
        self.tradeDate = 0

        # # 매매할 종목에 대한 정보 추가
        # stockInfo = StockInfo(self.htsServer.code, Constants.TOTAL_MONEY_PER_STOCK)
        # stockInfo.AddImportantPrice(24000)
        # self.tradeAlgorithm.AddCodeInfo(stockInfo)

        # 
        print("-------매매AI 초기화 끝--------")

        return    

    def Update(self):
        self.htsServer.Update()

        return

    def Subscribe(self):
        self.htsServer.Subscribe()
        return

    def GetCodesToTrade(self, tradeDate): # 매매할 종목들 얻기
        # # 날짜에 해당하는 매매데이터 파일을 읽어서 종목코드 얻기
        filePath = "./매매 데이터"
        dirPath = "./매매 데이터/" + tradeDate
        if os.path.isdir(dirPath): # 날짜의 폴더가 있으면 그 폴더읽기
            filePath = dirPath

        fileFullPath = filePath + "/" + tradeDate + "_1.p"
        dataList = self.tradeUtil.LoadFile(fileFullPath)

        MAX_PRICE = 100000 # 최대주가
        MIN_PRICE = 1100 # 최소주가
        codes = []
        print("-----------------매매후보-------------------")
        for data in dataList:
            if 0 == len(data['stockCur']):
                continue

            price = data['stockCur'][0][2]
            if MAX_PRICE <= price: # 주가가 최대가격 이상이면 제외
                continue
            elif price <= MIN_PRICE: # 주가가 최소가격 이하이면 제외
                continue

            codes.append(data["code"])
            print(data["name"])        

        print("-------------------------------------------")      

        # codes = []
        # codes.append("A032820")
        # codes.append("A256840")
        # codes.append("A123420")
        # codes.append("A058110")  

        return codes

    def IsTradeMarketEnded(self):
        return False

    def IsTradingStarted(self):
        return self.isTradingStarted

    def StartTrading(self, tradeDate):
        self.isTradingStarted = True

        # 매매할 종목코드 얻기
        tradeCodes = self.GetCodesToTrade(tradeDate)

        # 각 종목의 차트들을 분석해서 매매에 필요한 정보 얻기
        self.stockInfos = []
        self.tradeDate = tradeDate
        self.reqCountOfStockInfoForTrading = len(tradeCodes)
        for i in range(len(tradeCodes)):
            self.tradeAlgorithm.GetStockInfoForTrading(tradeCodes[i], tradeDate, self.OnEnd_GetStockInfoForTrading)

        return

    def OnEnd_GetStockInfoForTrading(self, result):
        if False != result:
            self.stockInfos.append(result)

        self.reqCountOfStockInfoForTrading -= 1

        # 요청에 대한 응답을 다 받았으면 종목정보를 이용해서 구독하기
        if 0 == self.reqCountOfStockInfoForTrading:
            # 종목들 구독하기
            for i in range(len(self.stockInfos)):
                self.tradeAlgorithm.AddCodeInfo(self.stockInfos[i])

                self.htsServer.Subscribe(self.stockInfos[i].code, self.tradeDate)            
        return        


            
