from MyAutoTradeUtil import StockInfo
from MyAutoTradeUtil import MyAutoTradeUtil
from TradeFsmMgr import TradeFsmMgr

class TradeAlgorithmBase:
    def __init__(self, tradeAI):
        self.tradeAI = tradeAI
        self.stockInfos = {}
        self.curStockBid = {} # 현재 호가창
        self.tradeFsm = {} # 종목별 매매 FSM
        self.tradeUtil = MyAutoTradeUtil()
        return

    def GetStockInfoForTrading(self, code, tradeDate): # 이 종목을 해당 날짜에 매매하기 위한 정보 얻기
        return

    def CreateTradeFSM(self, code):
        self.tradeFsm[code] = TradeFsmMgr(code)
        return

    def AddCodeInfo(self, stockInfo):
        self.stockInfos[stockInfo.code] = stockInfo
        return

    def GetCodeInfo(self, code):
        return self.stockInfos[code]

    def GetVolForCheckBigVols(self, code): # 거래량이 터지기 전 전조증상을 느끼기 위한 거래량 얻기
        return self.stockInfos[code].volForCheckBigVols

    def GetPriceToOver(self, code): # 뚫어야 하는 가격 얻기
        return self.GetCodeInfo(code).impPrices[0]    

    def GetProfitsRate(self, code):
        accountInfo = self.tradeAI.account.GetInfo(code)
        if None == accountInfo:
            return 0

        return accountInfo.profitsRate

    def GetCurPrice(self, code):
        return self.stockInfos[code].GetCurPrice()

    def GetSellableVol(self, code): # 현재 매도가능비중을 얻기
        accountInfo = self.GetAccountInfo(code)
        if None == accountInfo:
            return 0

        return accountInfo.sellableVol

    def GetAccountInfo(self, code): # 해당종목의 계좌정보를 얻기
        return self.tradeAI.account.GetInfo(code)

    def ReqSell(self, code, price, vol): # 매도주문
        self.tradeAI.ReqSell(code, price, vol)
        return

    def ReqSell_Market(self, code, vol): # 시장가 매도주문
        self.tradeAI.ReqSell_Market(code, vol)
        return

    def CancelAllReq(self, code): # 모든 주문을 취소
        self.tradeAI.CancelAllReq(code)
        return            

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        # FSM이 생성 안 됐으면 생성
        if None == self.tradeFsm.get(code):
            self.CreateTradeFSM(code)

        # print("[체결]", "시간:", stockCur[5], "누적거래량:", stockCur[1], "체결수량:", stockCur[4], "현재가", stockCur[2])

        # 체결데이터를 이용해서 실시간으로 3분봉 차트 생성 또는 갱신
        self.tradeUtil.ConvertToChart_3Min(self.stockInfos[code].chart_3Min, stockCur)

        #
        self.tradeFsm[code].OnUpdateStockCur(code, stockCur)

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터
        # FSM이 생성 안 됐으면 생성
        if None == self.tradeFsm.get(code):
            self.CreateTradeFSM(code)

        self.curStockBid[code] = stockBid
            
        # print('[호가]', '시간', stockBid['time'], '누적거래량', stockBid['vol'], '1호매도가:', stockBid['offer'][0], '1호매도대기물량:', stockBid['offervol'][0], '1호매수가:', stockBid['bid'][0], '1호매수대기물량:', stockBid['bidvol'][0]) 
        self.tradeFsm[code].OnUpdateStockBid(code, stockBid)

        return

    def OnFailReqBuy(self, code, price, vol): # 매수주문 실패 알림
        self.tradeFsm[code].OnFailReqBuy(code, price, vol)
        return

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        self.tradeFsm[code].OnSuccessBuy(reqID, code, price, vol)
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        self.tradeFsm[code].OnCompleteBuy(reqID, code, price, vol)
        return

    def OnFailReqSell(self, code, price, vol): # 매도주문 실패 알림
        self.tradeFsm[code].OnFailReqSell(code, price, vol)
        return

    def OnSuccessSell(self, reqID, code, price, vol): # 매도체결 알림
        self.tradeFsm[code].OnSuccessSell(reqID, code, price, vol)
        return

    def OnCompleteSell(self, reqID, code, price, vol): # 매도체결 완료 알림
        self.tradeFsm[code].OnCompleteSell(reqID, code, price, vol)
        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        self.tradeFsm[code].OnCompleteSellMarket(code, price, vol)
        return        

    pass