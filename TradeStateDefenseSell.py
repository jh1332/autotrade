from TradeFsmMgr import TradeStateBase, TradeStateType
import sys
import Constants

class TradeStateDefenseSell(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.DefenseSell.value
        self.MaxProfitsRate = 0 # 현재 최고수익률
        self.waitingForEnd = False
        self.sellableVol = 0 # 방어잽매수 비중
        self.averagePrice = 0 # 방어잽매수 평단
        self.curPrice = 0 # 현재가
        self.buyableMoney = 0 # 방어 잽매수할 돈
        self.maxZapMoney = 0 # 방어잽매수할 최대금액
        self.waitingForFinish = False # 매매를 끝낼 것인지
        
        return

    def Enter(self, args):
        super().Enter(args)

        self.sellableVol = int(args[0])
        self.averagePrice = float(args[1])
        self.curPrice = int(args[2])
        self.buyableMoney = int(args[3]) # 앞에서 방어 잽매수하다 남은 돈
        self.maxZapMoney = int(args[4]) # 방어잽매수할 최대금액

        self.waitingForEnd = False
        self.MaxProfitsRate = self.GetProfitsRate(self.averagePrice, self.sellableVol, self.curPrice)

        # 현재 매도가능한 비중의 일정부분을 시장가에 매도신청
        sellVol = int(self.sellableVol * Constants.FIRST_SELL_VOL_RATE_DEFENSE_SELL)
        self.tradeAlgorithm.ReqSell_Market(self.code, sellVol)        

        return

    def Exit(self):
        super().Exit()
        return

    def GetStateName(self): 
        return "방어매도"

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터 
        if self.waitingForEnd: # 매매 종료를 기다리고 있으면
            return

        # 현재가 갱신
        self.curPrice = stockCur[2]

        # 최고수익률 갱신
        self.UpdateMaxProfitsRate(self.curPrice)            

        # 현재가에 남은 비중을 전부 매도하면 일정 이상 수익이면 전부 매도하고 매매 끝내기
        realProfitsWhenAllSell = self.GetRealProfits(self.GetAveragePrice(), self.GetSellableVol(), self.curPrice) # 전부매도했을 때 실현손익
        curRealProfits = self.GetCurRealProfits() # 현재 실현손익
        totalRealProfits = realProfitsWhenAllSell + curRealProfits
        totalProfitsRate = totalRealProfits / self.GetCurTotalMoney() # 최종 수익률
        if 0.005 < totalProfitsRate:
            self.waitingForFinish = True
            self.tradeAlgorithm.ReqSell_Market(self.code, self.GetSellableVol())
            return

        # 최고수익률 대비 일정이상 하락하면 모두 시장가 매도하고 다음 상태로 전환
        profitsRateForExit = self.MaxProfitsRate * Constants.RATE_FOR_COMPLETED_PLUS_SELL 
        curProfitsRate = self.GetProfitsRate(self.averagePrice, self.sellableVol, self.curPrice)
        if curProfitsRate <= profitsRateForExit:
            self.waitingForEnd = True
            self.tradeAlgorithm.ReqSell_Market(self.code, self.sellableVol)
            return

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        return

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        self.sellableVol -= vol # 잽매수 비중 갱신
        self.buyableMoney += price * vol # 방어 잽매수할 금액 갱신

        if self.waitingForFinish:
            print("매매 끝")
            self.fsm.ChangeState(TradeStateType.End)
            return

        if self.waitingForEnd:
            if self.buyableMoney < self.maxZapMoney:
                self.buyableMoney = self.maxZapMoney

            args = []
            args.append(self.buyableMoney) # 잽매수할 금액
            self.fsm.ChangeState(TradeStateType.DefenseZapBuy, args)
            return

        return

    def UpdateMaxProfitsRate(self, curPrice):
        curProfitsRate = self.GetProfitsRate(self.averagePrice, self.sellableVol, curPrice)
        if self.MaxProfitsRate <= curProfitsRate:
            self.MaxProfitsRate = curProfitsRate

        return
           