from TradeFsmMgr import TradeStateBase, TradeStateType
import sys
import Constants

class TradeStateDefenseReady(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.DefenseReady.value
        self.buyableMoney = 0 # 매수가능한 금액
        self.maxZapMoney = 0 # 잽매수 최대금액
        return

    def Enter(self, args):
        super().Enter(args)

        sellVol = int(args[0]) # 매도할 비중
        remainZapMoney = int(args[1]) # 남은 잽매수 금액
        self.maxZapMoney = int(args[2]) # 방어잽매수할 최대금액
        
        self.buyableMoney = remainZapMoney
        
        # 잽매수한 금액의 일정비중을 시장가 매도
        self.tradeAlgorithm.ReqSell_Market(self.code, sellVol)         

        return

    def Exit(self):
        super().Exit()
        return

    def GetStateName(self): 
        return "방어대기"

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        stockInfo = self.tradeAlgorithm.GetCodeInfo(self.code)
        curCandleIndex = stockInfo.GetCurCandleIndex()
        if curCandleIndex < 2:
            return

        # 현재 캔들 이전 캔들 2개가 연속으로 양봉이면 하락세가 멈춘 것으로 판단
        isDownStopped = True
        for i in range(2):
            index = curCandleIndex - i - 1
            candle = stockInfo.GetCandle(index)
            if False == candle.IsPlusCandle():
                isDownStopped = False
                break

        if isDownStopped and 0 < self.buyableMoney:
            args = []
            args.append(self.buyableMoney)
            self.fsm.ChangeState(TradeStateType.DefenseZapBuy, args)
            return

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        return
        
    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        self.buyableMoney += price * vol
        if self.buyableMoney < self.maxZapMoney: # 확보한 금액이 방어잽매수할 최대금액보다 작다면
            # 돈을 끌어와서 보정
            self.buyableMoney = self.maxZapMoney

        return                          