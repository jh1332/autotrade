from TradeFsmMgr import TradeStateBase, TradeStateType
import sys
import Constants

class TradeStateZapBuy(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.ZapBuy.value
        self.zapping = False
        self.zapStartTime = 0
        self.zapHigh = 0
        self.zapLow = 0
        self.basisPrice = 0
        self.maxZapMoney = 0 # 잽매수 최대 금액
        self.remainMoney = 0 # 남은 잽매수할 금액
        self.moneyPerReq = 0 # 매수주문 당 금액
        self.zapBuyVol = 0 # 잽매수한 비중
        return

    def Enter(self, args):
        super().Enter(args)

        self.maxZapMoney = int(args[0])
        self.remainMoney = self.maxZapMoney
        self.moneyPerReq = int(self.remainMoney * Constants.MONEY_RATE_PER_ZAP_REQ)
        self.zapBuyVol = 0
        return

    def Exit(self):
        super().Exit()

        self.zapping = False

        # 이전에 해놓은 모든 주문 취소
        self.tradeAlgorithm.CancelAllReq(self.code)
        return

    def GetStateName(self): 
        return "잽매수"   

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        if False == self.zapping: # 잽매수 시작
            self.zapHigh = 0
            self.zapLow = sys.maxsize
            self.zapping = True
            self.zapStartTime = stockCur[5]
            self.basisPrice = 0
            # self.curZapCandleTime = self.stockInfos[code].chart_3Min[len(self.stockInfos[code].chart_3Min) - 1].time

        if self.MustGo_DefenseReady(stockCur): # 방어대기로 전환해야 하면
            if None != self.tradeAlgorithm.tradeAI.account.accountInfos.get(code):
                print("잽매수 끝 -", "평단:", self.tradeAlgorithm.tradeAI.account.accountInfos[code].averagePrice)

            # 방어잽매수할 최대금액 = 잽매수한 금액 * 일정비율(예: 절반)
            sellVol = int(self.zapBuyVol * Constants.SELL_VOL_RATE_ENTERING_DEFENSE_READY)
            zapBuyMoney = self.maxZapMoney - self.remainMoney # 잽매수한 금액
            maxMoneyForDefenseZap = zapBuyMoney * Constants.MAX_MONEY_RATE_FOR_DEFENSE_ZAP

            args = []
            args.append(sellVol) # 방어대기 상태에서 매도할 비중(예: 잽매수한 비중의 절반)
            args.append(0) # 남은 금액(잽매수->방어대기 일 때는 매수할 금액을 추가하지 않음)
            args.append(maxMoneyForDefenseZap) # 방어잽매수할 최대금액 = 잽매수한 금액 * 일정비율(예: 절반)
            self.fsm.ChangeState(TradeStateType.DefenseReady, args)
            return

        if self.MustGo_PlusSellReady(stockCur): # 수익매도대기로 전환해야 하면
            if None != self.tradeAlgorithm.tradeAI.account.accountInfos.get(code):
                print("잽매수 끝 -", "평단:", self.tradeAlgorithm.tradeAI.account.accountInfos[code].averagePrice)

            self.fsm.ChangeState(TradeStateType.PlusSellReady)
            return

        #
        delayTime = self.tradeAlgorithm.tradeUtil.GetTimeAddedSec(self.zapStartTime, 30)
        if stockCur[5] < delayTime: # 딜레이 중이면
            return

        curCandle = self.tradeAlgorithm.stockInfos[code].chart_3Min[len(self.tradeAlgorithm.stockInfos[code].chart_3Min) - 1]

        # # 이전 캔들 잽매수가 끝났으면 이전 주문들을 모두 취소
        # if self.curZapCandleTime < curCandle.time:
        #     self.tradeAI.CancelAllReqBuy(code)
        #     self.curZapCandleTime = curCandle.time

        #
        if self.zapHigh < curCandle.high:
            self.zapHigh = curCandle.high

        if curCandle.low < self.zapLow:
            self.zapLow = curCandle.low

        self.basisPrice = (self.zapHigh + self.zapLow) / 2
        if curCandle.IsPlusCandle(): # 양봉이면
            # 현재가와 기준값 밑의 호가에 매수주문
            MaxBuyCount = 3
            curBuyCount = 0
            bidList = self.tradeAlgorithm.curStockBid[code]['bid']
            for i in range(len(bidList)):
                if MaxBuyCount <= curBuyCount:
                    break

                if bidList[i] < stockCur[2] and bidList[i] < self.basisPrice:
                    priceToBuy = bidList[i]
                    volToBuy = int(self.moneyPerReq / priceToBuy)
                    if 0 == volToBuy:
                        volToBuy = 1                    

                    # 남은 돈이 없으면 안 사기
                    if self.remainMoney < priceToBuy * volToBuy:
                        break

                    # 
                    self.tradeAlgorithm.tradeAI.ReqBuy(code, priceToBuy, volToBuy)
                    curBuyCount += 1
                    self.zapStartTime = stockCur[5]

        else: # 음봉이면
            if stockCur[2] <= self.basisPrice: # 현재가 <= 기준값
                # 현재가 밑의 호가에 매수주문
                MaxBuyCount = 3
                curBuyCount = 0
                bidList = self.tradeAlgorithm.curStockBid[code]['bid']
                for i in range(len(bidList)):
                    if MaxBuyCount <= curBuyCount:
                        break

                    if bidList[i] < stockCur[2]:
                        priceToBuy = bidList[i]
                        volToBuy = int(self.moneyPerReq / priceToBuy)
                        if 0 == volToBuy:
                            volToBuy = 1

                        # 남은 돈이 없으면 안 사기
                        if self.remainMoney < priceToBuy * volToBuy:
                            break

                        #
                        self.tradeAlgorithm.tradeAI.ReqBuy(code, priceToBuy, volToBuy)
                        curBuyCount += 1
                        self.zapStartTime = stockCur[5]    

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        return

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        self.zapBuyVol += vol # 잽매수한 비중 갱신
        self.remainMoney -= (price * vol) # 남은 돈 갱신
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        if self.remainMoney <= 0:
            # 이전에 해놓은 모든 주문 취소
            self.tradeAlgorithm.CancelAllReq(self.code)

        return        

    def MustGo_DefenseReady(self, stockCur):
        if 0 == self.zapHigh: # 아직 잽매수가 시작되지 않았으면
            return False

        if self.basisPrice <= stockCur[2]: # 현재가가 중간값 위에 있으면
            return False     

        # 잽매수 구간이 일정이상 폭으로 만들어졌고
        # 이 구간의 중간값보다 일정이상 하락하면 방어대기로 전환

        curZapHeight = (self.zapHigh - self.zapLow) / self.zapLow
        if Constants.MIN_HEIGHT_OF_ZAP_RANGE <= curZapHeight:
            distBetweenBasisAndCurPrice = (self.basisPrice - stockCur[2]) / stockCur[2]
            if Constants.HEIGHT_FOR_DEFENSE_READY <= distBetweenBasisAndCurPrice:
                return True

        return False

    def MustGo_PlusSellReady(self, stockCur):
        # 최소 수익률 이상 상승했으면 수익매도대기로 전환
        profitsRate = self.tradeAlgorithm.GetProfitsRate(self.code)
        if Constants.MIN_PROFITS_RATE <= profitsRate:
            return True

        return False    