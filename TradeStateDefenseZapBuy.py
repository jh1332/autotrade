from TradeFsmMgr import TradeStateBase, TradeStateType
import sys
import Constants

class TradeStateDefenseZapBuy(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.DefenseZapBuy.value
        self.zapping = False
        self.zapStartTime = 0
        self.zapHigh = 0
        self.zapLow = 0
        self.basisPrice = 0
        self.isBoxCreated = False
        self.basisPriceForDefenseReady = 0
        self.averagePrice = 0 # 현재 구간의 평균단가
        self.zapBuyVol = 0 # 현재 구간에서 잽매수한 비중
        self.remainMoney = 0 # 잽매수할 돈
        self.moneyPerReq = 0 # 매수주문 당 금액
        self.maxZapMoney = 0 # 잽매수 최대 금액
        return

    def Enter(self, args):
        super().Enter(args)

        self.maxZapMoney = int(args[0])
        self.remainMoney = self.maxZapMoney
        self.moneyPerReq = int(self.maxZapMoney * Constants.MONEY_RATE_PER_ZAP_REQ)

        self.averagePrice = 0
        self.zapBuyVol = 0
        self.isBoxCreated = False
        self.basisPriceForDefenseReady = 0
        return

    def Exit(self):
        super().Exit()

        self.zapping = False

        # 이전에 해놓은 모든 주문 취소
        self.tradeAlgorithm.CancelAllReq(self.code)
        return

    def GetStateName(self): 
        return "방어잽매수"   

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        if False == self.zapping: # 잽매수 시작
            self.zapHigh = 0
            self.zapLow = sys.maxsize
            self.zapping = True
            self.zapStartTime = stockCur[5]
            self.basisPrice = 0

        if self.MustGo_DefenseReady(stockCur): # 방어대기로 전환해야 하면
            if None != self.tradeAlgorithm.tradeAI.account.accountInfos.get(code):
                print("잽매수 끝 -", "평단:", self.tradeAlgorithm.tradeAI.account.accountInfos[code].averagePrice)

            sellVol = int(self.zapBuyVol * Constants.SELL_VOL_RATE_ENTERING_DEFENSE_READY)

            args = []
            args.append(sellVol) # 방어대기 상태에서 매도할 비중(예: 잽매수한 비중의 절반
            args.append(self.remainMoney) # 남은 금액
            args.append(self.maxZapMoney) # 방어잽매수할 최대금액
            
            self.fsm.ChangeState(TradeStateType.DefenseReady, args)
            return

        if self.MustGo_DefenseSell(stockCur): # 방어매도로 전환해야 하면
            if None != self.tradeAlgorithm.tradeAI.account.accountInfos.get(code):
                print("잽매수 끝 -", "평단:", self.tradeAlgorithm.tradeAI.account.accountInfos[code].averagePrice)

            # 방어매도 상태로 전환
            args = []
            args.append(self.zapBuyVol) # 방어잽매수한 비중
            args.append(self.averagePrice) # 방어잽매수 평단
            args.append(stockCur[2]) # 현재가
            args.append(self.remainMoney) # 방어잽매수하다 남은 금액
            args.append(self.maxZapMoney) # 방어잽매수할 최대금액
            self.fsm.ChangeState(TradeStateType.DefenseSell, args)
            return

        curCandle = self.tradeAlgorithm.stockInfos[code].chart_3Min[len(self.tradeAlgorithm.stockInfos[code].chart_3Min) - 1]

        # 구간의 높이 갱신
        if self.zapHigh < curCandle.high:
            self.zapHigh = curCandle.high

        if curCandle.low < self.zapLow:
            self.zapLow = curCandle.low

        # 잽매수 구간의 크기가 일정이상 만들어졌으면
        # 방어대기 기준 평단을 정함
        if False == self.isBoxCreated:
            curZapHeight = (self.zapHigh - self.zapLow) / self.zapLow
            if Constants.MIN_HEIGHT_OF_ZAP_RANGE <= curZapHeight:
                if 0 < self.averagePrice:
                    self.isBoxCreated = True
                    self.basisPriceForDefenseReady = self.averagePrice

        # 
        delayTime = self.tradeAlgorithm.tradeUtil.GetTimeAddedSec(self.zapStartTime, 30)
        if stockCur[5] < delayTime: # 딜레이 중이면
            return

        self.basisPrice = (self.zapHigh + self.zapLow) / 2
        if curCandle.IsPlusCandle(): # 양봉이면
            # 현재가와 기준값 밑의 호가에 매수주문
            MaxBuyCount = 3
            curBuyCount = 0
            bidList = self.tradeAlgorithm.curStockBid[code]['bid']
            for i in range(len(bidList)):
                if MaxBuyCount <= curBuyCount:
                    break

                if bidList[i] < stockCur[2] and bidList[i] < self.basisPrice:
                    priceToBuy = bidList[i]
                    volToBuy = int(self.moneyPerReq / priceToBuy)                    
                    if 0 == volToBuy:
                        volToBuy = 1

                    # 남은 돈이 없으면 안 사기
                    if self.remainMoney < priceToBuy * volToBuy:
                        break

                    self.tradeAlgorithm.tradeAI.ReqBuy(code, priceToBuy, volToBuy)
                    curBuyCount += 1
                    self.zapStartTime = stockCur[5]

        else: # 음봉이면
            if stockCur[2] <= self.basisPrice: # 현재가 <= 기준값
                # 현재가 밑의 호가에 매수주문
                MaxBuyCount = 3
                curBuyCount = 0
                bidList = self.tradeAlgorithm.curStockBid[code]['bid']
                for i in range(len(bidList)):
                    if MaxBuyCount <= curBuyCount:
                        break

                    if bidList[i] < stockCur[2]:
                        priceToBuy = bidList[i]
                        volToBuy = int(self.moneyPerReq / priceToBuy)
                        if 0 == volToBuy:
                            volToBuy = 1

                        # 남은 돈이 없으면 안 사기
                        if self.remainMoney < priceToBuy * volToBuy:
                            break

                        self.tradeAlgorithm.tradeAI.ReqBuy(code, priceToBuy, volToBuy)
                        curBuyCount += 1
                        self.zapStartTime = stockCur[5]

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        return 

    def OnSuccessBuy(self, reqID, code, price, vol): #매수체결 알림
        self.averagePrice = ((self.averagePrice * self.zapBuyVol) + (price * vol)) / (self.zapBuyVol + vol)
        self.zapBuyVol += vol
        self.remainMoney -= (price * vol) # 남은 돈 갱신

        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        if self.remainMoney <= 0:
            # 이전에 해놓은 모든 주문 취소
            self.tradeAlgorithm.CancelAllReq(self.code)
        return

    def MustGo_DefenseReady(self, stockCur):
        if 0 == self.zapHigh: # 아직 잽매수가 시작되지 않았으면
            return False

        # if self.basisPrice <= stockCur[2]: # 현재가가 중간값 위에 있으면
        #     return False     

        # # 잽매수 구간이 일정이상 폭으로 만들어졌고
        # # 이 구간의 중간값보다 일정이상 하락하면 방어대기로 전환
        # curZapHeight = (self.zapHigh - self.zapLow) / self.zapLow
        # if Constants.MIN_HEIGHT_OF_ZAP_RANGE <= curZapHeight:
        #     distBetweenBasisAndCurPrice = (self.basisPrice - stockCur[2]) / stockCur[2]
        #     if Constants.HEIGHT_FOR_DEFENSE_READY <= distBetweenBasisAndCurPrice:
        #         return True

        # 잽매수 구간이 일정이상 폭으로 만들어졌을 때의 평단보다 현재가가 일정이상 하락하면 
        # 방어대기로 전환
        if self.basisPriceForDefenseReady <= stockCur[2]: # 현재가가 기준값보다 크면
            return False  

        if False == self.isBoxCreated: # 일정크기의 잽매수 구간이 안 만들어졌으면
            return

        dist = (self.basisPriceForDefenseReady - stockCur[2]) / stockCur[2]
        if Constants.HEIGHT_FOR_DEFENSE_READY <= dist:
            return True

        return False

    def MustGo_DefenseSell(self, stockCur):
        # 최소 수익률 이상 상승했으면 방어매도로 전환
        profitsRate = self.GetProfitsRate(self.averagePrice, self.zapBuyVol, stockCur[2])
        if Constants.MIN_PROFITS_RATE_FOR_DEFENSE_BUY <= profitsRate:
            return True

        return False