import time
import win32com.client
import datetime
from datetime import date, timedelta
from PyQt5.QtWidgets import *
import sys
from TradeLib import SendMsgToSlack
from TradeLib import GetGreatStockCodeList
from TradeLib import GetGoodStockCodeList 
from TradeLib import IsGreatBuyTiming, IsGoodBuyTiming, IsSellTiming
from TradeLib import SellStock, BuyStock
from TradeLib import CpStockCur, CpStockMst
from TradeLib import SubscribeConclusionInfo, UnSubscribeConclusionInfo
from TradeLib import StockInfosPerDay, DayStockInfo, GetLowPoints, GetHighPoints, CpStockChart, GetBaseLines, StockInfo
# from StockMonitoring import StockMonitor
from StockMonitoring2 import StockMonitor
import json
import math
from collections import OrderedDict
import pickle
from MyAutoTradeUtil import MyAutoTradeUtil
from MyAutoTradeUtil import Candle
from TradeAI import TradeAIReal, TradeAITest
import keyboard
# import MyAutoTradeTest

# tradeAI = TradeAIReal()
tradeAI = TradeAITest()

subscribed = False
while True:
    if keyboard.is_pressed("Esc"):
        print("프로그램 종료")

        # # 검증
        # chart_3Min = tradeAI.tradeAlgorithm.stockInfos['A004830'].chart_3Min

        # # 서버로부터 3분차트 얻기
        # code = 'A004830'
        # date = '20211102'
        # objChart = CpStockChart()
        # tempDayStockInfos = objChart.RequestMT(code, ord('m'), 3, 600)
        # downloadedChart_3Min = []
        # if tempDayStockInfos:
        #     for i in range(len(tempDayStockInfos))[::-1]:
        #         info = tempDayStockInfos[i]
        #         if info.date == int(date):
        #             # print("날짜", info.date, "시간:", info.time, "시가:", info.open, "고가:", info.high, "저가:", info.low, "종가:", info.close, "전일대비", info.diff, "거래량", info.vol)
        #             downloadedChart_3Min.append(info)

        # # 서버로부터 받은 데이터와 비교검증
        # equal = True
        # for i in range(len(chart_3Min)):
        #     if i == 0: # 첫 번째 캔들은 미완성이므로 무시
        #         continue

        #     candle = chart_3Min[i]

        #     # 비교할 캔들 찾기
        #     find = False
        #     targetCandle = {}
        #     for c in downloadedChart_3Min:
        #         if candle.time == c.time:
        #             find = True
        #             targetCandle = c
        #             break

        #     if find: # 찾았으면
        #         # 같은지 비교
        #         if False == candle.Equal(targetCandle.time, targetCandle.open, targetCandle.high, targetCandle.low, targetCandle.close, targetCandle.vol):
        #             equal = False
        #             break
        #         else:
        #             print(candle.time, " 캔들 같음", "시가:", candle.open, "서버 시가:", targetCandle.open)    

        #     else: # 못찾았으면
        #         equal = False
        #         print(candle.time, "캔들 못찾음")
        #         break

        # if equal:
        #     print("결과: 같음")
        # else:
        #     print("결과: 다름")



        break

    if tradeAI.IsTradeMarketEnded():
        print("장 종료")
        break

    # if False == tradeAI.IsSubscribed() and tradeAI.IsSubscribeTime():
    #     tradeAI.Subscribe()

    if False == tradeAI.IsTradingStarted() and keyboard.is_pressed("q"):
        tradeDate = "20220211"
        tradeAI.StartTrading(tradeDate)

    tradeAI.Update()


