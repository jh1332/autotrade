
def solution(board):
    dirList = [[0, -1], # 위
    [0, 1], # 아래
    [-1, 0], # 왼쪽
    [1, 0], # 오른쪽
    [-1, -1], # 왼쪽 위 대각선 
    [1, -1], # 오른쪽 위 대각선
    [-1, 1], # 왼쪽 아래 대각선
    [1, 1]] # 오른쪽 아래 대각선 

    maxCount = 15

    for i in range(maxCount):
        for j in range(maxCount):
            if 0 == board[i][j]: # 돌이 없으면
                continue

            for k in range(len(dirList)):
                dir = dirList[k]

                x = j
                y = i

                # 4개가 연속으로 같은 돌이면 승리(비교할 돌 제외하고)
                sameCount = 1
                for n in range(5): # 총 비교할 돌 포함 6개가 같은 경우도 있기 때문에 5번 실행
                    x += dir[0]
                    y += dir[1]

                    if x < 0 or y < 0 or maxCount <= x or maxCount <= y:
                        break

                    if board[y][x] != board[i][j]:
                        break

                    sameCount += 1

                if 5 == sameCount:
                    if 1 == board[i][j]: # 흑이면
                        return 1
                    else:
                        return 2


    return 0 # 승리없음



board = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 2, 1, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 2, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

# solution(board)

def solution2(board):
    maxCount = 15

    for i in range(maxCount):
        for j in range(maxCount):
            if 0 != board[i][j]:
                if checker1(board, i,j, board[i][j]):
                    return board[i][j]
                elif checker2(board, i,j,board[i][j]):
                    return board[i][j]
                elif checker3(board, i,j,board[i][j]):
                    return board[i][j]
                elif checker4(board, i,j,board[i][j]):
                    return board[i][j]
                
    return 0

def IsOutOfRange(y, x):
    if x < 0 or 15 <= x or y < 0 or 15 <= y:
        return True
    else:
        return False

def checker1(board, y, x, color):
    if IsOutOfRange(y+1, x-1) or board[y+1][x-1]==color: # 왼쪽 하단
        return 0

    for i in range(5):
        if False == IsOutOfRange(y-i, x+i) and board[y-i][x+i] == color: # 오른쪽 하단
            continue
        else:
             return 0
    
    if IsOutOfRange(y-5, x+5) or board[y-5][x+5] == color:
        return 0

    return 1

def checker2(board, y, x, color):

    if(IsOutOfRange(y, x-1) or board[y][x-1]==color):
        return 0

    for i in range(5):
        if False == IsOutOfRange(y, x+i) and board[y][x+i]==color:
            continue
        else: 
            return 0
    
    if IsOutOfRange(y, x+5) or board[y][x+5] == color:
        return 0

    return 1

def checker3(board, y, x, color):
    if IsOutOfRange(y-1, x-1) or board[y-1][x-1] == color:
        return 0

    for i in range(5):
        if False == IsOutOfRange(y+i, x+i) and board[y+i][x+i] == color:
            continue
        else:
            return 0
    
    if IsOutOfRange(y+5, x+5) or board[y+5][x+5] == color:
        return 0
    return 1

def checker4(board, y, x, color):

    if IsOutOfRange(y-1, x) or board[y-1][x] == color:
        return 0

    for i in range(5):
        if False == IsOutOfRange(y+i, x) and board[y+i][x] == color :
            continue
        else: 
            return 0
    
    if IsOutOfRange(y+5, x) or board[y+5][x] == color:
        return 0
    return 1






solution2(board)


