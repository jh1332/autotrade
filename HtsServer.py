from re import I
from MyAutoTradeUtil import MyAutoTradeUtil
from MyAutoTradeUtil import Candle
import win32com.client
import os, glob
import Constants
import datetime

g_objCpStatus = win32com.client.Dispatch('CpUtil.CpCybos')

class CpStockChart:
    class Candle:
        def __init__(self):
            self.date = 0       # 날짜
            self.time = 0       # 시간
            self.open = 0       # 시가
            self.high = 0       # 고가
            self.low = 0        # 저가
            self.close = 0      # 종가
            self.vol = 0        # 거래량
            self.tradeMoney = 0 # 거래대금
            return

        def IsPlus(self): # 양봉이냐
            return self.open < self.close

        pass

    def __init__(self, caller):
        self.caller = caller
        self.objStockChart = win32com.client.Dispatch("CpSysDib.StockChart")
        return

    def IsConnected(self):
        isConnected = (0 != g_objCpStatus.IsConnect)
        if False == isConnected:
            print("CpStockChart - PLUS가 정상적으로 연결되지 않음.")

        return isConnected

    def ReqChart_Day(self, code, fromDate, toDate):
        # 연결 여부 체크
        if False == self.IsConnected(): 
            return False

        # 요청
        self.objStockChart.SetInputValue(0, code)  # 종목코드
        self.objStockChart.SetInputValue(1, ord('1'))  # 기간으로 받기
        self.objStockChart.SetInputValue(2, toDate)  # To 날짜
        self.objStockChart.SetInputValue(3, fromDate)  # From 날짜
        #self.objStockChart.SetInputValue(4, 500)  # 최근 500일치
        self.objStockChart.SetInputValue(5, [0, 2, 3, 4, 5, 8, 9])  # 날짜,시가,고가,저가,종가,거래량, 거래대금
        self.objStockChart.SetInputValue(6, ord('D'))  # '차트 주기 - 일간 차트 요청
        self.objStockChart.SetInputValue(9, ord('1'))  # 수정주가 사용
        self.objStockChart.BlockRequest()

        rqStatus = self.objStockChart.GetDibStatus()
        rqRet = self.objStockChart.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            print("종목:", code, "통신 이상")
            exit()

        dayCandles = []
        for i in range(self.objStockChart.GetHeaderValue(3)):        
            candle = self.Candle()
            candle.date = self.objStockChart.GetDataValue(0, i)
            candle.open = self.objStockChart.GetDataValue(1, i)
            candle.high = self.objStockChart.GetDataValue(2, i)
            candle.low = self.objStockChart.GetDataValue(3, i)
            candle.close = self.objStockChart.GetDataValue(4, i)
            candle.vol = self.objStockChart.GetDataValue(5, i)
            candle.tradeMoney = self.objStockChart.GetDataValue(6, i)
            dayCandles.append(candle)

        dayCandles.reverse()    

        # for i in range(len(dayCandles)):
        #     print("종목:", code, "날짜:", dayCandles[i].date, "시가:", dayCandles[i].open, "고가:", dayCandles[i].high, "저가:", dayCandles[i].low, "종가:", dayCandles[i].close, "거래량:", dayCandles[i].vol, "거래대금:", dayCandles[i].tradeMoney)

        return dayCandles

    def ReqChart_3Min(self, code, date):
      # 연결 여부 체크
        if False == self.IsConnected(): 
            return False

        # 요청
        self.objStockChart.SetInputValue(0, code)  # 종목코드
        self.objStockChart.SetInputValue(1, ord('2'))  # 개수로 요청
        self.objStockChart.SetInputValue(2, date)  # 요청종료일
        self.objStockChart.SetInputValue(4, 150)  # 요청갯수
        self.objStockChart.SetInputValue(5, [0, 1, 2, 3, 4, 5, 8, 9])  # 날짜,시간, 시가,고가,저가,종가,거래량, 거래대금
        self.objStockChart.SetInputValue(6, ord('m'))  # '차트 주기 - 일간 차트 요청
        self.objStockChart.SetInputValue(7, 3) # 3분봉
        self.objStockChart.SetInputValue(9, ord('1'))  # 수정주가 사용
        self.objStockChart.BlockRequest()

        rqStatus = self.objStockChart.GetDibStatus()
        rqRet = self.objStockChart.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            print("종목:", code, "통신 이상")
            exit()

        candles = []
        for i in range(self.objStockChart.GetHeaderValue(3)):        
            candle = self.Candle()
            candle.date = self.objStockChart.GetDataValue(0, i)

            if candle.date != date:
                continue

            candle.time = self.objStockChart.GetDataValue(1, i)
            candle.open = self.objStockChart.GetDataValue(2, i)
            candle.high = self.objStockChart.GetDataValue(3, i)
            candle.low = self.objStockChart.GetDataValue(4, i)
            candle.close = self.objStockChart.GetDataValue(5, i)
            candle.vol = self.objStockChart.GetDataValue(6, i)
            candle.tradeMoney = self.objStockChart.GetDataValue(7, i)
            candles.append(candle)

        candles.reverse()    

        for i in range(len(candles)):
            print("종목:", code, "날짜:", candles[i].date, "시간:", candles[i].time, "시가:", candles[i].open, "고가:", candles[i].high, "저가:", candles[i].low, "종가:", candles[i].close, "거래량:", candles[i].vol, "거래대금:", candles[i].tradeMoney)

        return candles        

    pass

class ReqBase:
    def __init__(self, htsServer):
        self.htsServer = htsServer
        return

    def Run(self):
        return    
    pass

# 일봉차트 요청 
class ReqChart_Day(ReqBase):
    def __init__(self, htsServer, onEnd, code, fromDate, toDate):
        super().__init__(htsServer)

        self.onEnd = onEnd
        self.code = code
        self.fromDate = fromDate
        self.toDate = toDate
        return

    def Run(self):
        self.htsServer.Proc_ReqChart_Day(self.onEnd, self.code, self.fromDate, self.toDate)
        return     
    pass

# 분봉차트 요청
class ReqChart_Min(ReqBase):
    def __init__(self, htsServer, onEnd, code, date):
        super().__init__(htsServer)

        self.onEnd = onEnd
        self.code = code
        self.date = date
        return

    def Run(self):
        self.htsServer.Proc_ReqChart_3Min(self.onEnd, self.code, self.date)        
        return     
    pass

# 매수 요청
class Req_Buy(ReqBase):
    def Run(self):
        return     
    pass

# 매도 요청
class Req_Sell(ReqBase):
    def Run(self):
        return     
    pass


class HtsServerBase:
    def __init__(self, listener, account):
        self.listener = listener
        self.subscribed = False
        self.account = account
        self.objChart = CpStockChart(self) # 차트 요청 처리자
        self.reqQueue_SellBuy = [] # 주문요청 큐
        self.reqCount_SellBuy = 0 # 주문요청 횟수
        self.reqStartTime_SellBuy = 0 # 주문요청 시작시간

        self.reqQueue_Chart = [] # 시세요청 큐
        self.reqCount_Chart = 0 # 시세요청 횟수
        self.reqStartTime_Chart = 0 # 시세요청 시작시간

        return

    def ReqChart_Day(self, code, fromDate, toDate, onEnd): # 일간차트 요청
        if Constants.REQ_LIMIT_COUNT_CHART <= self.reqCount_Chart: # 제한된 요청횟수를 넘었으면
            # 차트요청을 큐에 넣기
            req = ReqChart_Day(self, onEnd, code, fromDate, toDate)
            self.reqQueue_Chart.append(req)
        else:
            # 차트요청 처리
            self.Proc_ReqChart_Day(onEnd, code, fromDate, toDate)

        return

    def ReqChart_3Min(self, code, date, onEnd): # 3분봉차트 얻기
        if Constants.REQ_LIMIT_COUNT_CHART <= self.reqCount_Chart: # 제한된 요청횟수를 넘었으면
            # 차트요청을 큐에 넣기
            req = ReqChart_Min(self, onEnd, code, date)
            self.reqQueue_Chart.append(req)
        else:
            # 차트요청 처리
            self.Proc_ReqChart_3Min(onEnd, code, date)

        return                

    def Proc_ReqChart_Day(self, onEnd, code, fromDate, toDate): # 일간차트 요청 처리
        result = self.objChart.ReqChart_Day(code, fromDate, toDate)
        self.OnProc_ReqChart()
        if False == result:
            print("일간차트 요청 실패", "종목:", code, "날짜:", fromDate, "부터", toDate, "까지")

        onEnd(code, result)
        return

    def Proc_ReqChart_3Min(self, onEnd, code, date): # 3분차트 요청 처리
        result = self.objChart.ReqChart_3Min(code, date)
        self.OnProc_ReqChart()
        if False == result:
            print("3분차트 요청 실패", "종목:", code, "날짜:", date)

        onEnd(code, result)        
        return

    # 차트요청을 한 후에 호출됨
    def OnProc_ReqChart(self):
        self.reqCount_Chart += 1
        if 1 == self.reqCount_Chart: # 첫 번째 요청이면 요청시작시간 설정
            self.reqStartTime_Chart = datetime.datetime.now()            

    def Update(self):
        # 차트요청 경과시간이 제한된 시간을 지났으면 차트요청횟수 리셋
        if 0 < self.reqCount_Chart:
            elapsed = datetime.datetime.now()- self.reqStartTime_Chart
            if Constants.REQ_LIMIT_TIME < elapsed.seconds:
                self.reqCount_Chart = 0

        # 요청큐에 쌓인 요청들 처리
        if 0 == self.reqCount_Chart and 0 < len(self.reqQueue_Chart):
            while 0 < len(self.reqQueue_Chart):
                if Constants.REQ_LIMIT_COUNT_CHART <= self.reqCount_Chart:
                    break

                self.reqQueue_Chart[0].Run()
                del self.reqQueue_Chart[0]
  
        return

    def Subscribe(self, code, tradeDate): # 구독
        return

    def ReqBuy(self, code, price, vol): # 매수주문
        return

    def CancelReqBuy(self, reqID, code): # 매수주문 취소
        return

    def ReqSell(self, code, price, vol): # 매도주문
        return

    def ReqSell_Market(self, code, vol): # 시장가 매도주문
        return  

    def CancelReqSell(self, reqID, code): # 매도주문 취소
        return                

    def IsSubscribed(self):    
        return self.subscribed

    pass

class HtsServerReal(HtsServerBase):
    def __init__(self, listener, account):
        super().__init__(listener, account)
        return

    def Subscribe(self, code, tradeDate): # 구독
        return

    def ReqBuy(self, code, price, vol): # 매수주문
        return

    def CancelReqBuy(self, reqID, code): # 매수주문 취소
        return      

    def ReqSell(self, code, price, vol): # 매도주문
        return

    def ReqSell_Market(self, code, vol): # 시장가 매도주문
        return        

    def CancelReqSell(self, reqID, code): # 매도주문 취소
        return 

    pass



class SellBuyReq:
    def __init__(self, reqType, id, code, price, vol, order):
        self.reqType = reqType # 0: 매수, 1: 매도
        self.id = id
        self.code = code
        self.price = price
        self.vol = vol
        self.curVol = vol
        self.order = order

        return
    pass

class HtsServerTest(HtsServerBase):
    def __init__(self, listener, account):
        super().__init__(listener, account)

        self.reqID = 0 # 주문ID를 생성하기 위한 변수

        # self.date = "20210914"
        # self.code = 'A042600'

        # # 덕성 11월 2일
        # self.date = '20211102'
        # self.code = 'A004830'

        # # [방어] 멕아이씨에스 12월 10일 
        # self.date = '20211210'
        # self.code = 'A058110'

        # # 대원제약 12월 15일
        # self.date = '20211215'
        # self.code = 'A003220'

        # # 현대바이오 12월 15일
        # self.date = '20211215'
        # self.code = 'A048410'

        # # KPX생명과학
        # self.date = '20211227'
        # self.code = 'A114450'

        # # 로보티즈
        # self.date = '20211227'
        # self.code = 'A108490'        

        # # NPC
        # self.date = '20211227'
        # self.code = 'A004250' 

        # 한전기술
        # 2022년 3월 10일 ~ 11일

        # 에이티세미콘
        # 2022년 3월 15일

        # 한신기계
        # 2022년 3월 15일

        # 인터파크
        # 2022년 3월 16일

        # 랩지노믹스
        # 2022년 3월 17일

        # 중앙에너비스
        # 2022년 3월 22일

        # 한일사료
        # 2022년 3월 23일

        # 2022년 3월 28일
        # 휴림로봇
        # 일동제약

        self.index = 0
        self.indexEnd = 0
        self.tradeCodes = [] # 구독중인 종목코드들
        
        self.tradeUtil = MyAutoTradeUtil()
        self.stockCurList = {} # 종목별 체결데이터 리스트
        self.stockBidList = {} # 종목별 호가창데이터 리스트
        self.sellBuyReqList = {} # 종목별 주문 리스트
        self.curStockBid = {} # 종목별 현재 호가창 정보

        return

    def Subscribe(self, code, tradeDate): # 구독
        for i in range(len(self.tradeCodes)):
            if self.tradeCodes[i] == code:
                print(code, "는 이미 구독중")
                return                

        self.tradeDate = tradeDate
        self.tradeCodes.append(code)
        self.stockCurList[code] = []
        self.stockBidList[code] = []
        self.sellBuyReqList[code] = []

        if False == self.subscribed: # 처음 구독하는 것이면
            self.index = 1
            self.indexEnd = self.GetFileIndexEnd(tradeDate)
            self.subscribed = True        

        return

    def ReqBuy(self, code, price, vol): # 매수주문
        print("매수주문 - ", "code:", code, "가격:", price, "수량:", vol)
        
        # 매수가격 보정
        buyPrice = price
        curStockBid = self.curStockBid[code]
        offer_1 = curStockBid['offer'][0]
        if offer_1 < buyPrice: # 매도1호가 < 매수가격
            buyPrice = offer_1

        # 즉시체결이 가능하면 예약하지 않고 즉시 처리
        if offer_1 <= buyPrice: # 즉시체결이 가능하면
            offerVol_1 = curStockBid['offervol'][0] # 매도1호 대기물량
            if offerVol_1 < 0:
                offerVol_1 = 0

            if vol <= offerVol_1: # 모든 수량 체결가능
                # 매수주문 성공 알림
                self.reqID += 1
                self.account.OnSuccessReqBuy(self.reqID, code, buyPrice, vol)

                # 매수체결 알림
                self.account.OnSuccessBuy(self.reqID, code, buyPrice, vol)

            else: # 일부 수량만 체결가능 또는 체결 불가능이면
                buyableVol = offerVol_1
                if 0 < buyableVol:
                    # 매수주문 성공 알림
                    self.reqID += 1
                    self.account.OnSuccessReqBuy(self.reqID, code, buyPrice, buyableVol)

                    # 매수체결 알림
                    self.account.OnSuccessBuy(self.reqID, code, buyPrice, buyableVol)

                # 매수주문 실패 알림
                noBuyableVol = vol - offerVol_1 # 체결실패 수량
                self.account.OnFailReqBuy(code, buyPrice, noBuyableVol)
            
            return

        # 매수주문정보 생성 및 추가
        order = self.GetStockBidVol(curStockBid, buyPrice) # 매수가격에 해당하는 대기물량이 순번
        self.reqID += 1
        buyReq = SellBuyReq(0, self.reqID, code, buyPrice, vol, order)
        self.sellBuyReqList[code].append(buyReq)

        # 매수주문 성공 알림
        print("****주문순번:", order, "****")
        self.account.OnSuccessReqBuy(self.reqID, code, buyPrice, vol)

        return    

    def CancelReqBuy(self, reqID, code): # 매수주문 취소
        buyReq = {}

        sellBuyReqList = self.sellBuyReqList[code]

        for i in range(len(sellBuyReqList)):
            if reqID == sellBuyReqList[i].id:
                buyReq = sellBuyReqList[i]
                del sellBuyReqList[i]
                break
        
        self.account.OnCancelReqBuy(buyReq.id, buyReq.code, buyReq.price, buyReq.vol)
        return

    def ReqSell(self, code, price, vol): # 매도주문
        print("매도주문 - ", "code:", code, "가격:", price, "수량:", vol)
        
        if self.account.GetInfo(code).vol < vol: # 보유한 수량보다 더 많이 주문했으면
            vol = self.account.GetInfo(code).vol

        # 매도가격 보정
        sellPrice = price
        curStockBid = self.curStockBid[code]
        bid_1 = curStockBid['bid'][0]
        if sellPrice <= bid_1: # 매도가격 <= 매수1호가
            sellPrice = bid_1

        # 즉시체결이 가능하면 예약하지 않고 즉시 처리
        if sellPrice <= bid_1: # 즉시체결이 가능하면
            bidVol_1 = curStockBid['bidvol'][0] # 매수1호 대기물량
            if bidVol_1 < 0:
                bidVol_1 = 0

            if vol <= bidVol_1: # 모든 수량 체결가능
                # 매도주문 성공 알림
                self.reqID += 1
                self.account.OnSuccessReqSell(self.reqID, code, sellPrice, vol)

                # 매도체결 알림
                self.account.OnSuccessSell(self.reqID, code, sellPrice, vol)

            else: # 일부 수량만 체결가능 또는 체결 불가능이면
                sellableVol = bidVol_1
                if 0 < sellableVol:
                    # 매도주문 성공 알림
                    self.reqID += 1
                    self.account.OnSuccessReqSell(self.reqID, code, sellPrice, sellableVol)

                    # 매도체결 알림
                    self.account.OnSuccessSell(self.reqID, code, sellPrice, sellableVol)

                # 매도주문 실패 알림
                noSellableVol = vol - bidVol_1 # 체결실패 수량
                self.account.OnFailReqSell(code, sellPrice, noSellableVol)
            
            return

        # 매도주문정보 생성 및 추가
        order = self.GetStockBidVol(curStockBid, sellPrice) # 매도가격에 해당하는 대기물량이 순번
        self.reqID += 1
        sellReq = SellBuyReq(1, self.reqID, code, sellPrice, vol, order)
        self.sellBuyReqList[code].append(sellReq)

        # 매도주문 성공 알림
        print("****주문순번:", order, "****")
        self.account.OnSuccessReqSell(self.reqID, code, sellPrice, vol)

        return            

    def ReqSell_Market(self, code, vol): # 시장가 매도주문
        print("시장가 매도주문 - ", "code:", code, "수량:", vol)

        if self.account.GetInfo(code).vol < vol: # 보유한 수량보다 더 많이 주문했으면
            vol = self.account.GetInfo(code).vol

        remainVol = vol

        # 매도가 = 현재 호가창에서 가장 높은 매수호가
        bidIndex = 0
        curStockBid = self.curStockBid[code]
        sellPrice = curStockBid['bid'][bidIndex]

        # 
        avgSellPrice = 0
        sellCount = 0
        while 0 < remainVol:
            if remainVol <= curStockBid['bidvol'][bidIndex]: # 남은 주문수량 <= 매도가에 해당하는 매수대기물량
                # 남은 주문수량만큼 매도주문

                # 매도주문 성공 알림
                self.reqID += 1
                self.account.OnSuccessReqSell(self.reqID, code, sellPrice, remainVol)

                # 매도체결 알림
                self.account.OnSuccessSell(self.reqID, code, sellPrice, remainVol)
                avgSellPrice += sellPrice
                sellCount += 1

                # 매도완료 알림
                self.account.OnCompleteSell(self.reqID, code, sellPrice, vol)

                # 시장가 매도완료 알림
                avgSellPrice = avgSellPrice / sellCount
                self.account.OnCompleteSellMarket(code, avgSellPrice, vol)
                remainVol = 0

            else:    
                # 주문가능 수량만큼 매도주문
                sellableVol = curStockBid['bidvol'][bidIndex] # 주문가능수량 = 매도가에 해당하는 매수대기물량
                if sellableVol <= 0:
                    bidIndex += 1
                    sellPrice = curStockBid['bid'][bidIndex]
                    continue

                # 매도주문 성공 알림
                self.reqID += 1
                self.account.OnSuccessReqSell(self.reqID, code, sellPrice, sellableVol)

                # 매도체결 알림
                self.account.OnSuccessSell(self.reqID, code, sellPrice, sellableVol)
                avgSellPrice += sellPrice
                sellCount += 1

                # 매도완료 알림
                self.account.OnCompleteSell(self.reqID, code, sellPrice, sellableVol)

                #
                remainVol -= sellableVol # 남은 주문수량 -= 주문가능수량
                
                # 그 아래의 매수호가를 다음 매수가로 설정
                bidIndex += 1
                sellPrice = curStockBid['bid'][bidIndex]
    
        return

    def CancelReqSell(self, reqID, code): # 매도주문 취소
        sellReq = {}

        sellBuyReqList = self.sellBuyReqList[code]

        for i in range(len(sellBuyReqList)):
            if reqID == sellBuyReqList[i].id:
                sellReq = sellBuyReqList[i]
                del sellBuyReqList[i]
                break
        
        self.account.OnCancelReqSell(sellReq.id, sellReq.code, sellReq.price, sellReq.vol)
        return        
         

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        self.account.OnUpdateStockCur(code, stockCur)
        self.listener.OnUpdateStockCur(code, stockCur)

        price = stockCur[2]
        vol = -1 * stockCur[4]

        # 임시호가창 갱신
        if self.curStockBid.get(code):
            self.UpdateCurStockBidVol(self.curStockBid[code], price, vol)

        # 
        removeReqList = [] # 제거할 주문리스트

        sellBuyReqList = self.sellBuyReqList[code]

        tempSellBuyReqList = list(sellBuyReqList)
        for i in range(len(tempSellBuyReqList)):
            # 주문이 리스트에 존재하는지 확인
            req = tempSellBuyReqList[i]
            isReqExisted = False
            for j in range(len(sellBuyReqList)):
                if sellBuyReqList[j].id == req.id:
                    isReqExisted = True
                    break            

            if False == isReqExisted:
                continue

            #
            if req.price == price: # 현재가에 주문을 넣었으면
                req.order += vol # 순번 -= 체결수량
                if req.order < 0: # 체결이 가능하면
                    if req.curVol <= abs(req.order): # 모든 물량이 체결가능하면
                        # 체결 알림
                        if 0 == req.reqType: # 매수주문
                            # 매수체결 알림
                            self.account.OnSuccessBuy(req.id, code, req.price, req.curVol)
                        else: # 매도주문
                            # 매도체결 알림
                            self.account.OnSuccessSell(req.id, code, req.price, req.curVol)

                        # # 주문정보를 제거하기 위해 제거할 리스트에 추가
                        # removeReqList.append(req)

                        # 완료 알림
                        index = -1
                        for j in range(len(sellBuyReqList)):
                            if sellBuyReqList[j].id == req.id:
                                index = j
                                break

                        #
                        completedReq = sellBuyReqList[index]    

                        # 완료된 주문 제거
                        del sellBuyReqList[index]

                        if 0 == completedReq.reqType: # 매수주문 체결이 완료됐으면
                            self.account.OnCompleteBuy(completedReq.id, code, completedReq.price, completedReq.vol)
                        else: # 매도주문 체결이 완료됐으면
                            self.account.OnCompleteSell(completedReq.id, code, completedReq.price, completedReq.vol)
                                                 
                    else: # 일부 물량만 체결가능하면
                        # 체결 알림
                        tempVol = abs(req.order) # 체결수량
                        req.order = 0
                        req.curVol -= tempVol # 남은 수량 -= 체결수량
                     
                        if 0 == req.reqType: # 매수주문
                            # 매수체결 알림
                            self.account.OnSuccessBuy(req.id, code, req.price, tempVol) 
                        else: # 매도주문
                            # 매도체결 알림
                            self.account.OnSuccessSell(req.id, code, req.price, tempVol)

        # # 완료된 주문정보 제거
        # for i in range(len(removeReqList)):
        #     index = -1
        #     for j in range(len(tempSellBuyReqList)):
        #         if tempSellBuyReqList[j].id == removeReqList[i].id:
        #             index = j
        #             break
            
        #     completedReq = tempSellBuyReqList[index]
        #     if 0 == completedReq.reqType: # 매수주문 체결이 완료됐으면
        #         self.account.OnCompleteBuy(completedReq.id, code, completedReq.price, completedReq.vol)
        #     else: # 매도주문 체결이 완료됐으면
        #         self.account.OnCompleteSell(completedReq.id, code, completedReq.price, completedReq.vol)

        #     #
        #     index = -1
        #     for j in range(len(self.sellBuyReqList)):
        #         if self.sellBuyReqList[j].id == completedReq.id:
        #             index = j
        #             break

        #     if 0 <= index:
        #         del self.sellBuyReqList[index]

        return

    # 매수주문을 걸어놓은 가격대의 매물대에 변화가 있을 때 예약된 매수주문순번 갱신
    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터
        self.listener.OnUpdateStockBid(code, stockBid)

        # 임시호가창 정보와 현재호가창의 같은 호가대기물량을 각각 비교해서 매수주문 순번 갱신
        sellBuyReqList = self.sellBuyReqList[code]
        for i in range(len(sellBuyReqList)):
            buyReq = sellBuyReqList[i]
            tempBidVol = self.GetStockBidVol(self.curStockBid[code], buyReq.price)
            if tempBidVol is None:
                continue

            bidVol = self.GetStockBidVol(stockBid, buyReq.price)
            if bidVol is None:
                continue

            # 내 앞의 누군가가 일부 매수대기물량을 취소했으면
            # (사실 내 앞이 아니라 뒤의 대기물량이 취소된 것을 수도 있지만 그것을 판단할 수 없으니 무시하자)
            if bidVol < tempBidVol:
                buyReq.order -= (tempBidVol - bidVol)
                if buyReq.order < 0:
                    buyReq.order = 0

        # 임시호가창 갱신
        self.curStockBid[code] = stockBid
        return

    def Update(self):
        super().Update()

        if False == self.subscribed: # 아직 구독 안 했으면
            return

        # 모든 매매 종목의 체결 또는 호가창 데이터가 비었는지 확인
        isAllStockCurEmpty = True
        for i in range(len(self.tradeCodes)):
            code = self.tradeCodes[i]
            if 0 != len(self.stockCurList[code]):
                isAllStockCurEmpty = False
                break
        
        isAllStockBidEmpty = True
        for i in range(len(self.tradeCodes)):
            code = self.tradeCodes[i]
            if 0 != len(self.stockBidList[code]):
                isAllStockBidEmpty = False
                break
    
        #
        if isAllStockCurEmpty or isAllStockBidEmpty: # 모든 종목의 체결 또는 호가창 데이터가 비었으면   
            if self.index <= self.indexEnd: # 로드할 파일이 남아있으면
                # 파일로부터 얻은 체결, 호가창 데이터를 추가
                filePath = self.GetFileFullPath(self.tradeDate, self.index)
                dataList = self.tradeUtil.LoadFile(filePath)
                for i in range(len(self.tradeCodes)):
                    code = self.tradeCodes[i]
                    tradeData = self.FindTradeData(dataList, code)
                    if tradeData is not None:
                        self.stockCurList[code].extend(tradeData['stockCur']) # 체결 데이터 추가
                        self.stockBidList[code].extend(tradeData['stockBid']) # 호가창 데이터 추가

                self.index += 1
            else:
                print("더이상 읽을 매매 데이터 파일 없음")
                exit()

        # 
        for i in range(len(self.tradeCodes)):
            code = self.tradeCodes[i]
            stockCurList = self.stockCurList[code]
            stockBidList = self.stockBidList[code]
            if 0 < len(stockCurList) and 0 < len(stockBidList): # 체결, 호가창 데이터 둘다 있으면
                cvolStockCur = stockCurList[0][1]
                cvolStockBid = stockBidList[0]['vol']
                if cvolStockCur <= cvolStockBid: # 체결 데이터의 누적거래량 <= 호가창 데이터의 누적거래량
                    self.OnUpdateStockCur(code, stockCurList[0])
                    del stockCurList[0]
                else:
                    self.OnUpdateStockBid(code, stockBidList[0])
                    del stockBidList[0]   
            else:
                if 0 < len(stockCurList): # 체결 데이터만 있으면
                    self.OnUpdateStockCur(code, stockCurList[0])
                    del stockCurList[0]
                elif 0 < len(stockBidList): # 호가창 데이터만 있으면
                    self.OnUpdateStockBid(code, stockBidList[0])
                    del stockBidList[0]   

 

        # for i in range(len(stockCurList)):
            # stockCur = stockCurList[i]
            # cvol = stockCur[1] # 누적거래량
            # price = stockCur[2] # 현재가
            # vol = stockCur[4] # 체결수량
            # time = stockCur[5] # 체결시간(초)

            # print("체결", "시간:", time, "누적거래량:", cvol, "체결수량:", vol, "현재가", price)
        
        # for i in range(len(stockBidList)):
        #     stockBid = stockBidList[i]
        #     time = stockBid['time'] # 시간
        #     cvol = stockBid['vol'] # 누적거래량
        #     offerList = stockBid['offer'] # 매도대기 가격
        #     offervolList = stockBid['offervol'] # 매도대기 물량
        #     totalOffer = stockBid['totalOffer'] # 총 매도대기 물량
        #     bidList = stockBid['bid'] # 매수대기 가격
        #     bidvolList = stockBid['bidvol'] # 매수대기 물량
        #     totalBid = stockBid['totalBid'] # 총 매수대기 물량

            # print('호가', '시간', time, '누적거래량', cvol)           

        return

    def GetFileIndexEnd(self, tradeDate): # 마지막 파일이름 뒤에 붙은 인덱스 얻기
        # 날짜에 해당하는 파일들의 끝파일 숫자를 얻기

        filePath = self.GetFilePath(tradeDate)
        condition = filePath + "/" + tradeDate + "*" + ".p"
        files = glob.glob(condition)

        return len(files)

    def GetFilePath(self, tradeDate):
        filePath = "./매매 데이터"
        dirPath = "./매매 데이터/" + tradeDate
        if os.path.isdir(dirPath): # 날짜의 폴더가 있으면 그 폴더읽기
            filePath = dirPath

        return filePath 

    def GetFileFullPath(self, tradeDate, index):
        filePath = self.GetFilePath(tradeDate)

        return filePath + "/" + tradeDate + "_" + str(index) + ".p"

    def FindTradeData(self, tradeDataList, code):
        targetTradeData = None
        for tradeData in tradeDataList:
            if tradeData['code'] == code:
                targetTradeData = tradeData
                break
        return targetTradeData

    def GetOfferVolIndex(self, stockBid, price): # 가격에 해당하는 매도대기 인덱스 구하기
        index = -1

        if False == ('offer' in stockBid):
            return index

        for i in range(len(stockBid['offer'])):
            if stockBid['offer'][i] == price:
                index = i
                break

        return index

    def GetBidVolIndex(self, stockBid, price): # 가격에 해당하는 매수대기 인덱스 구하기
        index = -1

        if False == ('bid' in stockBid):
            return index

        for i in range(len(stockBid['bid'])):
            if stockBid['bid'][i] == price:
                index = i
                break

        return index

    def GetStockBidVol(self, stockBid, price): # 가격에 해당하는 대기물량 구하기
        index = self.GetBidVolIndex(stockBid, price)
        if 0 <= index: # 찾았으면
            return stockBid['bidvol'][index]

        index = self.GetOfferVolIndex(stockBid, price)
        if 0 <= index: # 찾았으면
            return stockBid['offervol'][index]        

        return

    def UpdateCurStockBidVol(self, stockBid, price, vol): # 가격에 해당하는 대기물량 갱신
        index = self.GetBidVolIndex(stockBid, price)
        if 0 <= index: # 찾았으면
            stockBid['bidvol'][index] += vol
            return

        index = self.GetOfferVolIndex(stockBid, price)
        if 0 <= index: # 찾았으면
            stockBid['offervol'][index] += vol

        return

    pass