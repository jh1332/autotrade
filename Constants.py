# 요청 제한
REQ_LIMIT_TIME = 15 # 요청 제한시간
REQ_LIMIT_COUNT_SELL_BUY = 15 # 주문요청 제한 횟수. 원래는 20
REQ_LIMIT_COUNT_CHART = 55 # 시세요청 제한 횟수. 원래는 60


# 세금과 수수료
BUY_TAX = 0.00015
SELL_TAX = 0.00015
TRANSACTION_TAX = 0.0023

# 매매할 종목 선정할 때 분석할 일봉차트 기간
DAY_CHART_RANGE_FOR_SELECTING_TRADE_STOCK = 180

# 종목당 투입할 총금액
TOTAL_MONEY_PER_STOCK = 10000000

# 최소수익률
MIN_PROFITS_RATE = 2

# 수익매도대기 상태로 전환될 때 초기에 매도할 비중의 비율
FIRST_PLUS_SELL_VOL_RATE = 0.5 # 비중의 절반

# 수익매도대기 상태에서 나머지 비중을 모두 매도할지 판단할 수익률의 비율
RATE_FOR_COMPLETED_PLUS_SELL = 0.5 # 현재 최고수익률의 절반

# 잽매수 구간의 최소 폭(%)
MIN_HEIGHT_OF_ZAP_RANGE = 0.03

# 잽매수할 최대금액 비율
MAX_MONEY_RATE_FOR_ZAP_BUY = 0.3

# 잽매수 주문 건당 최대금액 대비 금액비율
MONEY_RATE_PER_ZAP_REQ = 0.01

# 방어대기 전환을 위한 잽구간 중간값 대비 하락 폭
HEIGHT_FOR_DEFENSE_READY = 0.01

# 방어대기를 할 때 최초에 매도할 비중의 비율
SELL_VOL_RATE_ENTERING_DEFENSE_READY = 0.5

# 방어잽매수 최대금액 비율
MAX_MONEY_RATE_FOR_DEFENSE_ZAP = 0.5

# 방어잽매수 최소수익률
MIN_PROFITS_RATE_FOR_DEFENSE_BUY = 1

# 방어매도에 진입했을 때 매도할 비중 비율
FIRST_SELL_VOL_RATE_DEFENSE_SELL = 0.5

