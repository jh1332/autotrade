import os
import pickle

path_dir = r"C:\Repository\AutoTrade\매매 데이터"

def LoadFile(filePath):
    data = {}
    with open(filePath, 'rb') as file:
        data = pickle.load(file)

    return data 


# codes = {}
# codes['1'] = { 'name' : '종목1', 'count' : 1 }
# codes['2'] = { 'name' : '종목2', 'count' : 10 }

# codeDatas = []
# for k, v in codes.items():
#     codeDatas.append({'code': k, 'name':v['name'], 'count':v['count']})

# codeDatas.sort(key = lambda x : x['count'], reverse=True)




codes = {}
folder_list = os.listdir(path_dir)
for folder in folder_list:
    folderPath = path_dir + '\\' + folder
    if os.path.isfile(folderPath):
        continue

    file_list = os.listdir(folderPath)
    if 0 == len(file_list):
        continue

    filePath = folderPath + '\\' + file_list[0]
    dataList = LoadFile(filePath)

    for data in dataList:
        if data.get('stockCur') and 0 < len(data['stockCur']):
            price = int(data['stockCur'][0][2])
            if price <= 1500 or 100000 <= price: # 1500원 이하 또는 10만원 이상은 무시
                continue

        code = data['code']
        if codes.get(code):
            codes[code]['count'] += 1   
            continue

        codes[code] = { 'name' : data['name'], 'count' : 0 }

# 중복이 많이 된 순으로 정렬
codeDatas = []
for k, v in codes.items():
    codeDatas.append({'code': k, 'name':v['name'], 'count':v['count']})

codeDatas.sort(key = lambda x : x['count'], reverse=True)

print("총 종목수: ", len(codes))
txtData = ""
for data in codeDatas:
    # data = {'code': k, 'name':v['name'], 'count':v['count'] }
    txtData = txtData + '코드: ' + data['code'] + ' 이름: ' + data['name'] + ' 중복수: ' + str(data['count']) + '\n' 

codeFilePath = path_dir + '\\' + "종목.txt"
file = open(codeFilePath, 'w')
file.write(txtData)
file.close()


