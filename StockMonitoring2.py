import sys
from PyQt5.QtWidgets import *
import win32com.client
import ctypes
from TradeLib import SendMsgToSlack 
import time
from datetime import date, timedelta
from TradeLib import StockInfosPerDay, DayStockInfo, GetLowPoints, GetHighPoints, CpStockChart, GetBaseLines, StockInfo
import json

################################################
# PLUS 공통 OBJECT
g_objCodeMgr = win32com.client.Dispatch('CpUtil.CpCodeMgr')
g_objCpStatus = win32com.client.Dispatch('CpUtil.CpCybos')
g_objCpTrade = win32com.client.Dispatch('CpTrade.CpTdUtil')

#####################################################################################
SLACK_TOKEN = "xoxb-1974644675939-1987050986353-NJjMu7Q32kjke0PpLKeGrzGd"
SLACK_CHANNEL = "#stock"

# 메시지를 슬랙으로 보내기
def SendMsg(msg):
    SendMsgToSlack(SLACK_TOKEN, SLACK_CHANNEL, msg)
    pass

#####################################################################################


 
################################################
# PLUS 실행 기본 체크 함수
def InitPlusCheck():
    # 프로세스가 관리자 권한으로 실행 여부
    if ctypes.windll.shell32.IsUserAnAdmin():
        print('정상: 관리자권한으로 실행된 프로세스입니다.')
    else:
        print('오류: 일반권한으로 실행됨. 관리자 권한으로 실행해 주세요')
        return False
 
    # 연결 여부 체크
    if (g_objCpStatus.IsConnect == 0):
        print("PLUS가 정상적으로 연결되지 않음. ")
        return False
 
    # # 주문 관련 초기화
    # if (g_objCpTrade.TradeInit(0) != 0):
    #     print("주문 초기화 실패")
    #     return False
 
    return True
 
 
################################################
# CpEvent: 실시간 이벤트 수신 클래스
class CpEvent:
    def set_params(self, client, name, caller):
        self.client = client  # CP 실시간 통신 object
        self.name = name  # 서비스가 다른 이벤트를 구분하기 위한 이름
        self.caller = caller  # callback 을 위해 보관
 
    def OnReceived(self):
        # 실시간 처리 - 현재가 주문 체결
        if self.name == 'stockcur':
            code = self.client.GetHeaderValue(0)  # 종목코드
            name = self.client.GetHeaderValue(1)  # 종목명
            diff = self.client.GetHeaderValue(2)  # 전일대비
            vol = self.client.GetHeaderValue(9)  # 누적거래량 
            cprice = self.client.GetHeaderValue(13)  # 현재가
            cVolType = self.client.GetHeaderValue(14) # 체결상태 1: 매수 2: 매도
            cVol = self.client.GetHeaderValue(17)  # 순간체결수량
            timess = self.client.GetHeaderValue(18)  # 체결 시간(초)
            exFlag = self.client.GetHeaderValue(19)  # 예상체결 플래그
 
            if exFlag == ord('2'): 
                item = {}
                item['code'] = code
                item['time'] = timess
                item['diff'] = diff
                item['cur'] = cprice
                item['vol'] = vol
    
                # 데이터 수집할 때는 주석처리
                # # 현재가 업데이트
                # self.caller.updateCurData(item)

            self.caller.OnUpdateStockCur(code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag)

        elif self.name == "stockbid": # 10호가창
           # 현재가 10차 호가 데이터 실시간 업데이c
            code = self.client.GetHeaderValue(0)
            time = self.client.GetHeaderValue(1) # 시간
            vol = self.client.GetHeaderValue(2) # 거래량
            dataindex = [3, 7, 11, 15, 19, 27, 31, 35, 39, 43]
            obi = 0
            offer = []
            bid = []
            offervol = []
            bidvol = []
            for i in range(10):
                offer.append(self.client.GetHeaderValue(dataindex[i])) # n차 매도호가
                bid.append(self.client.GetHeaderValue(dataindex[i] + 1)) # n차 매수호가
                offervol.append(self.client.GetHeaderValue(dataindex[i] + 2)) # n차 매도잔량
                bidvol.append(self.client.GetHeaderValue(dataindex[i] + 3)) # n차 매수잔량
 
            totOffer = self.client.GetHeaderValue(23) # 총매도잔량
            totBid = self.client.GetHeaderValue(24) # 총매수잔량

            codeName = g_objCodeMgr.CodeToName(code)
            # print(codeName, "총매도잔량:", str(totOffer), "총매수잔량:", str(totBid), "1차 매도호가:", str(offer[0]), "1차 매수호가:", str(bid[0]))

            self.caller.OnUpdateStockBid(code, time, vol, offer, offervol, bid, bidvol, totOffer, totBid)

        elif self.name == "stockSvr": # 투자자 매매동향
            personSell = self.client.GetDataValue(0, 0)  # 개인 매도거래량
            personBuy = self.client.GetDataValue(2, 0)  # 개인 매수거래량

            foreignerSell = self.client.GetDataValue(0, 1)  # 외국인 매도거래량
            foreignerBuy = self.client.GetDataValue(2, 1)  # 외국인 매수거래량

            companySell = self.client.GetDataValue(0, 2)  # 기관 매도거래량
            companyBuy = self.client.GetDataValue(2, 2)  # 기관 매수거래량            

            # print("개인매도:", personSell, "개인매수:", personBuy, "외국인매도:", foreignerSell, "외국인매수:", foreignerBuy, "기관매도:", companySell, "기관매수:", companyBuy)

        return
 
 
################################################
# plus 실시간 수신 base 클래스
class CpPublish:
    def __init__(self, name, serviceID):
        self.name = name
        self.obj = win32com.client.Dispatch(serviceID)
        self.bIsSB = False
 
    def Subscribe(self, var, caller):
        if self.bIsSB:
            self.Unsubscribe()
 
        if (len(var) > 0):
            self.obj.SetInputValue(0, var)
 
        handler = win32com.client.WithEvents(self.obj, CpEvent)
        handler.set_params(self.obj, self.name, caller)
        self.obj.Subscribe()
        self.bIsSB = True
 
    def Unsubscribe(self):
        if self.bIsSB:
            self.obj.Unsubscribe()
        self.bIsSB = False
 
 
################################################
# CpPBStockCur: 실시간 현재가 요청 클래스
class CpPBStockCur(CpPublish):
    def __init__(self):
        super().__init__('stockcur', 'DsCbo1.StockCur')
 

# CpPBStockBid: 실시간 10차 호가 요청 클래스
class CpPBStockBid(CpPublish):
    def __init__(self):
        super().__init__("stockbid", "Dscbo1.StockJpBid") 

# CpPBStockSvr: 투자자 매매 동향 요청 클래스
class CpPBStockSvr(CpPublish):
    def __init__(self):
        super().__init__("stockSvr", "CpSysDib.CpSvrNew7221S")        

################################################
# 분 차트 
class CMinchartData:
    caller = 0

    def __init__(self):
        self.minDatas = {}
        self.yesterdayMinDatas = {}
        self.threeMinDatas = {}
        self.objCur = {}
        self.objOfferbid = {}
        self.objCurSvr = {}
 
    def stop(self):
        for k,v in self.objCur.items() :
            v.Unsubscribe()

        for k,v in self.objOfferbid.items():
            v.Unsubscribe()

        for k,v in self.objCurSvr.items():
            v.Unsubscribe()            
 
 
    def addCode(self, code, caller): 
        self.caller = caller

        if self.minDatas.get(code) == None:
            self.minDatas[code] = [] 

        self.threeMinDatas[code] = []
        self.objCur[code] = CpPBStockCur()
        self.objOfferbid[code] = CpPBStockBid()
        self.objCurSvr[code] = CpPBStockSvr()

        self.objCur[code].Subscribe(code, self)
        self.objOfferbid[code].Subscribe(code, self)
        # self.objCurSvr[code].Subscribe("U201", self)
 
 
    def updateCurData(self, item):
        code = item['code']
        time  = item['time']
        cur = item['cur']
        vol = item['vol']
        self.makeMinchart(code, time, cur, vol)

    def UpdateToday1MinChart(self, code, dayStockInfos):
        logs = []

        for s in dayStockInfos:
            if self.minDatas.get(code) == None:
                self.minDatas[code] = [] 

            candle = 0
            for c in self.minDatas[code]:
                if c[0] == s.time: # 이미 캔들이 존재하면
                    c[1] = s.open
                    if c[2] < s.high:
                        c[2] = s.high
                    if s.low < c[3]:
                        c[3] = s.low

                    candle = c
            else:
                candle = [s.time, s.open, s.high, s.low, s.close, s.vol]
                self.minDatas[code].append(candle)
                # print("시간:", s.time, "시가:", s.open, "고가:", s.high, "저가:", s.low, "종가:", s.close, "거래량:", s.vol)

            log = "시간:" + str(candle[0]) + " 시가:" + str(candle[1]) + " 고가:" + str(candle[2]) + " 저가:" + str(candle[3]) + " 종가:" + str(candle[4]) + " 거래량:" + str(candle[5])    
            logs.append(log)

        return logs


    def UpdateYesterday1MinChart(self, code, dayStockInfos):
        for s in dayStockInfos:
            if self.yesterdayMinDatas.get(code) == None:
                self.yesterdayMinDatas[code] = []

            self.yesterdayMinDatas[code].append([s.time, s.open, s.high, s.low, s.close, s.vol])
            # print("시간:", s.time, "시가:", s.open, "고가:", s.high, "저가:", s.low, "종가:", s.close, "거래량:", s.vol)

        return
 
    def OnUpdateStockCur(self, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag):
        # print("code:", code, "price:", cprice, "time:", timess, "cVolType:", cVolType, "exFlag:", exFlag)

        if exFlag == ord('2'): 
            item = {}
            item['code'] = code
            item['time'] = timess
            item['diff'] = diff
            item['cur'] = cprice
            item['vol'] = cVol

            # 데이터 수집할 때는 분차트 만들지 않기
            # # 현재가 업데이트
            # self.updateCurData(item)

        cVolTypeTitle = ""
        if cVolType ==  ord('1'):
            cVolTypeTitle = "매수"
        else:
            cVolTypeTitle = "매도"

        # if (exFlag == ord('1')):  # 동시호가 시간 (예상체결)
        #     print("실시간(예상체결)", name, timess, "*", cprice, "대비", diff, cVolTypeTitle, "체결량", cVol, "거래량", vol)
        # elif (exFlag == ord('2')):  # 장중(체결)
        #     print("실시간(장중 체결)", name, "시간", timess, "현재가", cprice, "대비", diff, cVolTypeTitle, "체결량", cVol, "거래량", vol)       
 
        self.caller.OnUpdateStockCur(self, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag)
        return

    def OnUpdateStockBid(self, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):
        self.caller.OnUpdateStockBid(self, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid)
        return        

    # 해당 종목의 캔들이 하나라도 생성됐는지 검사
    def HasCandle(self, code):
        has = False

        if code in self.minDatas and 1 < len(self.minDatas[code]):
            has = True

        return has

    def GetAverageCloseMin20(self, code, hhmm):
        # try:
        #     totalClose = 0
        #     lastIndex = -1
        #     for e in self.minDatas[code]:
        #         if e[0] == hhmm:
        #             lastIndex = self.minDatas[code].index(e)
        #             break       

        #     if lastIndex < 0:
        #         return -1

        #     startIndex = lastIndex - 20 + 1
        #     if 0 <= startIndex:
        #         for i in range(startIndex, lastIndex + 1):
        #             totalClose = totalClose + self.minDatas[code][i][4]  
        #     else:
        #         for i in range(0, lastIndex + 1):
        #             totalClose = totalClose + self.minDatas[code][i][4]              

        #         yesterdayLastIndex = abs(startIndex) - 1
        #         for i in range(0, yesterdayLastIndex + 1):
        #             totalClose = totalClose + self.yesterdayMinDatas[code][i][4]              

        #     return totalClose / 20
        # except:
        #     return -1

        totalClose = 0
        lastIndex = -1
        for e in self.minDatas[code]:
            if e[0] == hhmm:
                lastIndex = self.minDatas[code].index(e)
                break       

        if lastIndex < 0:
            return -1

        startIndex = lastIndex - 20 + 1
        if 0 <= startIndex:
            for i in range(startIndex, lastIndex + 1):
                totalClose = totalClose + self.minDatas[code][i][4]  
        else:
            for i in range(0, lastIndex + 1):
                totalClose = totalClose + self.minDatas[code][i][4]              

            yesterdayLastIndex = abs(startIndex) - 1
            for i in range(0, yesterdayLastIndex + 1):
                totalClose = totalClose + self.yesterdayMinDatas[code][i][4]              

        return totalClose / 20


    def IsRightDir(self, code, time, logs):
        COUNT = 20
        MIN_DIFF_RATE = 0.007
        # MIN_DIFF_RATE = 0.006
        # 현재캔들을 포함한 20개 캔들의 종가의 평균이 우상향인지 확인

        hhmm = self.MakeHHMM(time)
        curAverageCloseMin20 = self.GetAverageCloseMin20(code, hhmm)
        if curAverageCloseMin20 < 0:
            return False

        # 현재 캔들 앞에서 10번째 캔들의 20분 종가평균을 구함
        targetAverageCloseMin20 = 0
        index = -1
        for e in self.minDatas[code]:
            if e[0] == hhmm:
                index = self.minDatas[code].index(e)
                break

        if index < 0:
            print(code, time, hhmm, "에 해당하는 캔들 없음")
            return False    

        targetIndex = index - 10 + 1            
        targetAverageCloseMin20 = 0

        if 0 <= targetIndex:
            for i in range(targetIndex, index):
                targetHHMM = self.minDatas[code][i][0]
                averageCloseMin20 = self.GetAverageCloseMin20(code, targetHHMM)

                if averageCloseMin20 < 0:
                    return False

                if curAverageCloseMin20 <= averageCloseMin20:
                    return False  

                if i == targetIndex:
                    targetAverageCloseMin20 = averageCloseMin20

                logs.append([targetHHMM, averageCloseMin20])    
        else:
            totalClose = 0
            targetIndex = abs(targetIndex) - 1
            lastIndex = targetIndex + 20 - 1
            try:
                for i in range(targetIndex, lastIndex + 1):
                    totalClose = totalClose + self.yesterdayMinDatas[code][i][4]

                targetAverageCloseMin20 = totalClose / 20    
            except:
                print("과거캔들 인덱스 없음", "갯수:", len(self.yesterdayMinDatas[code]), "시작인덱스:", targetIndex, "끝인덱스:", lastIndex)
                return False
        

        # print(hhmm, "캔들 20분평균선:", curAverageCloseMin20, "앞10번째 캔들 20분평균선:", targetAverageCloseMin20)




        baseValue = targetAverageCloseMin20 + (targetAverageCloseMin20 * MIN_DIFF_RATE)    
        if baseValue <= curAverageCloseMin20:
            return True
        else:        
            return False

    def HasCandleByHHMM(self, code, hhmm):
        if self.HasCandle(code) == False:
            return False

        has = False

        for e in self.minDatas[code]:
            if e[0] == hhmm:
                has = True
                break  

        return has

    def GetHHMMBeforeMin_1ByTime(self, time):
        hhmm = self.MakeHHMM(time)
        return self.GetHHMMBeforeMin_1(hhmm)

    def GetHHMMBeforeMin_1(self, hhmm):
        hh, mm = divmod(hhmm, 100)
        if mm == 0:
            hh = hh - 1
            mm = 59
            hhmm = hh * 100 + mm
        else:
            hhmm = hhmm - 1
            
        return hhmm

    def GetCandleByTime(self, code, time):
        hhmm = self.MakeHHMM(time)
        return self.GetCandle(code, hhmm)

    def GetCandle(self, code, hhmm):
        candle = []

        for e in self.minDatas[code]:
            if e[0] == hhmm:
                candle = e
                break  

        return candle 

    def IsMinusCandle(self, open, close):
        return close <= open 

    def GetMinusCandlesInFrontOf(self, code, baseHHMM):
        candles = []

        index = -1
        for e in self.minDatas[code]:
            if e[0] == baseHHMM:
                index = self.minDatas[code].index(e)
                break

        if index <= 0:
            return candles

        for i in range(index - 1, -1, -1):
            c = self.minDatas[code][i]
            if self.IsMinusCandle(c[1], c[4]) == False:
                break

            candles.append(c)         

        return candles
 
    def MakeHHMM(self, time):
        # 예: 93055   시:9, 분: 30, 초: 55
        hh, mm = divmod(time, 10000)
        mm, tt = divmod(mm, 100)

        mm += 1 # 예: 09:30:55에 들어온 데이터는 09:31분 봉의 데이터이기 때문에 분에 1을 더함
        if (mm == 60) :
            hh += 1
            mm = 0
 
        hhmm = hh * 100 + mm # 예: hh:9, mm:30, hhmm:930

        if hhmm > 1530:
            hhmm = 1530

        return hhmm   

    def MakeHHMM_3Min(self, time):
        hh, mm = divmod(time, 10000)
        mm, tt = divmod(mm, 100)

        mm += 1
        if (mm == 60) :
            hh += 1
            mm = 0

        hhmm = 0
        if 1 <= mm and mm <= 3: # xx:1 ~ xx:3
            hhmm = hh * 100 + 3
        elif 4 <= mm and mm <= 6: # xx:4 ~ xx:6
            hhmm = hh * 100 + 6            
        elif 7 <= mm and mm <= 9: # xx:7 ~ xx:9
            hhmm = hh * 100 + 9
        elif 10 <= mm and mm <= 12: # xx:10 ~ xx:12
            hhmm = hh * 100 + 12
        elif 13 <= mm and mm <= 15: # xx:13 ~ xx:15
            hhmm = hh * 100 + 15
        elif 16 <= mm and mm <= 18: # xx:16 ~ xx:18
            hhmm = hh * 100 + 18 
        elif 19 <= mm and mm <= 21: # xx:19 ~ xx:21
            hhmm = hh * 100 + 21    
        elif 22 <= mm and mm <= 24: # xx:22 ~ xx:24
            hhmm = hh * 100 + 24
        elif 25 <= mm and mm <= 27: # xx:25 ~ xx:27
            hhmm = hh * 100 + 27 
        elif 28 <= mm and mm <= 30: # xx:28 ~ xx:30
            hhmm = hh * 100 + 30 
        elif 31 <= mm and mm <= 33: # xx:31 ~ xx:33
            hhmm = hh * 100 + 33
        elif 34 <= mm and mm <= 36: # xx:34 ~ xx:36
            hhmm = hh * 100 + 36 
        elif 37 <= mm and mm <= 39: # xx:37 ~ xx:39
            hhmm = hh * 100 + 39
        elif 40 <= mm and mm <= 42: # xx:40 ~ xx:42
            hhmm = hh * 100 + 42  
        elif 43 <= mm and mm <= 45: # xx:43 ~ xx:45
            hhmm = hh * 100 + 45  
        elif 46 <= mm and mm <= 48: # xx:46 ~ xx:48
            hhmm = hh * 100 + 48   
        elif 49 <= mm and mm <= 51: # xx:49 ~ xx:51
            hhmm = hh * 100 + 51   
        elif 52 <= mm and mm <= 54: # xx:52 ~ xx:54
            hhmm = hh * 100 + 54      
        elif 55 <= mm and mm <= 57: # xx:55 ~ xx:57
            hhmm = hh * 100 + 57                                                                                                                                                                                                                     
        elif (58 <= mm and mm <= 59): # xx:58 ~ xx:59
            hhmm = (hh + 1) * 100
        elif mm == 0:
            hhmm = hh * 100

        return hhmm    

    def makeMinchart_3Min(self, code, time, cur, vol):
        hhmm = self.MakeHHMM_3Min(time)
        bFind = False
        minlen =len(self.threeMinDatas[code])
        if (minlen > 0) :
            # 0 : 시간 1 : 시가 2: 고가 3: 저가 4: 종가 5: 거래량
            # 배열의 [-1]은 배열의 마지막 요소를 의미한다.
            if (self.threeMinDatas[code][-1][0] == hhmm) : 
                item = self.threeMinDatas[code][-1]
                bFind = True
                item[5] = vol
                item[4] = cur
                if (item[2] < cur): # 고가 업데이트
                    item[2] = cur
                if (item[3] > cur): # 저가 업데이트
                    item[3] = cur
 
        if bFind ==  False :
            self.threeMinDatas[code].append([hhmm, cur, cur, cur, cur, vol])
            self.caller.OnCreate3MinCandle(code, hhmm, self.threeMinDatas[code])
     
        return         

    def makeMinchart_1Min(self, code, time, cur, vol):
        hhmm = self.MakeHHMM(time)
        bFind = False
        minlen =len(self.minDatas[code])
        if (minlen > 0) :
            # 0 : 시간 1 : 시가 2: 고가 3: 저가 4: 종가 5: 거래량
            # 배열의 [-1]은 배열의 마지막 요소를 의미한다.
            if (self.minDatas[code][-1][0] == hhmm) : 
                item = self.minDatas[code][-1]
                bFind = True
                item[5] = vol
                item[4] = cur
                if (item[2] < cur): # 고가 업데이트
                    item[2] = cur
                if (item[3] > cur): # 저가 업데이트
                    item[3] = cur
 
        if bFind ==  False :
            self.minDatas[code].append([hhmm, cur, cur, cur, cur, vol])
     
        return

    def makeMinchart(self, code, time, cur, vol) :
        self.makeMinchart_1Min(code, time, cur, vol)
        self.makeMinchart_3Min(code, time, cur, vol)
        return

    def print(self, code):
        print('====================================================-')
        print('분데이터 print', code, g_objCodeMgr.CodeToName(code))
        print('시간,시가,고가,저가,종가')
        for item in self.minDatas[code] :
            hh, mm = divmod(item[0], 100)
            print("%02d:%02d,%d,%d,%d,%d" %(hh, mm, item[1], item[2], item[3], item[4]))
 

class CpMarketEye:
    def Request(self, codes, rqField):
        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        # 관심종목 객체 구하기
        objRq = win32com.client.Dispatch("CpSysDib.MarketEye")
        # 요청 필드 세팅 - 종목코드, 종목명, 시간, 대비부호, 대비, 현재가, 거래량
        # rqField = [0,17, 1,2,3,4,10]
        objRq.SetInputValue(0, rqField) # 요청 필드
        objRq.SetInputValue(1, codes)  # 종목코드 or 종목코드 리스트
        objRq.BlockRequest()
 
 
        # 현재가 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        rqRet = objRq.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
 
        cnt  = objRq.GetHeaderValue(2)
 
        for i in range(cnt):
            rpCode = objRq.GetDataValue(0, i)  # 코드
            rpName = objRq.GetDataValue(1, i)  # 종목명
            rpTime= objRq.GetDataValue(2, i)  # 시간
            rpDiffFlag = objRq.GetDataValue(3, i)  # 대비부호
            rpDiff = objRq.GetDataValue(4, i)  # 대비
            rpCur = objRq.GetDataValue(5, i)  # 현재가
            rpVol = objRq.GetDataValue(6, i)  # 거래량
            # print(rpCode, rpName, rpTime,  rpDiffFlag, rpDiff, rpCur, rpVol)
 
        return True


class StockMonitor:
    minData = CMinchartData()
    caller = 0

    def SaveLogs(self, filePath, code, logs):
        with open(filePath, "w", encoding='UTF-8-sig') as file:
            file.write(json.dumps(logs, ensure_ascii=False))        

        return  

    def Subscribe(self, codes, caller):
        for code in codes:
            print(code, g_objCodeMgr.CodeToName(code))
            self.minData.addCode(code, self)

        self.caller = caller
        return

    def UnSubscribe(self):
        self.minData.stop()
        return

    def Print(self, code):
        self.minData.print(code)
        return

    def OnUpdateStockCur(self, chart, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag):
        self.caller.OnUpdateStockCur(chart, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag)
        return

    def OnUpdateStockBid(self, chart, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):
        self.caller.OnUpdateStockBid(chart, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid)
        return

    def OnCreate3MinCandle(self, code, hhmm, threeMinCandles):
        self.caller.OnCreate3MinCandle(code, hhmm, threeMinCandles)
        return

    def GetYesterday(self):
        todayWeek = date.today().weekday()
        yesterday = 0
        if todayWeek == 0: # 월요일
            yesterday = date.today() - timedelta(3)
        elif todayWeek ==  6: # 일요일
            yesterday = date.today() - timedelta(2)
        else:
            yesterday = date.today() - timedelta(1)       
            
        yesterdayDate = yesterday.year * 10000 + yesterday.month * 100 + yesterday.day
        return yesterdayDate

    def GetToday(self):
        today = date.today()
        todayDate = today.year * 10000 + today.month * 100 + today.day
        return todayDate        

    def SetToday1MinChart(self, codes):
        today = self.GetToday()

        objChart = CpStockChart()
        for code in codes:
            todayStockInfos = []
            tempDayStockInfos = objChart.RequestMT(code, ord('m'), 1, 500)
            if tempDayStockInfos == False:
                continue

            for i in range(len(tempDayStockInfos))[::-1]:
                info = tempDayStockInfos[i]
                if info.date == today:
                    todayStockInfos.append(info)
                    print("러ㅣ너히너 날짜", info.date, "시간:", info.time, "시가:", info.open, "고가:", info.high, "저가:", info.low, "종가:", info.close, "전일대비", info.diff, "거래량", info.vol)

            # 데이터 기록
            logs = []
            for info in todayStockInfos:
                logs.append("날짜 " + str(info.date) + " 시간 " + str(info.time) + " 시가 " + str(info.open) + " 고가" + str(info.high) + " 저가 " + str(info.low) + " 종가 " + str(info.close) + " 전일대비 " + str(info.diff) + " 거래량 " + str(info.vol))

            filePath = "./오늘 과거기록/"+ "[" + str(code) + "]" + " 오늘 과거기록" + ".json"
            self.SaveLogs(filePath, code, logs)


            # 오늘 1분차트 설정
            print(code, "오늘 데이터 갯수:", len(todayStockInfos))
            todayLogs = self.minData.UpdateToday1MinChart(code, todayStockInfos)
            filePath = "./오늘 차트/"+ "[" + str(code) + "]" + " 오늘 차트" + ".json"
            self.SaveLogs(filePath, code, todayLogs)

            time.sleep(0.5)        
        return

    def SetYesterday1MinChart(self, codes):
        # 종목 기록
        codelogs = []
        for code in codes:
            codelogs.append(str(code))

        filePath = "./오늘 종목/" + "오늘 종목" + ".json"
        self.SaveLogs(filePath, code, codelogs)        

        # 어제 1분봉 데이터 얻기
        yesterday = self.GetYesterday()
        
        objChart = CpStockChart()
        for code in codes:
            lastDayStockInfos = []
            tempDayStockInfos = objChart.RequestMT(code, ord('m'), 1, 1000)
            if tempDayStockInfos == False:
                continue

            for i in range(len(tempDayStockInfos)):
                info = tempDayStockInfos[i]
                if info.date == yesterday:
                    lastDayStockInfos.append(info)
                    # print("지난 날짜", info.date, "시간:", info.time, "시가:", info.open, "고가:", info.high, "저가:", info.low, "종가:", info.close, "전일대비", info.diff, "거래량", info.vol)

            # 어제 1분차트 설정
            lastDayStockInfos = lastDayStockInfos[0:40]
            print(code, "어제 데이터 갯수:", len(lastDayStockInfos))

           # 데이터 기록
            logs = []
            for info in lastDayStockInfos[::-1]:
                logs.append("날짜 " + str(info.date) + " 시간 " + str(info.time) + " 시가 " + str(info.open) + " 고가" + str(info.high) + " 저가 " + str(info.low) + " 종가 " + str(info.close) + " 전일대비 " + str(info.diff) + " 거래량 " + str(info.vol))

            filePath = "./과거기록/"+ "[" + str(code) + "]" + " 과거기록" + ".json"
            self.SaveLogs(filePath, code, logs)

            self.minData.UpdateYesterday1MinChart(code, lastDayStockInfos)

            time.sleep(1)        
        return

    def CodeToName(self, code):
        return g_objCodeMgr.CodeToName(code)

    def Test(self, objChart, date, codes, caller):
        self.Subscribe(codes, caller)

        for code in codes:
            # 날짜에 해당하는 1분봉 데이터 얻기
            dayStockInfos = []
            lastDayStockInfos = []
            tempDayStockInfos = objChart.RequestMT(code, ord('m'), 1, 1000)
            if tempDayStockInfos == False:
                continue

            # print(g_objCodeMgr.CodeToName(code), "데이터 구함")
            
            for i in range(len(tempDayStockInfos)):
                info = tempDayStockInfos[i]
                if info.date == date:
                    dayStockInfos.append(info)
                    # print("날짜", info.date, "시간:", info.time , "시가", info.open, "고가", info.high, "저가", info.low, "종가", info.close, "전일대비", info.diff, "거래량", info.vol)
                else:
                    lastDayStockInfos.append(info)
                    # print("지난 날짜", info.date, "시간:", info.time, "시가:", info.open, "고가:", info.high, "저가:", info.low, "종가:", info.close, "전일대비", info.diff, "거래량", info.vol)
                    continue

            # 어제날짜 1분차트 설정
            lastDayStockInfos = lastDayStockInfos[0:40]

            # 데이터 기록
            logs = []
            for info in lastDayStockInfos[::-1]:
                logs.append("날짜 " + str(info.date) + " 시간 " + str(info.time) + " 시가 " + str(info.open) + " 고가" + str(info.high) + " 저가 " + str(info.low) + " 종가 " + str(info.close) + " 전일대비 " + str(info.diff) + " 거래량 " + str(info.vol))

            for info in dayStockInfos[::-1]:
                logs.append("날짜 " + str(info.date) + " 시간 " + str(info.time) + " 시가 " + str(info.open) + " 고가" + str(info.high) + " 저가 " + str(info.low) + " 종가 " + str(info.close) + " 전일대비 " + str(info.diff) + " 거래량 " + str(info.vol))

            filePath = "./과거기록/"+ "[" + str(code) + "]" + " 과거기록" + ".json"
            self.SaveLogs(filePath, code, logs)

            # 
            self.minData.UpdateYesterday1MinChart(code, lastDayStockInfos)

            dayStockInfos.reverse()
            diff = 0
            vol = 0
            cVolType = '1'
            exFlag = ord('2')
            pricesAndTimes = []

            for s in dayStockInfos:
                hhmm = self.GetHHMMBeforeMin_1(s.time)

                pricesAndTimes.clear()
                pricesAndTimes.append((s.open, hhmm * 100 + 00))
                pricesAndTimes.append((s.low, hhmm * 100 + 10))            
                pricesAndTimes.append((s.high, hhmm * 100 + 20))             
                pricesAndTimes.append((s.close, hhmm * 100 + 50))             

                for pt in pricesAndTimes:
                    self.minData.OnUpdateStockCur(code, g_objCodeMgr.CodeToName(code), diff, vol, pt[0], cVolType, s.vol, pt[1], exFlag)

            time.sleep(1)

        print("-------------끝--------------")

        # for code in codes:
        #     # 날짜에 해당하는 1분봉 데이터 얻기
        #     dayStockInfos = []
        #     tempDayStockInfos = objChart.RequestMT(code, ord('m'), 1, 1000)
            
        #     for i in range(len(tempDayStockInfos)):
        #         info = tempDayStockInfos[i]
        #         if info.date == date:
        #             dayStockInfos.append(info)
        #             print("날짜", info.date, "시간:", info.time , "시가", info.open, "고가", info.high, "저가", info.low, "종가", info.close, "전일대비", info.diff, "거래량", info.vol)
        #         else:
        #             continue

        #         dayStockInfos.reverse()
        #         diff = 0
        #         vol = 0
        #         cVolType = '1'
        #         exFlag = ord('2')
        #         pricesAndTimes = []

        #         for s in dayStockInfos:
        #             hhmm = self.GetHHMMBeforeMin_1(s.time)

        #             pricesAndTimes.clear()
        #             pricesAndTimes.append((s.open, hhmm * 100 + 00))
        #             pricesAndTimes.append((s.low, hhmm * 100 + 10))            
        #             pricesAndTimes.append((s.high, hhmm * 100 + 20))             
        #             pricesAndTimes.append((s.close, hhmm * 100 + 41))             

        #             for pt in pricesAndTimes:
        #                 self.minData.OnUpdateStockCur(code, g_objCodeMgr.CodeToName(code), diff, vol, pt[0], cVolType, s.vol, pt[1], exFlag)

            

        return

    def GetHHMMBeforeMin_1(self, hhmm):
        hh, mm = divmod(hhmm, 100)
        if mm == 0:
            hh = hh - 1
            mm = 59
            hhmm = hh * 100 + mm
        else:
            hhmm = hhmm - 1
            
        return hhmm        












