import sys
from PyQt5.QtWidgets import *
import win32com.client
from TradeLib import SendMsgToSlack

#####################################################################################
SLACK_TOKEN = "xoxb-1974644675939-1987050986353-NJjMu7Q32kjke0PpLKeGrzGd"
SLACK_CHANNEL = "#stock"

# 메시지를 슬랙으로 보내기
def SendMsg(msg):
    SendMsgToSlack(SLACK_TOKEN, SLACK_CHANNEL, msg)
    pass

#####################################################################################
 
# 복수 종목 실시간 조회 샘플 (조회는 없고 실시간만 있음)
class SellBuyAIElement:
    startTime = 0
    didBuy = False
    buyVolCount = 0
    sellVolCount = 0
    buyPrice = 0

    pass

class SellBuyAI:
    elements = {}
    BUY_TIMING_CHECK_TIME = 60
    SELL_TIMING_CHECK_TIME = 40
    BUY_TIMING_VALUE = 4
    SELL_TIMING_VALUE = 0.8
    totalRate = 0

    def Process(self, code, name, vol, cprice, cVolType, cVol, timess):
        if False == (code in self.elements):
           self.elements[code] = SellBuyAIElement()
           self.elements[code].startTime = timess
        
        e = self.elements[code]

        if cVolType == ord('1'):
            e.buyVolCount = e.buyVolCount + 1
        else:
            e.sellVolCount = e.sellVolCount + 1    

        checkTime = 0
        if e.didBuy: # 종목을 샀으면
            checkTime = self.SELL_TIMING_CHECK_TIME
        else:
            checkTime = self.BUY_TIMING_CHECK_TIME

        if checkTime <= timess - e.startTime:
            buyVelocity = e.buyVolCount / checkTime
            sellVelocity = e.sellVolCount / checkTime

            print(name, "매수:", e.buyVolCount, "매도:", e.sellVolCount)

            checkValue = 0
            if e.sellVolCount < 1: # 매도갯수가 0
                checkValue = buyVelocity / 1
            else:
                checkValue = buyVelocity / sellVelocity

            if e.didBuy: # 종목을 샀으면
                # 매도 타이밍 체크
                if checkValue <= self.SELL_TIMING_VALUE:
                    # 현재가로 매도
                    rate = ((cprice - e.buyPrice) / e.buyPrice) * 100
                    self.totalRate = self.totalRate + rate
                    sellString = name + " 매도 - " + "가격: " + str(cprice) + "수익률: " + str(rate) + "%" + "총수익률:" + str(self.totalRate)
                    SendMsg(sellString)
                    e.didBuy = False
            else: 
                # 매수 타이밍 체크
                print(name, "매수 타이밍 체크", checkValue)
                if self.BUY_TIMING_VALUE <= checkValue:
                    # 현재가로 매수
                    sellString = name + " 매수 - " + " 가격: " + str(cprice)
                    SendMsg(sellString)
                    e.buyPrice = cprice
                    e.didBuy = True

            e.startTime = timess
            e.buyVolCount = 0
            e.sellVolCount = 0

        pass

    pass

class CpEvent:
    sellBuyAI = SellBuyAI()

    def set_params(self, client):
        self.client = client
 
    def OnReceived(self):
        code = self.client.GetHeaderValue(0)  # 종목코드
        name = self.client.GetHeaderValue(1)  # 종목명
        diff = self.client.GetHeaderValue(2)  # 전일대비
        vol = self.client.GetHeaderValue(9)  # 거래량 
        cprice = self.client.GetHeaderValue(13)  # 현재가
        cVolType = self.client.GetHeaderValue(14) # 체결상태 1: 매수 2: 매도
        cVol = self.client.GetHeaderValue(17)  # 순간체결수량
        timess = self.client.GetHeaderValue(18)  # 체결 시간(초)
        exFlag = self.client.GetHeaderValue(19)  # 예상체결 플래그

        cVolTypeTitle = ""
        if cVolType ==  ord('1'):
            cVolTypeTitle = "매수"
        else:
            cVolTypeTitle = "매도"

        if (exFlag == ord('1')):  # 동시호가 시간 (예상체결)
            print("실시간(예상체결)", name, timess, "*", cprice, "대비", diff, cVolTypeTitle, "체결량", cVol, "거래량", vol)
        elif (exFlag == ord('2')):  # 장중(체결)
            print("실시간(장중 체결)", name, timess, cprice, "대비", diff, cVolTypeTitle, "체결량", cVol, "거래량", vol)

        self.sellBuyAI.Process(code, name, vol, cprice, cVolType, cVol, timess)
 
 
class CpStockCur:
    def Subscribe(self, code):
        self.objStockCur = win32com.client.Dispatch("DsCbo1.StockCur")
        handler = win32com.client.WithEvents(self.objStockCur, CpEvent)
        self.objStockCur.SetInputValue(0, code)
        handler.set_params(self.objStockCur)
        self.objStockCur.Subscribe()
 
    def Unsubscribe(self):
        self.objStockCur.Unsubscribe()
 
 
class CpMarketEye:
    def Request(self, codes, rqField):
        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
            return False
 
        # 관심종목 객체 구하기
        objRq = win32com.client.Dispatch("CpSysDib.MarketEye")
        # 요청 필드 세팅 - 종목코드, 종목명, 시간, 대비부호, 대비, 현재가, 거래량
        # rqField = [0,17, 1,2,3,4,10]
        objRq.SetInputValue(0, rqField) # 요청 필드
        objRq.SetInputValue(1, codes)  # 종목코드 or 종목코드 리스트
        objRq.BlockRequest()
 
 
        # 현재가 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        rqRet = objRq.GetDibMsg1()
        print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
 
        cnt  = objRq.GetHeaderValue(2)
 
        for i in range(cnt):
            rpCode = objRq.GetDataValue(0, i)  # 코드
            rpName = objRq.GetDataValue(1, i)  # 종목명
            rpTime= objRq.GetDataValue(2, i)  # 시간
            rpDiffFlag = objRq.GetDataValue(3, i)  # 대비부호
            rpDiff = objRq.GetDataValue(4, i)  # 대비
            rpCur = objRq.GetDataValue(5, i)  # 현재가
            rpVol = objRq.GetDataValue(6, i)  # 거래량
            print(rpCode, rpName, rpTime,  rpDiffFlag, rpDiff, rpCur, rpVol)
 
        return True

class StockMonitor:
    objCur = []
    isSB = False

    def Subscribe(self, codes):
        self.UnSubscribe()
 
        # CpSysDib.MarketEye 도움말 참고
        # 요청 필드 배열 - 종목코드, 시간, 대비부호,  전일대비, 현재가, 거래량, 종목명
        rqField = [0, 1, 2, 3, 4, 10, 17]  #요청 필드
        objMarkeyeye = CpMarketEye()
        if (objMarkeyeye.Request(codes, rqField) == False):
            exit()
 
        cnt = len(codes)
        for i in range(cnt):
            self.objCur.append(CpStockCur())
            self.objCur[i].Subscribe(codes[i])
 
        print("================")
        print(cnt , "종목 실시간 현재가 요청 시작")
        self.isSB = True
        pass

    def UnSubscribe(self):
        if self.isSB:
            cnt = len(self.objCur)
            for i in range(cnt):
                self.objCur[i].Unsubscribe()
            print(cnt, "종목 실시간 해지되었음")
        self.isSB = False
        self.objCur = [] 
        pass

    pass        