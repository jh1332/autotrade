import os
import shutil


path_dir = r"C:\Repository\AutoTrade\매매 데이터"
# path_dir = r"C:\Users\HOON\Desktop\Test"

file_list = os.listdir(path_dir)
for file in file_list:
    filePathSource = path_dir + '\\' + file
    if os.path.isdir(filePathSource):
        continue
    
    splitedFileNames = file.split('_')
    if(len(splitedFileNames) < 2):
        continue

    folderPath = path_dir + "\\" + splitedFileNames[0]
    if False == os.path.isdir(folderPath): # 폴더가 존재하지 않으면 폴더생성
        os.makedirs(folderPath)

    # 파일을 폴더에 옮기기
    filePathDest = folderPath
    shutil.move(filePathSource, filePathDest)

        


