 
def Solution(board):
    n = 15
    dy = [1, 1, 0, -1]
    dx = [0, 1, 1, 1] 

    for y in range(n):
        for x in range(n):
            if board[y][x]:
                for i in range(4):
                    nx = x + dx[i]
                    ny = y + dy[i]
                    count = 1
 
                    if ny < 0 or nx < 0 or ny >= n or nx >= n: # 범위를 벗어났으면
                        continue
 
                    while 0 <= ny < n and 0 <= nx < n and board[y][x] == board[ny][nx]:
                        count += 1
 
                        if count == 5:
                            if 0 <= ny + dy[i] < n and 0 <= nx + dx[i] < n and board[ny][nx] == board[ny + dy[i]][nx + dx[i]]:    # 육목 판정 1
                                break
                            if 0 <= y - dy[i] < n and 0 <= x - dx[i] < n and board[y][x] == board[y - dy[i]][x - dx[i]]:    # 육목 판정 2
                                break
                            return board[y][x]    # 육목이 아닌 오목이면
 
                        ny += dy[i]
                        nx += dx[i]
    return 0
 

board = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 2, 1, 1, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 2, 1, 1, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 1, 2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
         [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]]

color = Solution(board)