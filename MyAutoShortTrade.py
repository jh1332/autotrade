import time
import win32com.client
import datetime
from datetime import date, timedelta
from PyQt5.QtWidgets import *
import sys

####################################################################
# PyQt5와 pywinauto를 같이 import할 때 문제 해결을 위해 추가한 코드
import warnings
warnings.simplefilter("ignore", UserWarning)
sys.coinit_flags = 2
####################################################################
from pywinauto import application

from TradeLib import SendMsgToSlack
from TradeLib import GetGreatStockCodeList
from TradeLib import GetGoodStockCodeList 
from TradeLib import IsGreatBuyTiming, IsGoodBuyTiming, IsSellTiming
from TradeLib import SellStock, BuyStock
from TradeLib import CpStockCur, CpStockMst
from TradeLib import SubscribeConclusionInfo, UnSubscribeConclusionInfo
from TradeLib import StockInfosPerDay, DayStockInfo, GetLowPoints, GetHighPoints, CpStockChart, GetBaseLines, StockInfo
# from StockMonitoring import StockMonitor
from StockMonitoring2 import StockMonitor
import json
import math
from collections import OrderedDict
import pickle
import asyncio
import os




#####################################################################################
SLACK_TOKEN = "xoxb-1974644675939-1987050986353-NJjMu7Q32kjke0PpLKeGrzGd"
SLACK_CHANNEL = "#stock"
#####################################################################################

#####################################################################################
cpCodeMgr = win32com.client.Dispatch('CpUtil.CpStockCode')
cpStatus = win32com.client.Dispatch('CpUtil.CpCybos')
cpTradeUtil = win32com.client.Dispatch('CpTrade.CpTdUtil')
cpStock = win32com.client.Dispatch('DsCbo1.StockMst')
cpOhlc = win32com.client.Dispatch('CpSysDib.StockChart')
cpBalance = win32com.client.Dispatch('CpTrade.CpTd6033')
cpCash = win32com.client.Dispatch('CpTrade.CpTdNew5331A')
cpOrder = win32com.client.Dispatch('CpTrade.CpTd0311')
#####################################################################################  

#####################################################################################
# 메시지를 슬랙으로 보내기
def SendMsg(msg):
    SendMsgToSlack(SLACK_TOKEN, SLACK_CHANNEL, msg)
#####################################################################################

#####################################################################################
# 이미 구입한 종목인지 체크
def DidBuy(stockCode):
    return False

#####################################################################################

SEED_MONEY = 100000000
SEED_PART_COUNT = 10

#####################################################################################
# 날짜별 정보
class DayStockInfo:
    date = ""       # 날짜
    open = 0        # 시가
    high = 0        # 고가
    low = 0         # 저가
    pass

objStockWeek = win32com.client.Dispatch("DsCbo1.StockWeek")
# def RequestDayStock(code, info):
#     # objStockWeek.SetInputValue(0, code) 

#     # 데이터 요청
#     objStockWeek.BlockRequest()
 
#     # 통신 결과 확인
#     rqStatus = objStockWeek.GetDibStatus()
#     rqRet = objStockWeek.GetDibMsg1()
#     print("통신상태", rqStatus, rqRet)
#     if rqStatus != 0:
#         return False
 
#     # 일자별 정보 데이터 처리
#     count = objStockWeek.GetHeaderValue(1)  # 데이터 개수
#     print("일자 데이터 갯수: " + str(count))
#     for i in range(count):
#         date = objStockWeek.GetDataValue(0, i)  # 일자
#         open = objStockWeek.GetDataValue(1, i)  # 시가
#         high = objStockWeek.GetDataValue(2, i)  # 고가
#         low = objStockWeek.GetDataValue(3, i)  # 저가
#         close = objStockWeek.GetDataValue(4, i)  # 종가
#         diff = objStockWeek.GetDataValue(5, i)  # 종가
#         vol = objStockWeek.GetDataValue(6, i)  # 종가
#         print(date, open, high, low, close, diff, vol)
 
#     return True    
#####################################################################################

# 종목별 필요한 정보
# 전 고점, 전 저점
# 과거와 현재의 5일, 20일 이동평균선 정보
# 실시간 거래량
# 실시간 호가창 정보

# class MyWindow(QMainWindow):
 
#     def __init__(self):
#         super().__init__()
#         self.setWindowTitle("PLUS API TEST")
#         self.setGeometry(300, 300, 300, 150)
#         self.isRq = False
#         self.objStockMst = CpStockMst()
#         self.objStockCur = CpStockCur()
 
#         btn1 = QPushButton("요청 시작", self)
#         btn1.move(20, 20)
#         btn1.clicked.connect(self.btn1_clicked)
 
#         btn2 = QPushButton("요청 종료", self)
#         btn2.move(20, 70)
#         btn2.clicked.connect(self.btn2_clicked)
 
#         btn3 = QPushButton("종료", self)
#         btn3.move(20, 120)
#         btn3.clicked.connect(self.btn3_clicked)
 
#     def StopSubscribe(self):
#         UnSubscribeConclusionInfo(self.objStockCur)
#         # if self.isRq:
#         #     self.objStockCur.Unsubscribe()
#         # self.isRq = False
 
#     def btn1_clicked(self):
#         SubscribeConclusionInfo(self.objStockCur, 'A000660')
#         SubscribeConclusionInfo(self.objStockCur, 'A005930')
#         # testCode = "A000660"
#         # if (self.objStockMst.Request(testCode) == False):
#         #     exit()
 
#         # # 하이닉스 실시간 현재가 요청
#         # self.objStockCur.Subscribe(testCode)
 
#         # print("빼기빼기================-")
#         # print("실시간 현재가 요청 시작")
#         # self.isRq = True
 
#     def btn2_clicked(self):
#         self.StopSubscribe()
 
 
#     def btn3_clicked(self):
#         self.StopSubscribe()
#         exit()
 

# Cp7043 상승률 상위 요청 클래스
class Cp7043:
    def __init__(self):
        # 통신 OBJECT 기본 세팅
        self.objRq = win32com.client.Dispatch("CpSysDib.CpSvrNew7043")
        self.objRq.SetInputValue(0, ord('0')) # 거래소 + 코스닥
        self.objRq.SetInputValue(1, ord('2'))  # 상승
        self.objRq.SetInputValue(2, ord('1'))  # 당일
        # self.objRq.SetInputValue(3, 21)  # 전일 대비 상위 순
        self.objRq.SetInputValue(3, 61)  # 거래대금상위순
        self.objRq.SetInputValue(4, ord('1'))  # 관리 종목 제외
        self.objRq.SetInputValue(5, ord('0'))  # 거래량 전체
        self.objRq.SetInputValue(6, ord('0'))  # '표시 항목 선택 - '0': 시가대비
        self.objRq.SetInputValue(7, 0)  #  등락율 시작
        self.objRq.SetInputValue(8, 30)  # 등락율 끝
        return
 
    # 실제적인 7043 통신 처리
    def rq7043(self, retcode):
        self.objRq.BlockRequest()
        # 현재가 통신 및 통신 에러 처리
        rqStatus = self.objRq.GetDibStatus()
        rqRet = self.objRq.GetDibMsg1()
        # print("통신상태", rqStatus, rqRet)
        if rqStatus != 0:
            return False
 
        cnt = self.objRq.GetHeaderValue(0)
        cntTotal  = self.objRq.GetHeaderValue(1)
        print(cnt, cntTotal)
 
        for i in range(cnt):
            code = self.objRq.GetDataValue(0, i)  # 코드
            retcode.append(code)
            if len(retcode) >=  100:       # 최대 200 종목만,
                break
            name = self.objRq.GetDataValue(1, i)  # 종목명
            diffflag = self.objRq.GetDataValue(3, i)
            diff = self.objRq.GetDataValue(4, i)
            vol = self.objRq.GetDataValue(6, i)  # 거래량
            print(code, name, diffflag, diff, vol)
 
    def Request(self, retCode):
        self.rq7043(retCode)
 
        # 연속 데이터 조회 - 400 개까지만.
        while self.objRq.Continue:
            self.rq7043(retCode)
            print(len(retCode))
            if len(retCode) >= 100:
                break
 
        # #7043 상승하락 서비스를 통해 받은 상승률 상위 200 종목
        # size = len(retCode)
        # for i in range(size):
        #     print(retCode[i])





        return True


class TradeDataMgr:
    tradeDatas = {}
    startTime = {}
    fileCount = 0
    SAVE_TIME = 2 * 60
    
    def AddCodes(self, codes, names):
        for i in range(len(codes)):
            code = codes[i]
            if self.tradeDatas.get(code) == None:
                self.tradeDatas[code] = TradeData(code, names[i]) 

        self.startTime = datetime.datetime.now()
        return

    def GetTradeData(self, code):
        return self.tradeDatas[code]      

    def OnUpdateStockCur(self, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag):
        self.tradeDatas[code].OnUpdateStockCur(code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag)

        return
    
    def OnUpdateStockBid(self, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):
        self.tradeDatas[code].OnUpdateStockBid(code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid)

        now = datetime.datetime.now()
        diffTime = now - self.startTime
        if self.SAVE_TIME <= diffTime.seconds:
            self.Save()
            self.startTime = now

        return

    def LoadFile(self, fileName):
        filePath = "./매매 데이터/"+ fileName  

        data = {}
        with open(filePath, 'rb') as file:
            data = pickle.load(file)   

        return data

    def Save(self):
        print("----------파일로 저장-----------")

        now = datetime.datetime.now()
        # fileName = str(now.year) + str(now.month).zfill(2) + str(now.day).zfill(2) + ".json"
        self.fileCount = self.fileCount + 1
        fileName = str(now.year) + str(now.month).zfill(2) + str(now.day).zfill(2) + "_" + str(self.fileCount) +  ".p"
        filePath = "./매매 데이터/"+ fileName

        datas = []
        for data in self.tradeDatas.values():
            datas.append(data.ConvertToDic())

        # with open(filePath, "w", encoding='UTF-8-sig') as file:
        #     file.write(json.dumps(datas, ensure_ascii=False))  

        with open(filePath, 'wb') as file:
            pickle.dump(datas, file)      

        # 기존 데이터 삭제
        for data in self.tradeDatas.values():
            data.Clear()

        return
    

class TradeData:
    code = 0
    name = ''
    stockCurDatas = []
    stockBidDatas = []   

    def __init__(self, code, name):
        self.code = code
        self.name = name
        self.stockCurDatas = []
        self.stockBidDatas = []

    def Clear(self):
        self.stockCurDatas.clear()
        self.stockBidDatas.clear()
        return

    def OnUpdateStockCur(self, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag):
        self.stockCurDatas.append([diff, vol, cprice, cVolType, cVol, timess, exFlag])
        return

    def OnUpdateStockBid(self, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):               
        self.stockBidDatas.append(TradeStockBidData(time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid))
        return

    def ConvertToDic(self):
        stockBids = []
        for bidData in self.stockBidDatas:
            stockBids.append(bidData.ConvertToDic())

        return {'code':self.code, 'name':self.name, 'stockCur':self.stockCurDatas, 'stockBid':stockBids}

class TradeStockBidData:
    time = 0
    vol = 0
    offer = []
    offervol = []
    bid = []
    bidvol = []
    totalOffer = 0
    totalBid = 0

    def __init__(self, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):
        self.time = time
        self.vol = vol
        self.offer = offer
        self.offervol = offervol
        self.bid = bid
        self.bidvol = bidvol
        self.totalOffer = totalOffer
        self.totalBid = totalBid

    def ConvertToDic(self):
        return {'time':self.time, 'vol':self.vol, 'offer':self.offer, 'offervol':self.offervol, 'bid':self.bid, 'bidvol':self.bidvol, 'totalOffer':self.totalOffer, 'totalBid':self.totalBid}


class MyWindow(QMainWindow):
    stockMonitor = StockMonitor()
    stockInfos = {}
    codes = []
    sellBuyAI = {}
    tradeDataMgr = {}

    def __init__(self):
        super().__init__()
        self.setWindowTitle("PLUS API TEST")
        self.setGeometry(300, 300, 300, 150)
        self.sellBuyAI = SellBuyAI()
        self.tradeDataMgr = TradeDataMgr()
 
        btnStart = QPushButton("요청 시작", self)
        btnStart.move(20, 20)
        btnStart.clicked.connect(self.btnStart_clicked)
 
        # 테스트
        # print("상위호가단위:", self.sellBuyAI.GetBidUnitUp(1000, True))



        # btnStop = QPushButton("요청 종료", self)
        # btnStop.move(20, 70)
        # btnStop.clicked.connect(self.btnStop_clicked)

        # btnStop = QPushButton("1분 차트", self)
        # btnStop.move(20, 70)
        # btnStop.clicked.connect(self.btnChart_clicked)

        # btnTest = QPushButton("테스트", self)
        # btnTest.move(20, 70)
        # btnTest.clicked.connect(self.btnTest_clicked)

        btnTest = QPushButton("데이터 수집", self)
        btnTest.move(20, 70)
        btnTest.clicked.connect(self.btnClicked_CollectDatas)     
 
        btnExit = QPushButton("종료", self)
        btnExit.move(20, 120)
        btnExit.clicked.connect(self.btnExit_clicked)
 
    def StopSubscribe(self):
        self.stockMonitor.UnSubscribe()
        self.sellBuyAI.SaveLogs()
 
    def InsertCodes(self, codes):
        addCodes = []
        addCodes.append("A005860") # 한일사료
        addCodes.append("A225570") # 넷게임즈
        addCodes.append("A090710") # 휴림로봇

        for code in addCodes:
            if code in codes:
                continue

            codes.append(code)
        return

    def btnClicked_CollectDatas(self):
        print("프로그램 시작")

        # while(True):
        #     now = datetime.datetime.now()
        #     if 10 <= now.hour:
        #         print(now.hour, "시", now.minute, "분")
        #         break

        #     elif 9 <= now.hour and 3 <= now.minute:
        #         print(now.hour, "시", now.minute, "분")
        #         break
        #     else:
        #         time.sleep(0.5)

        print("수집 시작")

        # 거래대금 상위순으로 종목 구하기
        self.codes = []
        obj7043 = Cp7043()
        if obj7043.Request(self.codes) == False:
            print("거래대금 상위종목 검색 실패")
            return

        self.InsertCodes(self.codes)

        # 종목 등록
        names = []
        for code in self.codes:
            names.append(self.stockMonitor.CodeToName(code))

        self.tradeDataMgr.AddCodes(self.codes, names)

        # 매매정보 구독
        self.stockMonitor.Subscribe(self.codes, self)


        ##############################################################################################
        # self.codes = ['A100']
        # names = ["정훈전자"]
        # self.tradeDataMgr.AddCodes(self.codes, names)

        # self.tradeDataMgr.OnUpdateStockCur('A100', "정훈전자", 11, 1000, 2012, '1', 123, 101023, '2')
        # time = 101023
        # vol = 1230
        # offer = [1, 2, 3, 4, 5]
        # offervol = [100, 200, 300, 400, 500]
        # bid = [5, 4, 3, 2 , 1]
        # bidvol = [100, 200, 300, 400, 500]
        # totalOffer = 2100
        # totalBid = 1200
        # self.tradeDataMgr.OnUpdateStockBid('A100', time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid)
        # self.tradeDataMgr.Save()

        # self.tradeDataMgr.OnUpdateStockCur('A100', "정훈전자", 11, 1000, 2012, '1', 123, 101024, '2')
        # self.tradeDataMgr.Save()

        # loadedDatas = self.tradeDataMgr.LoadFile('20210604_1.p')
        # for data in loadedDatas:
        #     print(data)

        # loadedDatas = self.tradeDataMgr.LoadFile('20210604_2.p')
        # for data in loadedDatas:
        #     print(data)

        ##############################################################################################

        return

    def btnTest_clicked(self):
        objChart = CpStockChart()


        codes= []
        # codes.append("A001780") # 우상향 판단값보다 약간 작아서 우상향이 아니라고 판단
        # codes.append("A103140") # 우상향 판단값보다 약간 작아서 우상향이 아니라고 판단        
        # codes.append("A018310") # 좋은 캔들 판단 기준에 1호가보다 큰 값이란 조건을 추가해서 좋은 캔들이 아니라고 판단        
        # codes.append("A185490") # 문제없음        


        # codes.append("A128660")        
        # codes.append("A019490")   

        # codes.append("A950130")

        # codes.append("A084010")
        # codes.append("A000970") 

        # codes.append("A014440") 


        # self.codes.clear()
        # self.codes.extend(codes)


        self.codes = []
        # obj7043 = Cp7043()
        # if obj7043.Request(self.codes) == False:
        #     return

        filePath = "./오늘 종목/"+ "오늘 종목.json"
        with open(filePath, "r", encoding='UTF-8-sig') as file:
            todayCodes = json.load(file, object_pairs_hook=OrderedDict)

        for tc in todayCodes:
            self.codes.append(tc)


        self.stockInfos = {}
        for code in self.codes:
            stockInfo = StockInfo()
            stockInfo.code = code
            self.stockInfos[code] = stockInfo

        DATE = 20210514
        self.stockMonitor.Test(objChart, DATE, self.codes, self)

        return

    def btnStart_clicked(self):
        # 종목 구하기
        self.codes = []
        obj7043 = Cp7043()
        if obj7043.Request(self.codes) == False:
            return

        self.stockInfos = {}
        for code in self.codes:
            stockInfo = StockInfo()
            stockInfo.code = code
            # stockInfo.baseLines = GetAllBaseLines(code, dates)
            self.stockInfos[code] = stockInfo

        # for k in stockInfos.keys():
        #     for line in stockInfos[k].baseLines:
        #         print(stockInfos[k].code, "기준선", line)
 
        # for code in self.codes:
        #     print("종목코드:", code)
            
        # print("상승종목 개수:", len(self.codes))


        self.stockMonitor.Subscribe(self.codes, self)
        self.stockMonitor.SetToday1MinChart(self.codes)
        self.stockMonitor.SetYesterday1MinChart(self.codes)

        return

    def btnStop_clicked(self):
        self.StopSubscribe()
 
    def btnExit_clicked(self):
        self.StopSubscribe()

        exit()

    def btnChart_clicked(self):
        for i in range(len(self.codes)):
            self.stockMonitor.Print(self.codes[i])
            if i > 10: break
        return

        exit()

    # 체결창
    def OnUpdateStockCur(self, chart, code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag):
        # 데이터 수집할 때는 매매 안 하기
        # stockInfo = self.stockInfos[code]
        # self.sellBuyAI.OnUpdateStockCur(stockInfo, chart, name, vol, cprice, cVolType, cVol, timess)

        # 체결창 정보 저장
        self.tradeDataMgr.OnUpdateStockCur(code, name, diff, vol, cprice, cVolType, cVol, timess, exFlag)

        return

    # 호가창
    def OnUpdateStockBid(self, chart, code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid):
        # 데이터 수집할 때는 매매 안 하기
        # stockInfo = self.stockInfos[code]
        # self.sellBuyAI.OnUpdateStockBid(stockInfo, chart, offer, offervol, bid, bidvol, totalOffer, totalBid)

        # 호가창 정보 저장
        self.tradeDataMgr.OnUpdateStockBid(code, time, vol, offer, offervol, bid, bidvol, totalOffer, totalBid)

        # 종료시간이 되면 종료

        now = datetime.datetime.now()
        if 15 <= now.hour and 32 <= now.minute:
            print("----------------------종료-------------------")
            self.btnExit_clicked()

        return

    # 3분봉 생성
    def OnCreate3MinCandle(self, code, hhmm, threeMinCandles):
        # if len(threeMinCandles) <= 5:
        #     return

        # # print(hhmm, "3분봉 생성 후 기준선 갱신")

        # dayStockInfos = []
        # for candle in threeMinCandles:
        #     info = DayStockInfo()
        #     info.time = candle[0] # 시간
        #     info.open = candle[1]  # 시가
        #     info.high = candle[2]  # 고가
        #     info.low = candle[3]  # 저가
        #     info.close = candle[4]  # 종가
        #     info.vol = candle[5]  # 거래량
        #     dayStockInfos.append(info)

        # stockInfo = stockInfos[code]
        # stockInfo.today3MinBaseLines.clear()
        # stockInfo.today3MinBaseLines.extend(GetBaseLines(False, dayStockInfos, 5))

        # for lines in stockInfo.today3MinBaseLines:
        #     print("3분봉 기준선 - ", "시간:", lines[0], "기준선:", lines[1])

        return                



def GetTargetCodes():
    codes = []
    codes.append('A214260')
    codes.append("A025550")
    codes.append("A001795")
    codes.append("A002360")

    codes.append("A021080")
    codes.append("A039240")
    codes.append("A246690")  

    codes.append("A027710")
    codes.append("A317320")
    codes.append("A186230")    
    codes.append("A344050")

    codes.append("A035890")
    codes.append("A101670")
    codes.append("A133750")    
    codes.append("A054940")

    codes.append("A006380")
    codes.append("A043220")
    codes.append("A092200")    
    codes.append("A058610")

    codes.append("A032640")    
    codes.append("A019550")

    return codes


def GetAllBaseLines(code, dates):
    # 일봉 차트를 기준으로 기준선 구하기
    stockInfosPerDay = StockInfosPerDay()
    stockInfosPerDay.SetCode(code)

    dayStockInfos = []
    stockInfosPerDay.RequestDayStocks(dayStockInfos, 3)
    baseLines = GetBaseLines(True, dayStockInfos, 3)

    # 3분봉 차트를 기준으로 기준선 구하기
    dayStockInfos.clear()
    objChart = CpStockChart()

    tempDayStockInfos = objChart.RequestMT(code, ord('m'), 3, 1000)
        
    for i in range(len(tempDayStockInfos)):
        info = tempDayStockInfos[i]
        if info.date in dates:
            dayStockInfos.append(info)
            # print("날짜", info.date, "시간:", info.time , "시가", info.open, "고가", info.high, "저가", info.low, "종가", info.close, "전일대비", info.diff, "거래량", info.vol)
        else:
            continue

    baseLines.extend(GetBaseLines(False, dayStockInfos, 5))

    return baseLines


#####################################################################################

class SellBuyAIElement:
    code = 0
    name = ""
    didBuy = False
    high = 0
    arrivedAtTarget = False

    didBuyExtra = False
    didSellPart = False # 일부를 매도했냐
    buyPrices = []
    buyStockCounts = []
    targetPrice = 0
    tempBaseLine = -1
    cutLine = -1
    extraBuyLine = -1
    isKosdaq = False

    totalOffer = 0 # 총 매도잔량
    totalBid = 0 # 총 매수잔량
    sellBuyPowerCheckStartTime = -1 # 매수매도 세기 체크 시작 시간
    buyVolCount = 0
    sellVolCount = 0

    def __init__(self):
        self.buyPrices = []
        self.buyStockCounts = []
        return

    def GetTotalBuyPrices(self):
        total = 0

        for i in range(0, len(self.buyPrices)):
            total = total + (self.buyPrices[i] * self.buyStockCounts[i])
 
        return total

    def GetTotalBuyStockCount(self):
        total = 0
        for i in range(0, len(self.buyStockCounts)):       
            total = total + self.buyStockCounts[i]

        return total
    pass

class SellBuyAI:
    elements = {}
    BUY_TIMING_CHECK_TIME = 60
    SELL_TIMING_CHECK_TIME = 40
    BUY_TIMING_VALUE = 4
    SELL_TIMING_VALUE = 0.8
    totalDealCount = 0
    totalRate = 0
    totalProfit = 0
    totalFail = 0
    totalSuccess = 0

    logs = []

    def __init__(self):
        self.logs = []
        return

    def Log(self, log):
        msg = "[" + self.GetCurTime() + "]" + " " + log
        self.logs.append(msg)
        return

    def SaveLogs(self):
        with open("TradeLogs.json", "w", encoding='UTF-8-sig') as file:
            file.write(json.dumps(self.logs, ensure_ascii=False))        

        return 

    def SaveLogsDir(self, filePath, logs):
        with open(filePath, "w", encoding='UTF-8-sig') as file:
            file.write(json.dumps(logs, ensure_ascii=False))        

        return                 

    def NoHasHighBaseLineThanCurPrice(self, stockInfo, curPrice):
        noHas = True

        baseLines = stockInfo.GetAllBaseLines()

        for line in baseLines:
            if curPrice < line[1]:
                noHas = False
                break

        return noHas

    def IsPlusCandle(self, open, close):
        return self.IsMinusCandle(open, close) == False

    def IsMinusCandle(self, open, close):
        return close <= open 

    def IsRealMinusCandle(self, open, close):
        return close < open   

    def UpdateTempBaseLine(self, sellBuyAIElement, chart, code, time):
        hhmmBeforeMin_1 = chart.GetHHMMBeforeMin_1ByTime(time)
        if chart.HasCandleByHHMM(code, hhmmBeforeMin_1) == False:
            return

        candleBeforeMin_1 = chart.GetCandle(code, hhmmBeforeMin_1)
        openBeforMin_1 = candleBeforeMin_1[1]
        closeBeforMin_1 = candleBeforeMin_1[4]
        if self.IsMinusCandle(openBeforMin_1, closeBeforMin_1) == False: # 1분전 캔들이 음봉이 아니면
            return

        hhmmBeforeMin_2 = chart.GetHHMMBeforeMin_1(hhmmBeforeMin_1)
        if chart.HasCandleByHHMM(code, hhmmBeforeMin_2) == False:
            return

        candleBeforeMin_2 = chart.GetCandle(code, hhmmBeforeMin_2)    

        volBeforeMin_1 = candleBeforeMin_1[5] # 1분전 누적 거래량
        volBeforeMin_2 = candleBeforeMin_2[5] # 2분전 누적 거래량

        tempBaseLine = -1
        if volBeforeMin_2 < volBeforeMin_1: # 1분전 음봉의 거래량이 그 이전 캔들의 거래량보다 크면
            if candleBeforeMin_1[1] == candleBeforeMin_1[4]: # 음봉의 시가와 종가가 같으면
                tempBaseLine = candleBeforeMin_1[2] # 음봉의 고가를 임시 저항선으로 설정
            else:
                tempBaseLine = candleBeforeMin_1[1] # 음봉의 시가를 임시 저항선으로 설정

        if 0 < tempBaseLine:
            if sellBuyAIElement.tempBaseLine != tempBaseLine:
                sellBuyAIElement.tempBaseLine = tempBaseLine
                print("시간:", time, "임시저항선 갱신:", sellBuyAIElement.tempBaseLine)
                self.Log(str(time) + "임시저항선 갱신: " + str(sellBuyAIElement.tempBaseLine))
        return

    def GetCurTime(self):
        now = datetime.datetime.now()
        return now.strftime('%m%d %H:%M:%S')

    def IsOverBaseLines(self, name, curPrice, tempBaseLine, stockInfo, isKosdaq):
        bidUnitUp = self.GetBidUnitUp(tempBaseLine, isKosdaq) # 상위 호가단위

        curTime = self.GetCurTime()

        if curPrice < (tempBaseLine + bidUnitUp): # 임시저항선을 못 넘었으면
            # print(curTime, name, "임시저항선 돌파실패", "현재가:", curPrice, "임시저항선:", tempBaseLine)
            return False

        # # 임시저항선보다 2호가 이하로 큰 기준선이 존재하면
        bidUnitUp2 = self.GetBidUnitUp(tempBaseLine + bidUnitUp, isKosdaq)
        up2ThanTempBaseLine = tempBaseLine + bidUnitUp + bidUnitUp2
        hasUp2ThanTempBaseLine = False

        baseLines = stockInfo.GetAllBaseLines()
        finalBaseLine = 0
        for line in baseLines:
            if line[1] <= tempBaseLine:
                continue

            if line[1] <= up2ThanTempBaseLine and finalBaseLine < line[1]:
                finalBaseLine = line[1]
                hasUp2ThanTempBaseLine = True

        if hasUp2ThanTempBaseLine == True:
            if curPrice <= finalBaseLine:
                print(curTime, name, "임시저항선 근처 기준선 돌파 실패", "현재가:", curPrice, "기준선:", finalBaseLine)
                return False
            else:
                print(curTime, name, "임시저항선 근처 기준선 돌파", "현재가:", curPrice, "기준선:", finalBaseLine)
            


        # # 임시저항선보다 1호가 위에 있는 기준선이 존재하면
        # up1ThanTempBaseLine = tempBaseLine + bidUnitUp
        # hasUp1ThanTempBaseLine = False
        # for line in baseLines:
        #     if line[1] <= tempBaseLine:
        #         continue

        #     if line[1] <= up1ThanTempBaseLine:
        #         hasUp1ThanTempBaseLine = True
        #         break

        
        # if hasUp1ThanTempBaseLine == True and curPrice <= up1ThanTempBaseLine:
        #     print(curTime, "임시저항선 근처 기준선 돌파 실패", "현재가:", curPrice, "기준선:", up1ThanTempBaseLine)
        #     return False        

        return True

    # 호가단위를 구함
    def GetBidUnitUp(self, basePrice, isKosdaq):
        bidUnit = 1

        if 1000 <= basePrice and basePrice < 5000:
            bidUnit = 5
        elif 5000 <= basePrice and basePrice < 10000: 
            bidUnit = 10
        elif 10000 <= basePrice and basePrice < 50000: 
            bidUnit = 50            
        elif 50000 <= basePrice and basePrice < 100000: 
            bidUnit = 100 
        elif 100000 <= basePrice and basePrice < 500000: 
            if isKosdaq == True:
                bidUnit = 100
            else:
                bidUnit = 500
        elif 500000 <= basePrice: 
            if isKosdaq == True:
                bidUnit = 100
            else:
                bidUnit = 1000            

        return bidUnit    

    def GetBidUnitDown(self, basePrice, isKosdaq):
        return self.GetBidUnitUp(basePrice - 1, isKosdaq)         

    def Buy(self, sellBuyAIElement, baseLines, curPrice, stockCount):
        sellBuyAIElement.tempBaseLine = -1 # 임시저항선 제거
        # sellBuyAIElement.cutLine = self.GetCutLine(curPrice, baseLines, sellBuyAIElement.isKosdaq)
        # sellBuyAIElement.targetPrice = self.GetTargetPrice(curPrice, baseLines, sellBuyAIElement.isKosdaq)
        sellBuyAIElement.buyPrices.append(curPrice)
        sellBuyAIElement.buyStockCounts.append(stockCount)

        if sellBuyAIElement.didBuy == True:
            sellBuyAIElement.didBuyExtra = True
        else:    
            sellBuyAIElement.didBuy = True

        buyTime = self.GetCurTime()
        sellString = buyTime + " " + sellBuyAIElement.name + " " + str(stockCount) + "주 " + "매수 - " + "1주 가격: " + str(curPrice) + " 목표가격: " + str(sellBuyAIElement.targetPrice)
        SendMsg(sellString)

        self.Log(sellString)
        return  

    def GetTargetPrice(self, curPrice, baseLines, isKosdaq):
        bidUnitUp = self.GetBidUnitUp(curPrice, isKosdaq)
        bidUnitUp2 = self.GetBidUnitUp(curPrice + bidUnitUp, isKosdaq)
        tempTargetPrice = curPrice + bidUnitUp + bidUnitUp2

        # print("임시 목표가격:" , tempTargetPrice, "1호가:", bidUnitUp, "1호가 다음:", bidUnitUp2)

        # 현재가보다 2호가 더 높은 저항선들 중에서 가장 작은 값을 찾음
        targetPrice = tempTargetPrice
        minLine = 100000000
        find = False
        for line in baseLines:
            # print("목표가를 위한 기준선:", line[1])

            if line[1] < tempTargetPrice:
                continue
            
            if line[1] < minLine:
                targetPrice = line[1]
                minLine = line[1]
                # print("최종가격 후보: ", line[1])
                find = True

        # # 저항선보다 1호가 밑의 값을 목표가로 설정
        # bidUnitDown = self.GetBidUnitDown(targetPrice, isKosdaq)
        # targetPrice = targetPrice - bidUnitDown

        if find == False:
            print("목표가:", str(targetPrice), "에 해당하는 저항선 없음")
        else:
            print("최종 목표가격:" , targetPrice)

        return targetPrice

    def GetCutLine(self, curPrice, baseLines, isKosdaq):
        # 현재가 이하인 지지선들 중에서 가장 큰 값을 찾음
        cutLine = 0
        for line in baseLines:
            if curPrice < line[1]:
                continue
            
            if cutLine < line[1]:
                cutLine = line[1]

        # 기준선보다 1호가 밑의 값을 손절가로 설정
        bidUnitDown = self.GetBidUnitDown(cutLine, isKosdaq)
        # print("임시저항선 기준선:", cutLine, "임시저항선: ", cutLine - bidUnitDown)
        cutLine = cutLine - bidUnitDown

        #
        minCutLine = curPrice - (curPrice * 0.01)
        if cutLine < minCutLine: # 손절가가 현재가와 1% 넘게 차이나면
            cutLine = minCutLine

        return cutLine

    def SellAll(self, sellBuyAIElement, curPrice):
        profit = curPrice * sellBuyAIElement.GetTotalBuyStockCount() - sellBuyAIElement.GetTotalBuyPrices()
        profitRate = ((profit / sellBuyAIElement.GetTotalBuyPrices()) - 0.0033) * 100
        self.totalDealCount = self.totalDealCount + 1
        self.totalProfit = self.totalProfit + (sellBuyAIElement.GetTotalBuyPrices() * profitRate)

        if 0 < profitRate:
            self.totalSuccess = self.totalSuccess + 1
        else:
            self.totalFail = self.totalFail + 1                

        self.totalRate = (self.totalProfit / SEED_MONEY) * 100
        sellTime = self.GetCurTime()


        # sellString = sellTime + " " + sellBuyAIElement.name + " " + str(sellBuyAIElement.code) + " " + str(sellBuyAIElement.buyStockCount) + "주 " + "매도 - " + "1주 가격: " + str(curPrice) + " 수익률: " + str(profitRate) + "%" + " 현재수익:" + str(self.totalProfit) + " 거래횟수:" + str(self.totalDealCount) + "총수익률:" + str(self.totalRate)
        sellString = sellBuyAIElement.name + " " + "매도 " + "1주: " + str(curPrice) + " 수익률: " + str(profitRate) + "%" + " 현재수익:" + str(self.totalProfit) + " 횟수:" + str(self.totalDealCount) + " 총수익률:" + str(self.totalRate) + "성공:" + str(self.totalSuccess) + " 실패:" + str(self.totalFail)
        SendMsg(sellString)
        self.Log(sellString)

        # 초기화
        sellBuyAIElement.cutLine = -1
        sellBuyAIElement.didBuy = False
        sellBuyAIElement.didBuyExtra = False     
        sellBuyAIElement.didSellPart = False
        sellBuyAIElement.buyPrices.clear()
        sellBuyAIElement.buyStockCounts.clear()
        sellBuyAIElement.targetPrice = 0
        sellBuyAIElement.tempBaseLine = -1
        return  

    def SellPart(self, sellBuyAIElement, curPrice):
        # partCount = 5

        # sellBuyAIElement.didSellPart = True
        # sellBuyAIElement.buyStockCount = sellBuyAIElement.buyStockCount - partCount

        # profit = curPrice - sellBuyAIElement.buyPrice
        # profitRate = (profit / sellBuyAIElement.buyPrice) * 100
 
        # sellTime = self.GetCurTime()

        # self.totalProfit = self.totalProfit + (profit * partCount)
        # sellString = sellTime + " " + sellBuyAIElement.name + " " + str(partCount) + "주 " +  "일부매도 - " + "1주 가격: " + str(curPrice) + "수익률: " + str(profitRate) + "%" + " 현재수익:" + str(self.totalProfit)
        # SendMsg(sellString)
        # self.Log(sellString)

        return     

    def IsSellPowerLarger(self, sellBuyAIElement, CHECK_TIME):
        MIN_CHECK_RATE = 0.6
        VELOCITY_DIFF = 1 # 매도세가 강한 속도차이
        OFFER_BID_RATE = 0.8 # 매도세가 강한 매도매수 잔량비율        

        checkValue = 0
        totalOfferBidRate = 0
        buyVelocity = sellBuyAIElement.buyVolCount / CHECK_TIME
        sellVelocity = sellBuyAIElement.sellVolCount / CHECK_TIME

        isSellPowerLarger = False

        if MIN_CHECK_RATE <= buyVelocity or MIN_CHECK_RATE <= sellVelocity:
            if sellBuyAIElement.sellVolCount < 1: # 매도갯수가 0
                checkValue = buyVelocity / 1
            else:
                checkValue = buyVelocity / sellVelocity              

            if sellBuyAIElement.totalBid == 0 or sellBuyAIElement.totalOffer == 0:
                if checkValue <= VELOCITY_DIFF: # 매도세가 더 강하면
                   isSellPowerLarger = True 

                print("[매도세체크]", "매수속도:", buyVelocity, "매도속도:", sellVelocity, "속도비율:", checkValue)            
                self.Log("[매도세체크]" + " 매수속도: " + str(buyVelocity) + " 매도속도: " + str(sellVelocity) + " 속도비율: " + str(checkValue))
            else:    
                totalOfferBidRate = sellBuyAIElement.totalOffer / sellBuyAIElement.totalBid
                # if totalOfferBidRate <= OFFER_BID_RATE and checkValue <= VELOCITY_DIFF: # 매도세가 더 강하면
                if checkValue <= VELOCITY_DIFF: # 매도세가 더 강하면
                    isSellPowerLarger = True
                
                print("[매도세체크]", "매수속도:", buyVelocity, "매도속도:", sellVelocity, "매도매수잔량 비율:", totalOfferBidRate, "속도비율:", checkValue)  
                self.Log("[매도세체크]" + " 매수속도: " + str(buyVelocity) + " 매도속도: " + str(sellVelocity) + " 매도매수잔량 비율: " + str(totalOfferBidRate) + " 속도비율: " + str(checkValue))

        if isSellPowerLarger == True:
            self.Log("매도세 강함")

        return isSellPowerLarger
                
    def IsBuyPowerLarger(self, sellBuyAIElement, CHECK_TIME):
        # MIN_CHECK_RATE = 0.6
        # VELOCITY_DIFF = 3 # 매수세가 강한 속도차이
        # OFFER_BID_RATE = 1.2 # 매수세가 강한 매도매수 잔량비율

        # checkValue = 0
        # totalOfferBidRate = 0
        # buyVelocity = sellBuyAIElement.buyVolCount / CHECK_TIME
        # sellVelocity = sellBuyAIElement.sellVolCount / CHECK_TIME

        # isBuyPowerLarger = False

        # if MIN_CHECK_RATE < buyVelocity or MIN_CHECK_RATE < sellVelocity:
        #     if sellBuyAIElement.sellVolCount < 1: # 매도갯수가 0
        #         checkValue = buyVelocity / 1
        #     else:
        #         checkValue = buyVelocity / sellVelocity              

        #     if sellBuyAIElement.totalBid == 0 or sellBuyAIElement.totalOffer == 0:
        #         if VELOCITY_DIFF <= checkValue: # 매수세가 더 강하면
        #            isBuyPowerLarger = True 

        #         print("[매수세체크]", "매수속도:", buyVelocity, "매도속도:", sellVelocity, "속도비율:", checkValue)            
        #     else:    
        #         totalOfferBidRate = sellBuyAIElement.totalOffer / sellBuyAIElement.totalBid
        #         if OFFER_BID_RATE <= totalOfferBidRate and VELOCITY_DIFF <= checkValue: # 매수세가 더 강하면
        #             isBuyPowerLarger = True
                
        #         print("[매수세체크]", "매수속도:", buyVelocity, "매도속도:", sellVelocity, "매도매수잔량 비율:", totalOfferBidRate, "속도비율:", checkValue)        

        # return isBuyPowerLarger
        return True

    # def OnUpdateStockCur(self, stockInfo, chart, name, vol, cprice, cVolType, cVol, timess):
    #     code = stockInfo.code

    #     # 해당 종목에 대한 정보가 없으면 추가
    #     if False == (code in self.elements):
    #         element = SellBuyAIElement()
    #         element.name = name
    #         element.isKosdaq = stockInfo.isKosdaq
    #         self.elements[code] = element

    #     if chart.HasCandle(code) == False: # 아직 캔들이 만들어지지 않았으면
    #         return

    #     e = self.elements[code]

    #     # 체결정보 갱신
    #     CHECK_TIME = 5
    #     MIN_CHECK_RATE = 0.6

    #     if e.sellBuyPowerCheckStartTime < 0:
    #         e.sellBuyPowerCheckStartTime = timess
    #         e.buyVolCount = 0
    #         e.sellVolCount = 0

    #     if 1 < cVol: # 체결량이 1보다 큰 것들만 체크
    #         if cVolType == ord('1'):
    #             e.buyVolCount = e.buyVolCount + 1
    #         else:
    #             e.sellVolCount = e.sellVolCount + 1

    #     if CHECK_TIME <= timess - e.sellBuyPowerCheckStartTime:
    #         e.sellBuyPowerCheckStartTime = -1

    #     if e.didBuy == True: # 종목을 샀으면
    #         if self.NoHasHighBaseLineThanCurPrice(stockInfo, cprice) == True: # 현재가보다 높은 저항선이 없으면
    #             if cprice <= e.cutLine or self.IsSellPowerLarger(e, CHECK_TIME): # 손절가에 도달했거나 매도세가 강하면
    #                 self.SellAll(e, cprice) # 손절
    #         else:    
    #             if e.didSellPart == False and e.targetPrice <= cprice: # 아직 매도하지 않았고, 목표가에 도달했으면
    #                 self.SellPart(e, cprice) # 일부 매도

    #                 # 손절가 갱신
    #                 e.cutLine = self.GetCutLine(cprice, stockInfo.GetAllBaseLines(), e.isKosdaq)
    #                 print("손절가 갱신:", e.cutLine)
    #                 self.Log("손절가 갱신: " + str(e.cutLine))
    #             elif e.targetPrice <= cprice and self.IsSellPowerLarger(e, CHECK_TIME): # 현재가가 목표가를 돌파했고 매도세가 강하면
    #                 self.SellAll(e, cprice) # 손절
    #             elif cprice <= e.cutLine: # 손절가에 도달했으면
    #                 self.SellAll(e, cprice) # 손절

    #     else: # 종목을 안 샀으면
    #         if self.NoHasHighBaseLineThanCurPrice(stockInfo, cprice) == True: # 현재가보다 높은 저항선이 없으면
    #             print("현재가보다 높은 저항선이 없음")
    #             return

    #         # 임시저항선 갱신
    #         self.UpdateTempBaseLine(e, chart, code, timess)
            
    #         if e.tempBaseLine < 0: # 임시저항선이 없으면
    #             return

    #         # 현재가가 20일선 아래에 있으면
    #         # return    

    #         # 현재가가 저항선을 돌파 못했으면
    #         if self.IsOverBaseLines(name, cprice, e.tempBaseLine, stockInfo, e.isKosdaq) == False:
    #             return

    #         print("--------저항선 돌파-------")    
    #         self.Log("저항선 돌파")

    #         if self.IsBuyPowerLarger(e, CHECK_TIME) == True: # 매수세가 강하면
    #             self.Buy(e, stockInfo.GetAllBaseLines(), cprice) # 매수
    #         else:
    #             curTime = self.GetCurTime()
    #             print(curTime, name, "매수세가 약함")
  



    #     # if False == (code in self.elements):
    #     #    self.elements[code] = SellBuyAIElement()
    #     #    self.elements[code].startTime = timess
        
    #     # e = self.elements[code]

    #     # if cVolType == ord('1'):
    #     #     e.buyVolCount = e.buyVolCount + 1
    #     # else:
    #     #     e.sellVolCount = e.sellVolCount + 1    

    #     # checkTime = 0
    #     # if e.didBuy: # 종목을 샀으면
    #     #     checkTime = self.SELL_TIMING_CHECK_TIME
    #     # else:
    #     #     checkTime = self.BUY_TIMING_CHECK_TIME

    #     # if checkTime <= timess - e.startTime:
    #     #     buyVelocity = e.buyVolCount / checkTime
    #     #     sellVelocity = e.sellVolCount / checkTime

    #     #     print(name, "매수:", e.buyVolCount, "매도:", e.sellVolCount)

    #     #     checkValue = 0
    #     #     if e.sellVolCount < 1: # 매도갯수가 0
    #     #         checkValue = buyVelocity / 1
    #     #     else:
    #     #         checkValue = buyVelocity / sellVelocity

    #     #     if e.didBuy: # 종목을 샀으면
    #     #         # 매도 타이밍 체크
    #     #         if checkValue <= self.SELL_TIMING_VALUE:
    #     #             # 현재가로 매도
    #     #             rate = ((cprice - e.buyPrice) / e.buyPrice) * 100
    #     #             self.totalRate = self.totalRate + rate
    #     #             sellString = name + " 매도 - " + "가격: " + str(cprice) + "수익률: " + str(rate) + "%" + "총수익률:" + str(self.totalRate)
    #     #             SendMsg(sellString)
    #     #             e.didBuy = False
    #     #     else: 
    #     #         # 매수 타이밍 체크
    #     #         print(name, "매수 타이밍 체크", checkValue)
    #     #         if self.BUY_TIMING_VALUE <= checkValue:
    #     #             # 현재가로 매수
    #     #             sellString = name + " 매수 - " + " 가격: " + str(cprice)
    #     #             SendMsg(sellString)
    #     #             e.buyPrice = cprice
    #     #             e.didBuy = True

    #     #     e.startTime = timess
    #     #     e.buyVolCount = 0
    #     #     e.sellVolCount = 0
    #     return

    # 기준 시간대에서 거대매수 물량이 들어왔는지 확인
    def DoBigBuyerEnter(self):
        return True

    # 좋은 캔들인지 확인
    def IsGoodCandle(self, curPrice, isKosdaq, open, close, high, low):
        if self.IsPlusCandle(open, close) == False: # 양봉이 아니면
            return False

        basePrice = open + self.GetBidUnitUp(open, isKosdaq)
        if curPrice <= basePrice: # 현재가가 시가의 1호가 위의 가격보다 크지 않으면
            return False

        # minRate = (high - low) * 0.6
        minRate = (high - low) * 0.7
        if minRate <= (close - open):
            return True
        else:
            return False

    def OnUpdateStockCur(self, stockInfo, chart, name, vol, cprice, cVolType, cVol, timess):
        BUY_TIME_SECOND = 49
        CUTLINE_MIN_RATE = 0.01
        # MINUS_CANDLE_MAX_COUNT_IN_FRONT_OF = 4
        MINUS_CANDLE_MAX_COUNT_IN_FRONT_OF = 7
        MIN_DIFF_RATE_BETWEEN_MIN_LOW_N_MAX_LOW = 0.005
        # DIFF_RATE_BETWEEN_CUR_N_MIN_LOW = 0.01
        # DIFF_RATE_BETWEEN_CUR_N_MIN_LOW = 0.02
        DIFF_RATE_BETWEEN_CUR_N_MIN_LOW = 0.025
        MAX_DIFF_RATE_BETWEEN_CUR_PRICE_N_AVERAGE20 = 0.008

        code = stockInfo.code

        # 해당 종목에 대한 정보가 없으면 추가
        if False == (code in self.elements):
            element = SellBuyAIElement()
            element.code = code
            element.name = name
            element.isKosdaq = stockInfo.isKosdaq
            self.elements[code] = element

        if chart.HasCandle(code) == False: # 아직 캔들이 만들어지지 않았으면
            return

        e = self.elements[code]

        if e.didBuy == True: # 종목을 샀으면
            # 고가 갱신
            if e.high < cprice:
                e.high = cprice

            if cprice <= e.cutLine: # 손절가에 도달했으면
                self.SellAll(e, cprice) # 손절

            else:
                if e.targetPrice <= cprice: # 목표가에 도달했으면
                    self.SellAll(e, cprice) # 손절 
                # if e.arrivedAtTarget == True: # 목표가에 도달해봤냐
                #     # 손절가 갱신
                #     e.cutLine = e.buyPrices[0] + ((e.high - e.buyPrices[0]) / 2)
                # else:
                #     if e.targetPrice <= cprice: # 목표가에 도달했으면
                #         e.cutLine = e.buyPrices[0] + ((e.high - e.buyPrices[0]) / 2) # 손절가 갱신
                #         e.arrivedAtTarget = True

        else: # 종목을 안 샀으면
            # 현재 시간의 초가 40초 이하이면
            # second = datetime.datetime.now().today().second
            # if second <= BUY_TIME_SECOND:
            #     return

            hh, mm = divmod(timess, 10000)
            mm, tt = divmod(mm, 100)
            if tt <= BUY_TIME_SECOND:
                return
                  
            if self.DoBigBuyerEnter() == False: # 현재 시점에 거대매수 물량이 안 들어왔으면
                return

            rightDirInfo = []
            isRightDir = chart.IsRightDir(code, timess, rightDirInfo)
            if isRightDir == False: # 우상향이 아니면
                return

            curCandle = chart.GetCandleByTime(code, timess)

            # # 현재가와 20분이평선과의 격차가 일정 이상이면 매수 금지
            # maxDiffBetweenCurPriceAndAverageMin20 = cprice * MAX_DIFF_RATE_BETWEEN_CUR_PRICE_N_AVERAGE20
            # curAverageMin20 = chart.GetAverageCloseMin20(code, curCandle[0])
            # if maxDiffBetweenCurPriceAndAverageMin20 <= abs(cprice - curAverageMin20): 
            #     # print("HHMM:", curCandle[0], "curAverageMin20:", curAverageMin20, "기준격차:", maxDiffBetweenCurPriceAndAverageMin20, "현재격차:", abs(cprice - curAverageMin20))
            #     return

            #
            if self.IsGoodCandle(cprice, e.isKosdaq, curCandle[1], curCandle[4], curCandle[2], curCandle[3]) == False: # 현재 캔들이 좋은 캔들이 아니면
                return

            hhmmBeforeMin_1 = chart.GetHHMMBeforeMin_1ByTime(timess)
            if chart.HasCandleByHHMM(code, hhmmBeforeMin_1) == False:
                return
  
            candleBeforeMin_1 = chart.GetCandle(code, hhmmBeforeMin_1)
            if self.IsPlusCandle(candleBeforeMin_1[1], candleBeforeMin_1[4]) == False: # 1분전 캔들이 양봉이 아니면
                return

            if curCandle[3] <= candleBeforeMin_1[3]: # 현재캔들의 저가 <= 1분전 캔들의 저가
                return

            hhmmBeforeMin_2 = chart.GetHHMMBeforeMin_1(hhmmBeforeMin_1)
            if chart.HasCandleByHHMM(code, hhmmBeforeMin_2) == False:
                return      

            candleBeforeMin_2 = chart.GetCandle(code, hhmmBeforeMin_2)
            if self.IsMinusCandle(candleBeforeMin_2[1], candleBeforeMin_2[4]) == False: # 2분전 캔들이 음봉이 아니면
                return                    

            if candleBeforeMin_1[3] < candleBeforeMin_2[3]: # 1분전 캔들의 저가 < 2분전 캔들의 저가
                return

            hhmmBeforeMin_3 = chart.GetHHMMBeforeMin_1(hhmmBeforeMin_2)
            if chart.HasCandleByHHMM(code, hhmmBeforeMin_3) == False:
                return    

            candleBeforeMin_3 = chart.GetCandle(code, hhmmBeforeMin_3)                
            if self.IsRealMinusCandle(candleBeforeMin_3[1], candleBeforeMin_3[4]) == False: # 3분전 캔들이 음봉이 아니면
                return

            hhmmBeforeMin_4 = chart.GetHHMMBeforeMin_1(hhmmBeforeMin_3)     
            if chart.HasCandleByHHMM(code, hhmmBeforeMin_4) == False:
                return            

            # candleBeforeMin_4 = chart.GetCandle(code, hhmmBeforeMin_4)      
            # if candleBeforeMin_4[3] <= candleBeforeMin_3[3]: # 3분전 캔들의 저가가 4분전 캔들의 저가보다 크거나 같으면
            #     return

            # 3분전 캔들 앞에 있는 연속적인 음봉들을 구함(list)
            minusCandlesInFrontOfCandleBeforeMin_3 = chart.GetMinusCandlesInFrontOf(code, hhmmBeforeMin_3)

            # 연속적인 음봉의 갯수가 일정 수를 넘어서면
            if MINUS_CANDLE_MAX_COUNT_IN_FRONT_OF - 2 < len(minusCandlesInFrontOfCandleBeforeMin_3):
                print("연속적인 음봉의 갯수를 넘김", len(minusCandlesInFrontOfCandleBeforeMin_3))
                return

            # 음봉들 중에서 가장 큰 저가와 가장 작은 저가를 구함
            minusCandles = []
            minusCandles.append(candleBeforeMin_2)
            minusCandles.append(candleBeforeMin_3)            
            minusCandles.extend(minusCandlesInFrontOfCandleBeforeMin_3)

            minLow = minusCandles[0][3]
            maxLow = minusCandles[0][3]
            for mCandle in minusCandles:
                if mCandle[3] < minLow:
                    minLow = mCandle[3]
                elif maxLow < mCandle[3]:    
                    maxLow =  mCandle[3]

            # 가장 큰 저가와 가장 작은 저가 사이의 비율이 특정값 미만이면(낙차확인)
            diffRateBetweenMinAndMaxLow = (maxLow - minLow) / maxLow
            if diffRateBetweenMinAndMaxLow < MIN_DIFF_RATE_BETWEEN_MIN_LOW_N_MAX_LOW:
                print("낙차가 적음", diffRateBetweenMinAndMaxLow)
                return

            # 가장 작은 저가와 현재가와의 사이의 비율이 특정값 이상이면(손절 리스크)
            diffRateBetweenCurAndMinLow = (cprice - minLow) / cprice
            if DIFF_RATE_BETWEEN_CUR_N_MIN_LOW <= diffRateBetweenCurAndMinLow:
                print("현재가와 가장 작은 저가와의 비율이 큼", diffRateBetweenCurAndMinLow)
                return

            print("매수시간:", curCandle[0], "범위 안의 ", "큰 저가:", maxLow, "작은 저가:", minLow)

            # 우상향 데이터 기록
            rightDirLog = []
            for dirInfo in rightDirInfo:
                rightDirLog.append("[" + str(code) +  "]" + " [" + str(dirInfo[0]) + "]" + " 20분평균값: " + str(dirInfo[1]))
            
            dirFilePath = "./우상향/"+ str(code) + " " + datetime.datetime.now().strftime('%H%M%S') + ".json"
            self.SaveLogsDir(dirFilePath, rightDirLog)

            
            ## 손절가 설정(가장 작은 저가의 1호가 밑)
            # e.cutLine = minLow - self.GetBidUnitDown(minLow, e.isKosdaq)
            # 손절가 설정 // 현재가의 1% 밑에 해당하는 가격
            e.cutLine = cprice - (cprice * 0.01)
            # e.cutLine = cprice - (cprice * 0.007)
            e.arrivedAtTarget = False
            e.high = cprice
            e.extraBuyLine = e.cutLine + self.GetBidUnitUp(e.cutLine, e.isKosdaq)
            print("추가매수구간:", e.extraBuyLine, "손절가:", e.cutLine)

            # 목표가 설정(현재가보다 1% 위)
            # e.targetPrice = cprice + (cprice * 0.005)
            e.targetPrice = cprice + (cprice * 0.01)

            # 매수
            buyCount = math.trunc((SEED_MONEY / SEED_PART_COUNT) / cprice)
            self.Buy(e, stockInfo.GetAllBaseLines(), cprice, buyCount)

        return


    def OnUpdateStockBid(self, stockInfo, chart, offer, offervol, bid, bidvol, totalOffer, totalBid):
        code = stockInfo.code

        if False == (code in self.elements): # 해당 종목에 대한 정보가 없으면
            return

        e = self.elements[code]
        e.totalOffer = totalOffer # 총 매도잔량
        e.totalBid = totalBid # 총 매수잔량


        # print(self.GetCurTime(), "호가정보", "총 매도잔량:", e.totalOffer, "총 매수잔량:", e.totalBid)

        return

    pass

#####################################################################################

async def Process():
        # 로그인 시간까지 대기
        print('로그인 대기중')  
        now = datetime.datetime.now()
        startDate = datetime.datetime(now.year, now.month, now.day, 8, 50, 0)
        if now < startDate:
            await asyncio.sleep((startDate - now).seconds) 
        
        # 로그인
        print('로그인 시작')
        # isLogin = False
        # while(False == isLogin):
        #     isLogin = await Login()
        await Login()

        # 실행시간까지 대기 
        print('대기중')  
        now = datetime.datetime.now()
        startDate = datetime.datetime(now.year, now.month, now.day, 9, 3, 0)
        if now < startDate:
            await asyncio.sleep((startDate - now).seconds)       
     
async def Login():
    isSuccess = False

    try:
        os.system('taskkill /IM coStarter* /F /T')
        os.system('taskkill /IM CpStart* /F /T')
        os.system('taskkill /IM DibServer* /F /T')
        os.system('wmic process where "name like \'%coStarter%\'" call terminate')
        os.system('wmic process where "name like \'%CpStart%\'" call terminate')
        os.system('wmic process where "name like \'%DibServer%\'" call terminate')
        time.sleep(5)        

        app = application.Application()
        app.start('C:\CREON\STARTER\coStarter.exe /prj:cp /id:JH1332 /pwd:antk84$ /pwdcert:shffjrkfrp84$ /autostart')
        time.sleep(60)  

        # 연결 여부 체크
        objCpCybos = win32com.client.Dispatch("CpUtil.CpCybos")
        bConnect = objCpCybos.IsConnect
        if (bConnect == 0):
            print("PLUS가 정상적으로 연결되지 않음. ")
        else:
            print('로그인 성공')                       
            isSuccess = True
    except:
            print("로그인 도중 예외발생")
            isSuccess = False  

    return isSuccess    


if __name__ == '__main__': 
    try:
        asyncio.run(Process())

        app = QApplication(sys.argv)
        myWindow = MyWindow()
        myWindow.show()
        myWindow.btnClicked_CollectDatas()        
        app.exec_() 


    except Exception as ex:
        print(ex)
        pass

