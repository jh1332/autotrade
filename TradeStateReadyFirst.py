from TradeFsmMgr import TradeStateBase, TradeStateType
import Constants

class TradeStateReadyFirst(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.ReadyFirst.value
        # self.priceToOver = self.tradeAlgorithm.GetPriceToOver(self.code)
        # self.readyLevel = 0

        return

    def GetStateName(self): 
        return "초기대기"   

    def Enter(self, args):
        # self.readyLevel = 0
        self.volForCheckBigVols = self.tradeAlgorithm.GetVolForCheckBigVols(self.code)
        self.candlesForCheckBigVols = []
        return

    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        curCandle = self.GetCurCandle()

        # if 0 == len(self.candlesForCheckBigVols): # 아직 전조증상을 체크를 시작 못했으면
        #     if self.volForCheckBigVols <= curCandle.vol:
        #         self.candlesForCheckBigVols.append(curCandle)
        # else: # 전조증상 체크중이면
        #     # 다음 캔들의 거래량도 기준 거래량 이상이면 계속 진행 
        #     # 아니면 초기화하고 다시 체크
        #     lastCandle = self.candlesForCheckBigVols[len(self.candlesForCheckBigVols) - 1]
        #     if 6 <= curCandle.time - lastCandle.time: # 마지막 캔들 다음 캔들이 완성됐으면
        #         nextCandle = self.GetCandle(self.GetCurCandleIndex() - 1) 
        #         if self.volForCheckBigVols <= nextCandle.vol:
        #             self.candlesForCheckBigVols.append(nextCandle)
        #             # 거래량 전조증상을 느끼기 위한 캔들이 4개이면 매수상태로 전환
        #             if 4 <= len(self.candlesForCheckBigVols):
        #                 args = []
        #                 self.fsm.ChangeState(TradeStateType.ZapBuy, args)
        #         else:    
        #             self.candlesForCheckBigVols.clear()

        return

    def OnUpdateStockBid(self, code, stockBid): # 호가창 데이터                        
        curCandle = self.GetCurCandle()
        if curCandle.time == 918:
            i = 0

        return    

 

    # def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
    #     # # 9시 54이 지났으면 잽매수 시작
    #     # if 95400 <= stockCur[5]:
    #     #     maxMoneyForZap = int(self.tradeAlgorithm.GetCodeInfo(self.code).totalMoney * Constants.MAX_MONEY_RATE_FOR_ZAP_BUY)
    #     #     args = []
    #     #     args.append(maxMoneyForZap) # 잽매수할 최대금액
    #     #     self.fsm.ChangeState(TradeStateType.ZapBuy, args)

    #     # 대기 0단계 - 좋은 캔들이 강한 거래대금으로 뚫어야 하는 가격을 뚫은 후에
    #     #           그 다음 캔들도 뚫어야 하는 가격 위의 양봉이 나오길 기다리는 상태
    #     # 대기 1단계 - 이후 음봉이 나오길 기다림
    #     # 대기 2단계 - 음봉이 나온 후 뚫어야 하는 가격 위의 양봉이 나오길 기다리는 상태
    #     #           - 양봉을 기다리다가 음봉이 뚫어하는 가격 밑으로 빠지면 대기 0단계로 초기화

    #     if 0 == self.readyLevel:
    #         self.UpdateLevel_0(stockCur)
    #     elif 1 == self.readyLevel:
    #         self.UpdateLevel_1(stockCur)
    #     elif 2 == self.readyLevel:
    #         self.UpdateLevel_2(stockCur)            

    #     return   
          
    # def UpdateLevel_0(self, stockCur):     
    #     curCandleIndex = self.GetCurCandleIndex()
    #     prevCandle = self.GetCandle(curCandleIndex - 1)        
    #     if prevCandle is None: # 이전 캔들이 없으면
    #         return

    #     # 현재 캔들의 이전 캔들이 양봉이고 시가와 종가가 모두 뚫어야 하는 가격 위에 있고
    #     if False == prevCandle.IsPlusCandle():
    #         return

    #     if prevCandle.open <= self.priceToOver or prevCandle.close <= self.priceToOver:
    #         return

    #     # 그 이전 캔들이 좋은캔들이고 거래대금이 크고 종가가 뚫어야 하는 가격 위에 있으면 1단계로 전환              
    #     prevCandleOfPrev = self.GetCandle(curCandleIndex - 2)
    #     if prevCandleOfPrev is None: # 이전 캔들의 이전 캔들이 없으면
    #         return

    #     if False == prevCandleOfPrev.IsPlusCandle():
    #         return 

    #     if prevCandleOfPrev.close <= self.priceToOver:
    #         return

    #     if 7000000000 <= prevCandleOfPrev.tradeMoney:
    #         self.readyLevel = 1
    #         print("종목:", self.code, "시간:", stockCur[5], "에 강한 거래대금으로 뚫었음")
    #         return

    #     return    

    # def UpdateLevel_1(self, stockCur):
    #     return

    # def UpdateLevel_2(self, stockCur):
    #     return           