import sys
from enum import Enum

class Dir(Enum):
  UP = 0
  RIGHT = 1
  DOWN = 2
  LEFT = 3

# 상하좌우에 따른 이동 함수 결정하기
def MoveBoard(board, dir):
  mergeCheckBoard = [[0 for _ in range(TILE_COUNT)] for _ in range(TILE_COUNT)]
  # 상
  if Dir.UP == dir:
    for y in range(TILE_COUNT):
      for x in range(TILE_COUNT):
        if board[y][x] == 0:
          continue
        MoveTile(y, x, dir, board, mergeCheckBoard)

  # 하
  elif Dir.DOWN == dir:
    for y in range(TILE_COUNT):
      for x in range(TILE_COUNT):
        if board[TILE_COUNT-1-y][x] == 0:
          continue
        MoveTile(TILE_COUNT-1-y, x, dir, board, mergeCheckBoard)        

  # 좌  
  elif Dir.LEFT == dir:
    for x in range(TILE_COUNT):
      for y in range(TILE_COUNT):
        if board[y][x] == 0:
          continue
        MoveTile(y, x, dir, board, mergeCheckBoard)

  # 우 
  elif Dir.RIGHT == dir:
    for x in range(TILE_COUNT):
      for y in range(TILE_COUNT):
        if board[y][TILE_COUNT-1-x] == 0:
          continue
        MoveTile(y, TILE_COUNT-1-x, dir, board, mergeCheckBoard)


# 타일 이동
def MoveTile(y, x, dir, board, mergeCheckBoard):
  i = 0
  # 상
  if Dir.UP == dir:
    if y != 0:
      while y > i:
        i += 1
        if board[y-i][x] != 0:
          break    
      if board[y-i][x] == board[y][x] and mergeCheckBoard[y-i][x] == 0:
        board[y][x] = 0
        board[y-i][x] = board[y-i][x]*2
        mergeCheckBoard[y-i][x] = 1
      elif board[y-i][x] == 0:
        board[y-i][x] = board[y][x]
        board[y][x] = 0
      else:
        i -= 1
        if i > 0:
          board[y-i][x] = board[y][x]
          board[y][x] = 0
          mergeCheckBoard[y][x] = 0
  # 하
  elif Dir.DOWN == dir:
    if y != TILE_COUNT-1:
      while y + i < TILE_COUNT-1:
        i += 1
        if board[y+i][x] != 0:
          break    
      if board[y+i][x] == board[y][x] and mergeCheckBoard[y+i][x] == 0:
        board[y][x] = 0
        board[y+i][x] = board[y+i][x]*2
        mergeCheckBoard[y+i][x] = 1
      elif board[y+i][x] == 0:
        board[y+i][x] = board[y][x]
        board[y][x] = 0
      else:
        i -= 1
        if i > 0:
          board[y+i][x] = board[y][x]
          board[y][x] = 0
          mergeCheckBoard[y][x] = 0          
  # 좌
  elif Dir.LEFT == dir:
    if x != 0:
      while x > i:
        i += 1
        if board[y][x-i] != 0:
          break    
      if board[y][x-i] == board[y][x] and mergeCheckBoard[y][x-i] == 0:
        board[y][x] = 0
        board[y][x-i] = board[y][x-i]*2
        mergeCheckBoard[y][x-i] = 1
      elif board[y][x-i] == 0:
        board[y][x-i]= board[y][x]
        board[y][x] = 0
      else:
        i -= 1
        if i > 0:
          board[y][x-i]= board[y][x]
          board[y][x] = 0
          mergeCheckBoard[y][x] = 0

  # 우
  elif Dir.RIGHT == dir:
    if x != TILE_COUNT-1:
      while x + i < TILE_COUNT-1:
        i += 1
        if board[y][x+i] != 0:
          break    
      if board[y][x+i] == board[y][x] and mergeCheckBoard[y][x+i] == 0:
        board[y][x] = 0
        board[y][x+i] = board[y][x+i]*2
        mergeCheckBoard[y][x+i] = 1
      elif board[y][x+i] == 0:
        board[y][x+i] = board[y][x]
        board[y][x] = 0
      else:
        i -= 1
        if i > 0:
          board[y][x+i] = board[y][x]
          board[y][x] = 0
          mergeCheckBoard[y][x] = 0          

def SearchByDFS(board, curMoveCount):
  global answer
  
  # 모든 이동이 끝나면 최대값 구하기
  if MAX_MOVE_COUNT < curMoveCount:
    result = 0

    for y in range(TILE_COUNT):
      for x in range(TILE_COUNT):
        result = max(result, board[y][x])
    answer = max(answer, result)

  else:
    dirList = [Dir.UP, Dir.RIGHT, Dir.DOWN, Dir.LEFT]

    for i in range(len(dirList)):
      cloneBoard = [board[_][:] for _ in range(TILE_COUNT)]
      MoveBoard(cloneBoard, dirList[i])  
      SearchByDFS(cloneBoard, curMoveCount + 1)

TILE_COUNT = 4
MAX_MOVE_COUNT = 5

board = [[2, 4, 8, 2],
         [2, 2, 2, 2],
         [0, 4, 2, 4],
         [2, 2, 2, 4]]

answer = 0
SearchByDFS(board, 1)
print(answer)