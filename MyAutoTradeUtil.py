import time
import win32com.client
import datetime
from datetime import date, timedelta
from PyQt5.QtWidgets import *
import sys
from TradeLib import SendMsgToSlack
from TradeLib import GetGreatStockCodeList
from TradeLib import GetGoodStockCodeList 
from TradeLib import IsGreatBuyTiming, IsGoodBuyTiming, IsSellTiming
from TradeLib import SellStock, BuyStock
from TradeLib import CpStockCur, CpStockMst
from TradeLib import SubscribeConclusionInfo, UnSubscribeConclusionInfo
from TradeLib import StockInfosPerDay, DayStockInfo, GetLowPoints, GetHighPoints, CpStockChart, GetBaseLines, StockInfo
# from StockMonitoring import StockMonitor
from StockMonitoring2 import StockMonitor
import json
import math
from collections import OrderedDict
import pickle
import Constants

class Candle:
    time = 0        # hhmm
    open = 0        # 시가
    high = 0        # 고가
    low = 0         # 저가
    close = 0       # 종가
    vol = 0         # 거래량
    tradeMoney = 0  # 거래대금
    def __init__(self, time, price, vol):
        self.time = time
        self.open = price
        self.high = price
        self.low = price
        self.close = price
        self.vol = vol
        self.tradeMoney = price * vol

        return

    def Update(self, price, vol):
        self.close = price # 종가 갱신
        self.vol = self.vol + vol # 거래량 갱신
        self.tradeMoney = self.tradeMoney + (price * vol) # 거래대금 갱신

        if (self.high < price): # 고가 갱신
            self.high = price
        if (self.low > price): # 저가 갱신
            self.low = price

    def Equal(self, time, open, high, low, close, vol):
        if self.time == time and self.open == open and self.high == high and self.low == low and self.close == close and self.vol == vol:
            return True
        else:
            return False

    def IsPlusCandle(self):
        return self.open < self.close

    def Print(self):
        print("시간:", self.time, "시가:", self.open, "고가:", self.high, "저가:", self.low, "종가:", self.close, "거래량", self.vol, "거래대금:", self.tradeMoney)

class MyAutoTradeUtil:
    def LoadFile(self, filePath):
        data = {}
        with open(filePath, 'rb') as file:
            data = pickle.load(file)

        return data    

    def ConvertToChart_1Min(self, candleList, stockCurData):
        # 0: 전일대비
        # 1: 누적거래량
        # 2: 현재가
        # 3: 체결상태
        # 4: 순간체결수량
        # 5: 체결시간(초)
        # 6: 예상체결 플래그

        # print("stockCur 데이터 갯수:", len(stockCur))

        hhmm = self.MakeHHMM(stockCurData[5])
        if 1520 < hhmm: # 1521 ~ 1530은 1530봉인데 결과가 맞지 않아서 무시
            return candleList

        # hhmm에 해당하는 캔들찾기
        find = False
        targetCandle = {}
        for candle in candleList:
            if candle.time == hhmm:
                find = True
                targetCandle = candle
                break

        if find: # hhmm에 해당하는 캔들을 찾았으면
            targetCandle.Update(stockCurData[2], stockCurData[4])
        else:
            targetCandle = Candle(hhmm, stockCurData[2], stockCurData[4])
            candleList.append(targetCandle)

        return candleList

    def ConvertToChart_1MinFromStockCurList(self, candleList, stockCur):
        for stockCurData in stockCur:
            self.ConvertToChart_1Min(candleList, stockCurData)

        return candleList        

    def ConvertToChart_3Min(self, candleList, stockCurData):
        hh, mm = divmod(stockCurData[5], 10000)
        mm, tt = divmod(mm, 100)

        hhmm = 0
        if 57 <= mm and mm <= 59: # 57분 ~ 59분이면 hh += 1
            hhmm = (hh + 1) * 100
        else:
            mm = 3 + (3 * (int)(mm / 3))
            hhmm = hh * 100  + mm

        if 1520 < hhmm: # 1521 ~ 1530은 결과가 맞지 않아서 무시
            return candleList

        # hhmm에 해당하는 캔들찾기
        find = False
        targetCandle = {}
        for candle in candleList:
            if candle.time == hhmm:
                find = True
                targetCandle = candle
                break

        if find: # hhmm에 해당하는 캔들을 찾았으면
            targetCandle.Update(stockCurData[2], stockCurData[4])
        else:
            targetCandle = Candle(hhmm, stockCurData[2], stockCurData[4])
            candleList.append(targetCandle)

        return candleList

    def ConvertToChart_3MinFromStockCurList(self, candleList, stockCur):
        for stockCurData in stockCur:
            self.ConvertToChart_3Min(candleList, stockCurData)

        return candleList

    def MakeHHMM(self, time):
        # 예: 93055   시:9, 분: 30, 초: 55
        hh, mm = divmod(time, 10000)
        mm, tt = divmod(mm, 100)

        mm += 1 # 예: 09:30:55에 들어온 데이터는 09:31분 봉의 데이터이기 때문에 분에 1을 더함
        if (mm == 60) :
            hh += 1
            mm = 0
 
        hhmm = hh * 100 + mm # 예: hh:9, mm:30, hhmm:930

        if hhmm > 1530:
            hhmm = 1530

        return hhmm      

    def GetTimeAddedSec(self, time, secToAdd):
        resultHH = 0
        resultMM = 0
        resultTT = 0

        hh, mm = divmod(time, 10000)
        mm, tt = divmod(mm, 100)

        if tt + secToAdd < 60: 
            resultHH = hh
            resultMM = mm
            resultTT = tt + secToAdd
        else:
            resultHH = hh
            resultMM = mm + 1
            resultTT = tt + secToAdd - 60
            if 60 == resultMM:
                resultHH = hh + 1
                resultMM = 0

        return resultHH * 10000 + resultMM * 100 + resultTT  

# 매매 처리
class TradeRequest:
    def __init__(self):
        return

    pass


# 계좌
class AccountInfo:
    def __init__(self, code):
        self.code = code # 종목
        self.curPrice = 0 # 현재가
        self.epal = 0 # 평가손익
        self.profitsRate = 0 # 수익률
        self.averagePrice = 0 # 매입가(평단)
        self.vol = 0 # 보유수량
        self.sellableVol = 0 # (매도)가능수량
        self.totalMoney = 0 # 총매입(총 매수금액)
        self.totalCurMoney = 0 # 총평가(총매입을 현재가로 재평가한 금액)
        self.realProfits = 0 # 실현손익
        return

    def UpdateInfo(self): # 정보갱신
        self.totalMoney = self.averagePrice * self.vol # 총매입
        self.totalCurMoney = self.curPrice * self.vol # 총평가
        self.epal = ((self.curPrice - self.averagePrice) * self.vol) - self.GetTax(self.totalMoney, self.totalCurMoney) # 평가손익

        if 0 == self.totalMoney:
            self.profitsRate = 0
        else:
            self.profitsRate = (self.epal / self.totalMoney) * 100 # 수익률
        
        return

    def GetTax(self, totalMoney, totalCurMoney):
        return (totalMoney * Constants.BUY_TAX) + (totalCurMoney * Constants.SELL_TAX) + (totalCurMoney * Constants.TRANSACTION_TAX)

    def OnUpdateCurPrice(self, curPrice): # 현재가 갱신 알림
        self.curPrice = curPrice
        self.UpdateInfo()
        return

    def OnSuccessBuy(self, price, vol): # 매수체결 알림
        self.curPrice = price
        self.averagePrice = ((self.averagePrice * self.vol) + (price * vol)) / (self.vol + vol)
        self.vol += vol
        self.sellableVol += vol

        self.UpdateInfo()
        self.PrintInfo()
        return        

    def OnSuccessReqSell(self, price, vol): # 매도주문 성공 알림
        self.sellableVol -= vol
        return

    def OnCancelReqSell(self, price, vol): # 매도주문 취소 알림
        self.sellableVol += vol
        return      

    def OnSuccessSell(self, price, vol): # 매도체결 알림
        print("****체결직전 계좌정보***")
        self.PrintInfo()

        self.curPrice = price
        self.vol -= vol
        self.UpdateInfo()

        # 실현손익
        totalMoney = self.averagePrice * vol
        totalCurMoney = price * vol
        self.realProfits += ((price - self.averagePrice) * vol) - self.GetTax(totalMoney, totalCurMoney)
        
        print("****체결직후 계좌정보***")
        self.PrintInfo()
        return

    def PrintInfo(self):
        print("****총매입:", self.totalMoney, "총평가:", self.totalCurMoney, "평가손익", self.epal, "수익률:", round(self.profitsRate, 2), "%", "매입가:", self.averagePrice, "보유수량:", self.vol, "가능수량:", self.sellableVol, "현재가:", self.curPrice, "실현손익:", self.realProfits)

    pass

class MyAccount:
    def __init__(self, tradeAI):
        self.tradeAI = tradeAI
        self.accountInfos = {}
        return

    def GetInfo(self, code):
        if None == self.accountInfos.get(code):
            return None
        
        return self.accountInfos[code]
    
    def OnUpdateStockCur(self, code, stockCur): # 체결 데이터
        if None == self.accountInfos.get(code):
            return

        self.accountInfos[code].OnUpdateCurPrice(stockCur[2]) # 해당 종목에 대한 계좌 갱신
        return

    def OnSuccessReqBuy(self, reqID, code, price, vol): # 매수주문 성공 알림
        self.tradeAI.OnSuccessReqBuy(reqID, code, price, vol)
        return

    def OnFailReqBuy(self, code, price, vol): # 매수주문 실패 알림
        self.tradeAI.OnFailReqBuy(code, price, vol)
        return

    def OnCancelReqBuy(self, reqID, code, price, vol): # 매수주문 취소 알림
        self.tradeAI.OnCancelReqBuy(reqID, code, price, vol)
        return

    def OnSuccessBuy(self, reqID, code, price, vol): # 매수체결 알림
        # 계좌 갱신
        if None == self.accountInfos.get(code):
            accountInfo = AccountInfo(code)
            self.accountInfos[code] = accountInfo
        
        self.accountInfos[code].OnSuccessBuy(price, vol)

        #
        self.tradeAI.OnSuccessBuy(reqID, code, price, vol)
        return

    def OnCompleteBuy(self, reqID, code, price, vol): # 매수체결 완료 알림
        self.tradeAI.OnCompleteBuy(reqID, code, price, vol)
        return

    def OnSuccessReqSell(self, reqID, code, price, vol): # 매도주문 성공 알림
        self.accountInfos[code].OnSuccessReqSell(price, vol)
        self.tradeAI.OnSuccessReqSell(reqID, code, price, vol)
        return

    def OnFailReqSell(self, code, price, vol): # 매도주문 실패 알림
        self.tradeAI.OnFailReqSell(code, price, vol)
        return

    def OnCancelReqSell(self, reqID, code, price, vol): # 매도주문 취소 알림
        self.accountInfos[code].OnCancelReqSell(price, vol)
        self.tradeAI.OnCancelReqSell(reqID, code, price, vol)
        return

    def OnSuccessSell(self, reqID, code, price, vol): # 매도체결 알림
        # 계좌 갱신        
        self.accountInfos[code].OnSuccessSell(price, vol)

        #
        self.tradeAI.OnSuccessSell(reqID, code, price, vol)
        return

    def OnCompleteSell(self, reqID, code, price, vol): # 매도체결 완료 알림
        self.tradeAI.OnCompleteSell(reqID, code, price, vol)
        return             

    def OnCompleteSellMarket(self, code, price, vol): # 시장가 매도체결 완료 알림
        self.tradeAI.OnCompleteSellMarket(code, price, vol)
        return
    pass    


# 종목정보
class StockInfo:         
    def __init__(self, code):
        self.code = code
        self.impPrices = [] # 중요한 가격대
        self.chart_3Min = []
        self.totalMoney = 0 # 거래대금
        self.volForCheckBigVols = 0 # 거래량 터지기 전의 전조증상을 느끼기 위한 거래량

        return

    def GetCurPrice(self):
        return self.chart_3Min[len(self.chart_3Min) - 1].close

    def GetCurCandleIndex(self):
        return len(self.chart_3Min) - 1

    def GetCandle(self, candleIndex):
        if candleIndex < 0 or len(self.chart_3Min) <= candleIndex:
            return None

        return self.chart_3Min[candleIndex]      

    def AddImportantPrice(self, price): # 중요한 가격 추가
        if price in self.impPrices:
            return

        self.impPrices.append(price)
        return
    pass    