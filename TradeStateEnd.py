from TradeFsmMgr import TradeStateBase, TradeStateType


class TradeStateEnd(TradeStateBase):
    def __init__(self, code, fsm, tradeAlgorithm):
        super().__init__(code, fsm, tradeAlgorithm)

        self.stateType = TradeStateType.End.value
        return

    def GetStateName(self): 
        return "매매 종료"
 
    def Enter(self, args):
        super().Enter(args)
        return