import time
import win32com.client
import datetime
from datetime import date, timedelta
from PyQt5.QtWidgets import *
import sys
from TradeLib import SendMsgToSlack
from TradeLib import GetGreatStockCodeList
from TradeLib import GetGoodStockCodeList 
from TradeLib import IsGreatBuyTiming, IsGoodBuyTiming, IsSellTiming
from TradeLib import SellStock, BuyStock
from TradeLib import CpStockCur, CpStockMst
from TradeLib import SubscribeConclusionInfo, UnSubscribeConclusionInfo
from TradeLib import StockInfosPerDay, DayStockInfo, GetLowPoints, GetHighPoints, CpStockChart, GetBaseLines, StockInfo
# from StockMonitoring import StockMonitor
from StockMonitoring2 import StockMonitor
import json
import math
from collections import OrderedDict
import pickle
from MyAutoTradeUtil import MyAutoTradeUtil
from MyAutoTradeUtil import Candle

def Test_ConfirmTradeDatasOfFile():
    # 매매데이터 파일 로드
    tradeUtil = MyAutoTradeUtil()

    # date = "20210914"
    date = "20211102"
    indexEnd = 194
    # code = 'A042600'
    code = 'A004830'

    # 해당 종목에 대한 체결정보 배열 얻기
    stockCur = []

    for i in range(indexEnd):
        # 매매데이터 파일 로드
        index = i + 1
        filePath = "./매매 데이터/"+ date + "_" + str(index) + ".p"

        tradeDataList = tradeUtil.LoadFile(filePath)
        # print('종목갯수:', len(tradeDataList))
        # for tradeData in tradeDataList:
        #     print("코드:", tradeData['code'], "이름:", tradeData['name'])

        # 특정 종목에 대한 매매데이터를 찾음
        targetTradeData = {}
        for tradeData in tradeDataList:
            if tradeData['code'] == code:
                targetTradeData = tradeData
                break

        stockCur.extend(targetTradeData['stockCur'])

    print("종목이름:", targetTradeData['name'])

    # # 체결정보 배열을 1분 차트로 변환
    # chart_1Min = []
    # tradeUtil.ConvertToChart_1MinFromStockCurList(chart_1Min, stockCur)
    # # for candle in chart_1Min:
    # #     candle.Print()

    # 체결정보 배열을 3분 차트로 변환
    chart_3Min = []
    tradeUtil.ConvertToChart_3MinFromStockCurList(chart_3Min, stockCur)
    # for candle in chart_3Min:
    #     candle.Print()

    # 서버로부터 3분차트 얻기
    objChart = CpStockChart()
    tempDayStockInfos = objChart.RequestMT(code, ord('m'), 3, 600)
    downloadedChart_3Min = []
    if tempDayStockInfos:
        for i in range(len(tempDayStockInfos))[::-1]:
            info = tempDayStockInfos[i]
            if info.date == int(date):
                # print("날짜", info.date, "시간:", info.time, "시가:", info.open, "고가:", info.high, "저가:", info.low, "종가:", info.close, "전일대비", info.diff, "거래량", info.vol)
                downloadedChart_3Min.append(info)

    # 서버로부터 받은 데이터와 비교검증
    equal = True
    for i in range(len(chart_3Min)):
        if i == 0: # 첫 번째 캔들은 미완성이므로 무시
            continue

        candle = chart_3Min[i]

        # 비교할 캔들 찾기
        find = False
        targetCandle = {}
        for c in downloadedChart_3Min:
            if candle.time == c.time:
                find = True
                targetCandle = c
                break

        if find: # 찾았으면
            # 같은지 비교
            if False == candle.Equal(targetCandle.time, targetCandle.open, targetCandle.high, targetCandle.low, targetCandle.close, targetCandle.vol):
                equal = False
                break
            else:
                print(candle.time, " 캔들 같음", "시가:", candle.open, "서버 시가:", targetCandle.open)    

        else: # 못찾았으면
            equal = False
            print(candle.time, "캔들 못찾음")
            break

    if equal:
        print("결과: 같음")
    else:
        print("결과: 다름")


Test_ConfirmTradeDatasOfFile()